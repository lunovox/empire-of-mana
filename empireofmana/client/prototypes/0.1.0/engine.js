
// Version 3 - Basic collision (viewbox), perfectly elastic (no energy loss)
var fps = 77;
var seTxtVisivel = true;
var angulo = (Math.PI*2)*Math.random();
var myTeclado = {btnNumero:0, btnTecla:"", btnCtrl:false, btnAlt:false, btnShift:false};
var myMouse = {x:0, y:0};
var canvas = document.getElementById("surface");
var ctx = canvas.getContext('2d');
var imgTitulo = new Image();
var imgFundo = new Image();
var imgBola = new Image();
var ball = {x:300, y:200, r:24, vx:30*Math.sin(angulo), vy:30*Math.cos(angulo), elasticity:0.8, color:"#FFFF00"};

function init() {
    //document.onload = function(){ setInterval(update, 1000/fps); }
    document.onmousemove = function(event) {onMouseMove(event);}
    document.onmousedown = function(event) {onMouseDown(event);}
    document.onkeydown = function(event) {onKeyDown(event);}
    document.onkeypress = function(event) {onKeyPress(event);} // Não está funcionando do Chromium
    imgFundo.src = 'anime.png';
    imgTitulo.src = 'tmw-maker.png';
    imgBola.src = 'icon_48x48_tmw.png';
    setInterval(update, 1000/fps);
}
function onKeyPress(event){
    myTeclado.btnNumero = event.keyCode;
    myTeclado.btnTecla = String.fromCharCode(event.charCode);
    myTeclado.btnCtrl = event.ctrlKey;
    myTeclado.btnAlt = event.altKey;
    myTeclado.btnShift = event.shiftKey;
    if(
        myTeclado.btnNumero == 13 ||
        myTeclado.btnCtrl ||
        myTeclado.btnAlt ||
        myTeclado.btnShift
    ){ ball.vx = 0; ball.vy = 0; } //Enter
}
function onKeyDown(event){
    myTeclado.btnNumero = event.keyCode;
    myTeclado.btnTecla = String.fromCharCode(event.charCode);
         if(myTeclado.btnNumero == 27){ seTxtVisivel = !seTxtVisivel; }  //ESC
    else if(myTeclado.btnNumero == 38){ ball.vy = -30; } //Up
    else if(myTeclado.btnNumero == 40){ ball.vy = 30; }  //down
    else if(myTeclado.btnNumero == 37){ ball.vx = -30; } //left
    else if(myTeclado.btnNumero == 39){ ball.vx = 30; }  //right
    else if(myTeclado.btnNumero == 112){ alert("nothing!!");} //F1 (Não está cancelando a tecla)
}
function onMouseDown(event){
    onMouseMove(event);
    if(
        myMouse.x >= canvas.offsetLeft + ball.x &&
        myMouse.x <= canvas.offsetLeft + ball.x + (ball.r*2) &&
        myMouse.y >= canvas.offsetTop + ball.y &&
        myMouse.y <= canvas.offsetTop + ball.y + (ball.r*2)
    ){
        var angulo = (Math.PI*2)*Math.random();
        ball.vx=30*Math.sin(angulo);
        ball.vy=30*Math.cos(angulo);
    }
}
function onMouseMove(event){
    if (event.layerX || event.layerX == 0) { // Firefox
        myMouse.x = event.layerX;
        myMouse.y = event.layerY;
    } else if (event.offsetX || event.offsetX == 0) { // Opera
        myMouse.x = event.offsetX;
        myMouse.y = event.offsetY;
    }
}
function printTesto(myTesto, X, Y) {
    printTesto(myTesto, X, Y, "", "", "");
}
function printTesto(myTesto, X, Y, myFonte) {
    printTesto(myTesto, X, Y, myFonte, "", "");
}
function printTesto(myTesto, X, Y, myFonte, corFonte) {
    printTesto(myTesto, X, Y, myFonte, corFonte, "");
}
function printTesto(myTesto, X, Y, myFonte, corFonte, corAureula) {
    if(myTesto!=undefined && myTesto!=""){
        if(myFonte!=undefined && myFonte!="") ctx.font = myFonte;
        if(corAureula!=undefined && corAureula!=""){
            //myTesto+=" ["+corAureula+"]";
            ctx.fillStyle = corAureula;
            ctx.fillText(myTesto, X, Y-1);
            ctx.fillText(myTesto, X, Y+1);
            ctx.fillText(myTesto, X-1, Y);
            ctx.fillText(myTesto, X+1, Y);
        }
        if(corFonte!=undefined && corFonte!="") ctx.fillStyle = corFonte;
        ctx.fillText(myTesto, X, Y);
    }
}
function showTesto() {
    ctx.save();
    //ctx.fillStyle = "#000000"; ctx.font = "14pt Verdana";
    //ctx.fillText("Demonstração de Dinamica de Jogo WEB sem uso de Flash", 5, 20);
    ctx.drawImage(imgTitulo,0,0,imgTitulo.width,imgTitulo.height);

    //ctx.fillStyle = "rgba(255,255,255, 1.0)";
    //ctx.font = "10pt Verdana";
    //ctx.fillText("Bola.: "+Math.round(ball.x)+"/"+Math.round(ball.y)+" px(x/y)",5, 70);
    //ctx.fillText("Vel..: "+Math.round(ball.vx)+"/"+Math.round(ball.vy,2)+" px/Hz(x/y)", 5, 85);
    //ctx.fillText("FPS..: "+fps,5, 100);
    //ctx.fillText("Mouse: "+myMouse.x+"/"+myMouse.y,5, 115);
    //ctx.fillText("Key..: "+myTeclado.btnNumero+(myTeclado.btnTecla.trim()!=""?"["+myTeclado.btnTecla.trim()+"]":""),5, 130);
    printTesto("Bola.: "+Math.round(ball.x)+"/"+Math.round(ball.y)+" px(x/y)",5, 70, "10pt Verdana", "rgba(0,0,0,0.5)");
    printTesto("Vel..: "+Math.round(ball.vx)+"/"+Math.round(ball.vy,2)+" px/Hz(x/y)", 5, 85);
    printTesto("FPS..: "+fps,5, 100, "10pt Verdana", "#000000", "#00FFFF"/**/);
    printTesto("Mouse: "+myMouse.x+"/"+myMouse.y,5, 115, "10pt Verdana", "rgba(0,0,0,0.5)");
    printTesto("Key..: "+myTeclado.btnNumero+(myTeclado.btnTecla.trim()!=""?"["+myTeclado.btnTecla.trim()+"]":""),5, 130);
    printTesto(
        "Pressione setas do teclado ou use o mouse para chutar a bola!",
        5, canvas.height-10, "10pt Verdana", "#000000", "rgba(255,255,255, 1.0)"
    );
    ctx.restore();
}
function update(){

    // Clear display
    ctx.save();
    ctx.drawImage(imgFundo,0,0,canvas.width,canvas.height);
    //ctx.fillStyle = "rgba(255,255,255, 0.25)";
    //ctx.fillRect(0, 0, canvas.width, canvas.height);
    //ctx.globalAlpha = 0.1;
    ctx.restore();


    // Update ball (with Physics! =)
    //
    // 1 - apply velocity to position (vx -> x)
    ball.x += ball.vx;
    ball.y += ball.vy;

    // 2 - apply drag/friction to velocity
    ball.vx *= .99;
    ball.vy *= .99;

    // 3 - apply gravity to velocity
    ball.vy += .25; //.25;


    // 4 - check for collisions
    if (ball.y > canvas.height - ball.r) {
        ball.y = canvas.height - ball.r;
        ball.vy = -Math.abs(ball.vy);
        ball.vy *= ball.elasticity;
    }
    if (ball.x > canvas.width - ball.r) {
        ball.x = canvas.width - ball.r;
        ball.vx = -Math.abs(ball.vx);
        ball.vy *= ball.elasticity;
    }
    if (ball.y < 0) {
        ball.y = 0 + ball.r;
        ball.vy = Math.abs(ball.vy);
        ball.vy *= ball.elasticity;
    }
    if (ball.x < 0) {
        ball.x = 0 + ball.r;
        ball.vx = Math.abs(ball.vx);
        ball.vy *= ball.elasticity;
    }

    // Draw ball
    ctx.save();
    ctx.translate(ball.x, ball.y);

    ctx.fillStyle = ball.color;
    ctx.beginPath();
    //ctx.fillStyle = ball.color;
    //ctx.arc(0, 0, ball.r, 0, Math.PI * 2, true);

    ctx.drawImage(imgBola,0,0,imgBola.width,imgBola.height);
    ctx.closePath();
    ctx.fill();
    
    ctx.restore();

    if(seTxtVisivel==true) showTesto();
}

init();
