function loadBackground(url){
	var canvas = document.getElementById("cnvPalco");
	var ctx = canvas.getContext('2d');
	if(url!=null && url!=""){
		document.body.style.cursor='wait';
		ctx.save();
		var imgEngine = new Image();
		imgEngine.src=url+getIfUptadeFiles();
		imgEngine.onload = function (){
			ctx.drawImage(imgEngine,0,0,imgEngine.width,imgEngine.height,0,0,canvas.width,canvas.height);
			document.body.style.cursor='default';
		};
		ctx.restore();
	}else{
		ctx.clearRect(0, 0, canvas.width, canvas.height);
	}
}
function drawSprite($canvas, $context, $imgSprite, $xBoxes, $yBoxes, $box){
	if($imgSprite!=null){
		$w=$imgSprite.width/$xBoxes;
		$h=$imgSprite.height/$yBoxes;
		x1=parseInt($box % $xBoxes) * $w;
		y1=parseInt($box / $xBoxes) * $h;
		$context.save();
		try{
			$context.drawImage(
				$imgSprite,
				x1,y1,$w,$h,
				0,0,$canvas.width,$canvas.height
			);
		}catch(ex){
			alert(ex);
		}
		$context.restore();
	}else{
		alert("ERRO: imagem nula!");
	}
}
function drawSpriteRecolor($cnv1,$ctx1,$imgSprite,$xBoxes,$yBoxes,$box,$left,$top,$deCorHex,$toCorHex,$ReCorHex,$Antialiase,$Gama){
				//Gama = (-100 ~ +100)
				/*alert(
					"$cnv1 → "+$cnv1+"\n"+
					"$ctx1 → "+$ctx1+"\n"+
					"$imgSprite → "+$imgSprite+"\n"+
					"$xBoxes → "+$xBoxes+"\n"+
					"$yBoxes → "+$yBoxes+"\n"+
					"$box → "+$box+"\n"+
					"$left → "+$left+"\n"+
					"$top → "+$top+"\n"+
					"$deCorHex → "+$deCorHex+"\n"+
					"$toCorHex → "+$toCorHex+"\n"+
					"$ReCorHex → "+$ReCorHex+"\n"+
					"$Antialiase → "+$Antialiase+"\n"+
					"$Gama → "+$Gama
				);/**/
				
				function getCutHex(h) {return (h.charAt(0)=="#") ? h.substring(1,7):h}
				function getHexToR(h) {return parseInt((getCutHex(h)).substring(0,2),16)}
				function getHexToG(h) {return parseInt((getCutHex(h)).substring(2,4),16)}
				function getHexToB(h) {return parseInt((getCutHex(h)).substring(4,6),16)}
				function getHexToX(h,X) {if(X==0){return getHexToR(h);}else if(X==1){return getHexToG(h);}else if(X==2){return getHexToB(h);}}
				if($imgSprite!=null){
					//try{
						var $cnv2 = document.createElement("canvas");
						$w=$imgSprite.width/$xBoxes;
						$h=$imgSprite.height/$yBoxes;
						$cnv2.width=$cnv1.width;
						$cnv2.height=$cnv1.height;
						var $ctx2 = $cnv2.getContext('2d');
						x1=parseInt($box % $xBoxes) * $w;
						y1=parseInt($box / $xBoxes) * $h;
						$ctx2.drawImage(
							$imgSprite,
							x1,y1,$w,$h,
							0,0,$cnv2.width,$cnv2.height
						);
						//alert("$cnv1.width → "+$cnv1.width);
						//var $ctx1 = $cnv1.getContext('2d');
						var $date1 = $ctx1.getImageData(0,0,$cnv1.width,$cnv1.height);
						var $date2 = $ctx2.getImageData(0,0,$cnv2.width,$cnv2.height);
						$pix1=$date1.data;
						$pix2=$date2.data;
						for (var i=0; i<$pix2.length; i+=4) {
							if($pix2[i+3]>(($Antialiase/100)*255)){ // i+3 = Alpha (4º cor). 0>(Sessibilidade do Anti-aliase)
								for (var c=0; c<3; c++) {
									if(
										($pix2[i+0]>=getHexToX($deCorHex,0) && $pix2[i+0]<=getHexToX($toCorHex,0)) &&
										($pix2[i+1]>=getHexToX($deCorHex,1) && $pix2[i+1]<=getHexToX($toCorHex,1)) &&
										($pix2[i+2]>=getHexToX($deCorHex,2) && $pix2[i+2]<=getHexToX($toCorHex,2))
									){/**/
									//if($pix2[i+c]>=getHexToX($deCorHex,c) && $pix2[i+c]<=getHexToX($toCorHex,c)){
										$pix2[i+c]=(((getHexToX($ReCorHex,c)/255)*$pix2[i+c])*($pix2[i+3]/255))+($pix1[i+c]*((255-$pix2[i+3])/255));
										$pix1[i+c]=$Gama<=0?($pix2[i+c]-($pix2[i+c]*($Gama/-100))):($pix2[i+c]+((255-$pix2[i+c])*($Gama/100)));
									}else{
										$pix1[i+c]=$pix2[i+c];
										//$pix2[i+c]=(((getHexToX($ReCorHex,c)/255)*$pix2[i+c])*($pix2[i+3]/255))+($pix1[i+c]*((255-$pix2[i+3])/255));
										$pix1[i+c]=$Gama<=0?($pix2[i+c]-($pix2[i+c]*($Gama/-100))):($pix2[i+c]+((255-$pix2[i+c])*($Gama/100)));
										
									}
								}
								$pix1[i+3] = Math.max($pix1[i+3],$pix2[i+3]); // alpha
							}
						}
						$ctx1.putImageData($date1,$left,$top); // Draw the ImageData at the given (x,y) coordinates.
					/*}catch(ex){
						alert("ERRO 'drawSpriteRecolor()': "+ex);
					}/**/
				}else{
					alert("ERRO 'drawSpriteRecolor()': imagem nula!");
				}
			}
function drawAvatar($Canvas, $context, $Sex, $Race, $Class, $HairType, $HairColor, $Eye, $Nose, $Lip, $Mark){
	$context.save();
	$context.clearRect(0, 0, $Canvas.width, $Canvas.height);
	if(usersXML!=null && usersXML.getURL().toString()!=""){
		//var $context = $Canvas.getContext('2d');
		var DefaultClasses = usersXML.getClasses();
		var GenericAvatars = usersXML.getAvatars();
		
		//alert("$Sex="+$Sex);
		if(GenericAvatars.races.path!=""){
			if($Race>=0){
				$box = $Sex=="M" ? GenericAvatars.races.race[$Race].male.box : GenericAvatars.races.race[$Race].female.box;
				//alert("$box="+$box);
				if($box>=1){ // ← Zero significa que a box é invisivel!
					drawSprite($Canvas, $context, 
						GenericAvatars.races.sprite, 
						GenericAvatars.races.boxes.split(",")[0], 
						GenericAvatars.races.boxes.split(",")[1], 
						$box - 1
					);
				}
			}
		}
		if(DefaultClasses.path!=""){
			if($Class>=0){
				$box = $Sex=="M" ? DefaultClasses.class[$Class].male.box : DefaultClasses.class[$Class].female.box;
				//alert("$box="+$box);
				if($box>=1){ // ← Zero significa que a box é invisivel!
					drawSprite($Canvas, $context, 
						DefaultClasses.sprite, 
						DefaultClasses.boxes.split(",")[0], 
						DefaultClasses.boxes.split(",")[1], 
						$box - 1
					); /**/ 
					/*drawSpriteRecolor($Canvas,$context,
						DefaultClasses.sprite,
						DefaultClasses.boxes.split(",")[0],
						DefaultClasses.boxes.split(",")[1],
						$box - 1,0,0,
						//"#000000","#FFFFFF","FFFFFF",80,15
						"#000000","#989898","#0000FF",0,15
					);/**/
				}
			}
		}
		if(GenericAvatars.eyes.path!=""){
			if($Eye>=0){
				$box = $Sex=="M" ? GenericAvatars.eyes.eye[$Eye].male.box : GenericAvatars.eyes.eye[$Eye].female.box; 
				//alert("$box="+$box);
				if($box>=1){ // ← Zero significa que a box é invisivel!
					drawSprite($Canvas, $context, 
						GenericAvatars.eyes.sprite, 
						GenericAvatars.eyes.boxes.split(",")[0], 
						GenericAvatars.eyes.boxes.split(",")[1], 
						$box - 1
					);
				}
			}
		}
		if(GenericAvatars.noses.path!=""){
			if($Nose>=0){
				$box = $Sex=="M" ? GenericAvatars.noses.nose[$Nose].male.box : GenericAvatars.noses.nose[$Nose].female.box; 
				//alert("$box="+$box);
				if($box>=1){ // ← Zero significa que a box é invisivel!
					drawSprite($Canvas, $context, 
						GenericAvatars.noses.sprite, 
						GenericAvatars.noses.boxes.split(",")[0], 
						GenericAvatars.noses.boxes.split(",")[1], 
						$box - 1
					);
				}
			}
		}
		if(GenericAvatars.lips.path!=""){
			if($Lip>=0){
				$box = $Sex=="M" ? GenericAvatars.lips.lip[$Lip].male.box : GenericAvatars.lips.lip[$Lip].female.box; 
				//alert("$box="+$box);
				if($box>=1){ // ← Zero significa que a box é invisivel!
					drawSprite($Canvas, $context, 
						GenericAvatars.lips.sprite, 
						GenericAvatars.lips.boxes.split(",")[0], 
						GenericAvatars.lips.boxes.split(",")[1], 
						$box - 1
					);
				}
			}
		}
		if(GenericAvatars.hairs.path!="" /*&& GenericAvatars.hairs.sprite/**/){
			if($HairType>=0){
				$box = $Sex=="M" ? GenericAvatars.hairs.hair[$HairType].male.box : GenericAvatars.hairs.hair[$HairType].female.box; 
				if($box>=1){ // ← Zero significa que a box é invisivel!
					/*alert(
						"$Sex → "+$Sex+"\n"+
						"GenericAvatars.hairs.path → "+GenericAvatars.hairs.path+"\n"+
						"GenericAvatars.hairs.sprite → "+GenericAvatars.hairs.sprite+"\n"+
						"GenericAvatars.hairs.sprite.src → "+GenericAvatars.hairs.sprite.src+"\n"+
						"GenericAvatars.hairs.boxes[0] → "+GenericAvatars.hairs.boxes.split(",")[0]+"\n"+
						"GenericAvatars.hairs.boxes[1] → "+GenericAvatars.hairs.boxes.split(",")[1]+"\n"+
						"$box → "+($box - 1)+"\n"+
						0+"\n"+
						0+"\n"+
						//"#000000","#FFFFFF","#FFFF00",0,25
						"GenericAvatars.haircolors.blackcolor[1] → "+GenericAvatars.haircolors.blackcolor+"\n"+
						"GenericAvatars.haircolors.whitecolor[1] → "+GenericAvatars.haircolors.whitecolor+"\n"+
						"GenericAvatars.haircolors.colornew["+$HairColor+"].recolor → "+GenericAvatars.haircolors.colornew[$HairColor].recolor+"\n"+
						"GenericAvatars.haircolors.antialiase → "+GenericAvatars.haircolors.antialiase+"\n"+
						"GenericAvatars.haircolors.colornew["+$HairColor+"].gama → "+GenericAvatars.haircolors.colornew[$HairColor].gama
					);/**/
					drawSprite($Canvas, $context, 
						GenericAvatars.hairs.sprite, 
						GenericAvatars.hairs.boxes.split(",")[0], 
						GenericAvatars.hairs.boxes.split(",")[1], 
						$box - 1
					);/**/
					/*drawSpriteRecolor($Canvas,$context,
						GenericAvatars.hairs.sprite,
						GenericAvatars.hairs.boxes.split(",")[0],
						GenericAvatars.hairs.boxes.split(",")[1],
						$box - 1,0,0,
						//"#000000","#FFFFFF","#FFFF00",0,25
						GenericAvatars.haircolors.blackcolor,
						GenericAvatars.haircolors.whitecolor,
						GenericAvatars.haircolors.colornew[$HairColor].recolor,
						GenericAvatars.haircolors.antialiase,
						GenericAvatars.haircolors.colornew[$HairColor].gama
					);/**/
				}
			}
		}
		if($Race>=0){
			if(GenericAvatars.races.race[$Race].top.box!=""){
				//alert("GenericAvatars.races.race["+slcCharMakerRace.selectedIndex+"].top.box="+GenericAvatars.races.race[slcCharMakerRace.selectedIndex].top.box);
				$box = GenericAvatars.races.race[$Race].top.box;
				//alert("$box="+$box);
				if($box>=1){ // ← Zero significa que a box é invisivel!
					drawSprite($Canvas, $context, 
						GenericAvatars.races.sprite, 
						GenericAvatars.races.boxes.split(",")[0], 
						GenericAvatars.races.boxes.split(",")[1], 
						$box - 1
					);
				}
			}
		}
	}
	$context.restore();
}
