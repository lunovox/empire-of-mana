function getListMaps(){
	var $novaURL = serverXML.getPath()+"maps/list.xml"
	$import="importFile.php?mimetype=application/xml&url="+$novaURL;
	//alert($import);
	if(window.ActiveXObject){
		xmlNode = new ActiveXObject("Msxml2.DOMDocument.3.0");
		xmlNode.async = false;
		xmlNode.load($import);
		//xmlNode.load($novaURL);
	}else if(window.XMLHttpRequest){
		var Loader = new XMLHttpRequest();
		Loader.open("GET", $import, false);
		//Loader.open("GET", $novaURL ,false);
		Loader.send(null);
		xmlNode = Loader.responseXML;
	}
	if(xmlNode!=null){
		$mapas=xmlNode.getElementsByTagName("map");
		var $listMaps = new Array();
		for(var m=0;m<$mapas.length;m++){//percorrendo os filhos do nó
			if($mapas[m].nodeType == 1){
				$listMaps[$listMaps.length]=[];
				$listMaps[$listMaps.length-1].name=$mapas[m].getAttribute("name");
				var $Filename = $mapas[m].getAttribute("src");
				$listMaps[$listMaps.length-1].filename=$Filename;
				$listMaps[$listMaps.length-1].body=openMapByFilename($Filename);
				$listMaps[$listMaps.length-1].tilesets = new Array();
				var $Tilesets=$listMaps[$listMaps.length-1].body.getElementsByTagName("tileset");
				for(var t=0; t<$Tilesets.length; t++){ 
					$listMaps[$listMaps.length-1].tilesets[t]=[];
					$listMaps[$listMaps.length-1].tilesets[t].name=$Tilesets[t].getAttribute("name");
					$listMaps[$listMaps.length-1].tilesets[t].tileWidth=$Tilesets[t].getAttribute("tilewidth");
					$listMaps[$listMaps.length-1].tilesets[t].tileHeight=$Tilesets[t].getAttribute("tileheight");
					var $Filename = $Tilesets[t].getAttribute("image");
					$listMaps[$listMaps.length-1].tilesets[t].filename=$Filename;
					$listMaps[$listMaps.length-1].tilesets[t].image=new Image();
					loadAdd($Filename);
					$listMaps[$listMaps.length-1].tilesets[t].image.src=serverXML.getPath()+"tilesets/"+$Filename+getIfUptadeFiles();
					$listMaps[$listMaps.length-1].tilesets[t].image.onload = function (){
						//ctx.drawImage(imgGrounds,0,0,imgGrounds.width,imgGrounds.height,0,0,canvas.width,canvas.height);
						loadRemove($Filename);
					};
					//alert($Tilesets[t].getAttribute("name"));
					//alert(serverXML.getPath()+"tilesets/"+$Filename);
				}
				var $Musics=$listMaps[$listMaps.length-1].body.getElementsByTagName("backgroundmusic");
				if($Musics.length>=1){
					$listMaps[$listMaps.length-1].music=[];
					var $Filename = $Musics[0].getAttribute("source");
					$listMaps[$listMaps.length-1].music.filename=$Filename;
					$listMaps[$listMaps.length-1].music.audio = new Audio();
					loadAdd($Filename);
					$listMaps[$listMaps.length-1].music.audio.src=serverXML.getPath()+"musics/"+$Filename+getIfUptadeFiles();
					//alert("$listMaps["+($listMaps.length-1)+"].music.audio.src = "+$listMaps[$listMaps.length-1].music.audio.src);
					$listMaps[$listMaps.length-1].music.audio.load();
					$listMaps[$listMaps.length-1].music.audio.onload = function (){
						loadRemove($Filename);
					}
					/*$listMaps[$listMaps.length-1].music.play = function(){
						loadMusicByAudio($listMaps[$listMaps.length-1].music.audio);
					}/**/
				}
				//alert($Tilesets.length);
			}
		}
		return $listMaps;
	}
}
function openMapByFilename($Filename){
	var $novaURL = serverXML.getPath()+"maps/"+$Filename
	$import="importFile.php?mimetype=application/xml&url="+$novaURL;
	//alert($import);
	if(window.ActiveXObject){
		xmlNode = new ActiveXObject("Msxml2.DOMDocument.3.0");
		xmlNode.async = false;
		xmlNode.load($import);
		//xmlNode.load($novaURL);
	}else if(window.XMLHttpRequest){
		var Loader = new XMLHttpRequest();
		Loader.open("GET", $import, false);
		//Loader.open("GET", $novaURL ,false);
		Loader.send(null);
		xmlNode = Loader.responseXML;
	}
	if(xmlNode!=null){
		$mapas=xmlNode.getElementsByTagName("map");
		return $mapas[0];
	}
	return null;
}
function drawMap(){
	drawLayer("ground");
	drawLayer("obstacle"); //← Obstacle não deve ser desenhada por drawLayer() pq tem que ser desenhada ao mesmo tempo do char uma linha de casa vez.
	//drawLayer("top");
	if(SeDepureMode){drawColision();}
}
function drawLayer($TypeLayer){
	$Mapa = myMaps[myChar.atributos.mapa.id];
	var canvas = document.getElementById("cnvPalco");
	var ctx = canvas.getContext('2d');
	var $MapWidth = $Mapa.body.getAttribute("width");
	var $MapHeight = $Mapa.body.getAttribute("height");
	var $TileWidth = $Mapa.body.getAttribute("tilewidth");
	var $TileHeight = $Mapa.body.getAttribute("tileheight");

	var $Layers = $Mapa.body.getElementsByTagName($TypeLayer);
	for(var g=0;g<$Layers.length;g++){
		var $tiles=$Layers[g].getAttribute("tiles");
		var $partTiles = $tiles.split(";");
		for(var t=0;t<$partTiles.length;t++){
			//document.title="$partTiles["+t+"]="+$partTiles[t];
			if($partTiles[t].trim()!=""){
				if($partTiles[t]!="..."){
					var $EnderecoDoTile = $partTiles[t].split(":");
					//var $TileImage=myMaps[myChar.atributos.mapa.id].tilesets[$EnderecoDoTile[0]-1].image;
					//var $TileBox=$EnderecoDoTile[1];
					//document.title=$EnderecoDoTile[0];
					var $Sprite=myMaps[myChar.atributos.mapa.id].tilesets[$EnderecoDoTile[0]-1].image;
					var $BoxWidth=myMaps[myChar.atributos.mapa.id].tilesets[$EnderecoDoTile[0]-1].tileWidth;
					var $BoxHeight=myMaps[myChar.atributos.mapa.id].tilesets[$EnderecoDoTile[0]-1].tileHeight;
					var $Box=$EnderecoDoTile[1];
				}
			}else{
				$Box=undefined;
			}
				
			if($Box!=undefined){
				//document.title="Ultimo.Box="+Ultimo.Box;
				//document.title="myMaps["+myChar.atributos.mapa.id+"].tilesets["+$EnderecoDoTile[0]-1+"].image="+$TileImage;
				var onBoxX = ($Box%($Sprite.width/$BoxWidth))*$BoxWidth;
				var onBoxY = parseInt($Box/($Sprite.width/$BoxWidth))*$BoxHeight;
				//document.title=onBoxX+"/"+onBoxY;
				var toBoxX = (t%(canvas.width/$TileWidth))*$TileWidth;
				//var toBoxY = (parseInt(t/(canvas.width/$TileWidth))*$TileHeight)-($TileHeight+$BoxHeight);
				var toBoxY = (parseInt(t/(canvas.width/$TileWidth))*$TileHeight)+($TileHeight-$BoxHeight);
				//document.title=toBoxX+"/"+toBoxY;
				
				ctx.drawImage(
					$Sprite,
					onBoxX,onBoxY,$BoxWidth,$BoxHeight, //0,0,$Sprite.width/2,$Sprite.height/2,
					//toBoxX,toBoxY,$TileWidth,$TileHeight //0,0,canvas.width,canvas.height
					toBoxX,toBoxY,$BoxWidth,$BoxHeight //0,0,canvas.width,canvas.height
				);
			}
			
		}
	}
	//document.title="$MapHeight="+$MapHeight;
	//alert($MapHeight);
}
function drawColision(){
	$Mapa = myMaps[myChar.atributos.mapa.id];
	var canvas = document.getElementById("cnvPalco");
	var ctx = canvas.getContext('2d');
	var $MapWidth = $Mapa.body.getAttribute("width");
	var $MapHeight = $Mapa.body.getAttribute("height");
	var $TileWidth = $Mapa.body.getAttribute("tilewidth");
	var $TileHeight = $Mapa.body.getAttribute("tileheight");
	
	var $tangibles=$Mapa.body.getElementsByTagName("collisions")[0].getAttribute("tangibles");
	var $opacity=$Mapa.body.getElementsByTagName("collisions")[0].getAttribute("opacity");
	ctx.fillStyle = "rgba(256,0,0,"+$opacity+")";
	for(var t=0;t<$tangibles.length;t++){
		var $tangible = $tangibles.charAt(t);
		//var $BoxWidth=myMaps[myChar.atributos.mapa.id].tilesets[$EnderecoDoTile[0]-1].tileWidth;
		//var $BoxHeight=myMaps[myChar.atributos.mapa.id].tilesets[$EnderecoDoTile[0]-1].tileHeight;
		var toBoxX = (t%(canvas.width/$TileWidth))*$TileWidth;
		var toBoxY = parseInt(t/(canvas.width/$TileWidth))*$TileHeight;
		//document.title = $tangible;
		if($tangible==1){
			ctx.fillRect(toBoxX+1, toBoxY+1, $TileWidth-2, $TileHeight-2);
		}
	}
}