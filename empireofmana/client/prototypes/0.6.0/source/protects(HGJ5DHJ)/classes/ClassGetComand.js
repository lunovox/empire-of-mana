function detectComand(protocolo){
	contUsers(protocolo);
	contOnline(protocolo);
	contLogged(protocolo);
	onConect(protocolo);
	onLogin(protocolo);
	onRegister(protocolo);
	onLogout(protocolo);
	onNewPass(protocolo);
	onNewChar(protocolo);
	onDelChar(protocolo);
	onListChars(protocolo);
	onSelChar(protocolo);
	onSay(protocolo);
	onBroadCast(protocolo);
	onAnnounce(protocolo);
}
function onConect(myTesto){
	//logout?you
	//logout?you&mensagem
	var divConectorTesto = document.getElementById("divConectorTesto");
	var divLoginTesto = document.getElementById("divLoginTesto");
	var slcServidores = document.getElementById("slcServidores");
	var btnConectorConectar = document.getElementById("btnConectorConectar");
	var txtLogin = document.getElementById("txtLogin");
	var txtSenha = document.getElementById("txtSenha");
	partes = myTesto.split("?");
	//alert(myTesto);
	if(partes.length==2 && partes[0]=="conect"){
		prop = partes[1].split("&");
		if(prop[0].toString().toLowerCase()=="you"){
			if(serverXML.getClientBackground().toString()!=""){ loadBackground(serverXML.getPath()+serverXML.getClientBackground()); }
			if(serverXML.getClientMusic().toString()!=""){ loadBmgByUrl(serverXML.getPath()+serverXML.getClientMusic()); }
			if(
				parseInt(serverXML.getClientFrmLoginX())>=0 && parseInt(serverXML.getClientFrmLoginX())<640 && 
				parseInt(serverXML.getClientFrmLoginY())>=0 && parseInt(serverXML.getClientFrmLoginY())<480
			){ 
				moveForm(
					document.getElementById("frmLogin"),
					parseInt(serverXML.getClientFrmLoginX()),
					parseInt(serverXML.getClientFrmLoginY())
				);
			}
			if(getServerName()=="localhost"){
				txtLogin.value="rui.gravata@gmail.com";
				txtSenha.value="123123";
			}else{
				txtLogin.value="";
				txtSenha.value="";
			}
			clearTimeout(ConectTimeOut);
			showAviso(divConectorTesto,"");
			showAviso(divLoginTesto,"");
			showForm("frmLogin");
			document.body.style.cursor='default';
			btnLoginEntrar.disabled=false;
		}/**/
	}
}/**/
function getTestLoggedAndOnline(){
	var $testo = "";
	if(online-1>logged){
		$testo=logged+" de "+parseInt(serverXML.getUsersMax())+" no servidor. (+"+((online-1) - logged)+" conectando...)";
	}else{
		$testo = logged+" de "+parseInt(serverXML.getUsersMax())+" no servidor.";
	}
	return $testo;
}
function contUsers(myTesto){
	if(myTesto.toString().toLowerCase().indexOf("ifexist?users&")==0){
		myTesto.replace("  ", " ");
		partes = myTesto.split("&");
		online=parseInt(partes[1]);
		logged=parseInt(partes[2]);
		showAviso(document.getElementById("divLoginUsers"),getTestLoggedAndOnline());
	}
}
function contOnline(myTesto){
	if(myTesto.toString().toLowerCase().indexOf("ifexist?online&")==0){
		myTesto.replace("  ", " ");
		partes = myTesto.split("&");
		online=parseInt(partes[1]);
		showAviso(document.getElementById("divLoginUsers"),getTestLoggedAndOnline());
	}
}
function contLogged(myTesto){
	if(myTesto.toString().toLowerCase().indexOf("ifexist?logged&")==0){
		myTesto.replace("  ", " ");
		partes = myTesto.split("&");
		logged=parseInt(partes[1]);
		showAviso(document.getElementById("divLoginUsers"),getTestLoggedAndOnline());
	}
}
function onRegister(myTesto){
	var btnLoginRegistrar = document.getElementById("btnLoginRegistrar");
	var btnRegisterSend = document.getElementById("btnRegisterSend");

	partes = myTesto.split("?");
	if(partes.length==2 && partes[0]=="register"){
		document.body.style.cursor='default';
		prop = partes[1].split("&");
		if(prop[0].toString().toLowerCase()=="1"){
			if(prop.length==1){
				showAviso(divLoginTesto,"Registro efetuado com sucesso!");
				showForm('frmLogin');
			}else if(prop.length>=2){
				showAviso(divLoginTesto,unescape(prop[1]));
				showForm('frmLogin');
			}
			showAviso(divRegisterTesto,"");
		}else{
			if(prop.length==1){
				showAviso(divRegisterTesto,"Falha do servidor ao salvar seu registro!");
				showForm('frmRegister');
			}else if(prop.length>=2){
				showAviso(divRegisterTesto,unescape(prop[1]));
				showForm('frmRegister');
			}
			showAviso(divLoginTesto,"");
			btnRegisterSend.disabled=false;
			btnLoginRegistrar.disabled=false;
		}
	}
}
function onLogin(myTesto){
	//login?0&Senha%20Incorreta%21%20Tentativa%202%20de%203
	var txtSenha = document.getElementById("txtSenha");
	var btnLoginRegistrar = document.getElementById("btnLoginRegistrar");
	var btnLoginEntrar = document.getElementById("btnLoginEntrar");
	var divLoginTesto = document.getElementById("divLoginTesto");
	partes = myTesto.split("?");
	if(partes.length==2 && partes[0]=="login"){
		document.body.style.cursor='default';
		prop = partes[1].split("&");
		if(prop.length>=3 && prop[0]=="1"){
			//alert("setEmail("+unescape(prop[1])+") setSexo("+prop[2]+")");
			usersXML.setEmail(unescape(prop[1]));
			usersXML.setSexo(prop[2]);
			//alert("getEmail("+usersXML.getEmail()+") getSexo("+usersXML.getSexo()+")");
			showForm('frmCharList');
			txtSenha.value="";
			divLoginTesto.style.display="none";
			btnLoginEntrar.disabled=true;
			btnLoginRegistrar.disabled=true;
		}else{
			if(prop.length==1){
				showAviso(divLoginTesto,"O servidor retornou uma resposta de login inválida!");
			}else if(prop.length>=2){
				showAviso(divLoginTesto,unescape(prop[1]));
			}
			btnLoginEntrar.disabled=false;
			btnLoginRegistrar.disabled=false;
		}
	}
}
function onLogout(myTesto){
	//logout?you
	//logout?you&mensagem
	var btnLoginRegistrar = document.getElementById("btnLoginRegistrar");
	var btnLoginEntrar = document.getElementById("btnLoginEntrar");
	var btnCharListCancel = document.getElementById("btnCharListCancel");
	var divLoginTesto = document.getElementById("divLoginTesto");
	partes = myTesto.split("?");
	if(partes.length==2 && partes[0]=="logout"){
		prop = partes[1].split("&");
		if(prop[0].toString().toLowerCase()=="you"){
			if(prop.length>=2){
				divLoginTesto.style.display="block";
				divLoginTesto.innerHTML = unescape(prop[1]);
			}else{
				divLoginTesto.style.display="none";
			}
			btnLoginEntrar.disabled=false;
			btnLoginRegistrar.disabled=false;
			btnCharListCancel.disabled=false;
			clearTimeout(loopPalco);
			loopPalco=null;
			document.getElementById("frmDialog").style.display="none";
			if(serverXML.getClientBackground().toString()!=""){ loadBackground(serverXML.getPath()+serverXML.getClientBackground()); }
			if(serverXML.getClientMusic().toString()!=""){ loadBmgByUrl(serverXML.getPath()+serverXML.getClientMusic()); }
			showForm('frmLogin');
			document.body.style.cursor='default';
		}
	}
}
function onListChars(myTesto){
	//listchars?1&Lunovox%2cM%2c2%2c1%2c1%2c1%2c0%2c0%2c0%2c0
	//listchars?1&Lunovox,M,2,1,1,1,0,0,0,0:Vanderson,M,0,0,1,1,0,0,0,0
	partes = myTesto.split("?");
	if(partes.length==2 && partes[0]=="listchars"){
		var btnCharListChangePassWord = document.getElementById("btnCharListChangePassWord");
		var btnCharListCancel = document.getElementById("btnCharListCancel");
		var btnCharListSelectChar = document.getElementsByName("btnCharListSelectChar");
		var btnCharListDeleteChar = document.getElementsByName("btnCharListDeleteChar");
		var btnCharListMakerChar = document.getElementsByName("btnCharListMakerChar");
		var divCharListTesto = document.getElementById("divCharListTesto");
		btnCharListChangePassWord.disabled=false;
		btnCharListCancel.disabled=false;
		for($i=0;$i<btnCharListMakerChar.length;$i++){
			//btnCharListSelectChar[$i].disabled=false;
			btnCharListDeleteChar[$i].disabled=false;
			btnCharListMakerChar[$i].disabled=false;
		}
		if(partes[1].split("&").length==2){
			$myCont = unescape(partes[1].split("&")[0]);
			$myChars = unescape(partes[1].split("&")[1]);
			$myAvatars=$myChars.split(";");
			//alert($myCont+"/"+$myChars);
			if($myCont>=1){
				if($myAvatars.length>=1 && $myAvatars.length==$myCont){
					showChars($myAvatars);
				}
			}else{
				showChars("");
			}
		}else{
			showChars("");
		}
		document.body.style.cursor='default';
	}
}
function onNewPass(myTesto){
	var divCharListTesto = document.getElementById("divCharListTesto");
	var divChangePassWord = document.getElementById("divChangePassWord");
	var btnCharListChangePassWord = document.getElementById("btnCharListChangePassWord");
	var btnCharListCancel = document.getElementById("btnCharListCancel");
	var btnCharListSelectChar = document.getElementsByName("btnCharListSelectChar");
	var btnCharListDeleteChar = document.getElementsByName("btnCharListDeleteChar");
	var btnCharListMakerChar = document.getElementsByName("btnCharListMakerChar");
	partes = myTesto.split("?");
	if(partes.length==2 && partes[0]=="newpass"){
		document.body.style.cursor='default';
		prop = partes[1].split("&");
		if(prop[0].toString().toLowerCase()=="1"){
			if(prop.length==1){
				showAviso(divCharListTesto,"Senha alterada com sucesso!");
				showForm('frmCharList');
			}else if(prop.length>=2){
				showAviso(divCharListTesto,unescape(prop[1]));
				showForm('frmCharList');
			}
			showAviso(divChangePassWord,"");
		}else{
			if(prop.length==1){
				showAviso(divChangePassWord,"Falha do servidor ao alterar sua senha!");
				showForm('frmChangePassWord');
			}else if(prop.length>=2){
				showAviso(divChangePassWord,unescape(prop[1]));
				showForm('frmChangePassWord');
			}
			showAviso(divCharListTesto,"");
		}
		btnCharListChangePassWord.disabled=false;
		btnCharListCancel.disabled=false;
		for($i=0;$i<btnCharListMakerChar.length;$i++){
			btnCharListSelectChar[$i].disabled=false;
			btnCharListDeleteChar[$i].disabled=false;
			btnCharListMakerChar[$i].disabled=false;
		}
	}
}
function onNewChar(myTesto){
	var divCharListTesto = document.getElementById("divCharListTesto");
	var divCharMakerTesto = document.getElementById("divCharMakerTesto");
	partes = myTesto.split("?");
	if(partes.length==2 && partes[0]=="newchar"){
		//alert(unescape(myTesto));
		prop = partes[1].split("&");
		if(prop[0]=="1"){
			if(prop.length==1){
				showAviso(divCharListTesto,"Avatar criado com sucesso!");
				showForm('frmCharList');
			}else if(prop.length>=2){
				showAviso(divCharListTesto,unescape(prop[1]));
				showForm('frmCharList');
			}
			showAviso(divCharMakerTesto,"");
		}else{
			if(prop.length==1){
				showAviso(divCharMakerTesto,"Falha do servidor ao criar seu avatar!");
				showForm('frmCharMaker');
			}else if(prop.length>=2){
				showAviso(divCharMakerTesto,unescape(prop[1]));
				showForm('frmCharMaker');
			}
			showAviso(divCharListTesto,"");
		}
		document.body.style.cursor='default';
	}
}
function onDelChar(myTesto){
	var divCharListTesto = document.getElementById("divCharListTesto");
	var divCharListTesto = document.getElementById("divCharListTesto");
	var btnCharListChangePassWord = document.getElementById("btnCharListChangePassWord");
	var btnCharListCancel = document.getElementById("btnCharListCancel");
	var btnCharListSelectChar = document.getElementsByName("btnCharListSelectChar");
	var btnCharListDeleteChar = document.getElementsByName("btnCharListDeleteChar");
	var btnCharListMakerChar = document.getElementsByName("btnCharListMakerChar");
	partes = myTesto.split("?");
	if(partes.length==2 && partes[0]=="delchar"){
		prop = partes[1].split("&");
		if(prop.length>=2){
			showAviso(divCharListTesto,unescape(prop[1]));
		}else if(prop.length==1 && prop=="1"){
			showAviso(divCharListTesto,"Avatar apagado com sucesso!");
		}else{
			showAviso(divCharListTesto,"Falha do servidor ao apagar seu avatar!");
		}
		document.body.style.cursor='default';
	}
}
function onSelChar(myTesto){
	var divCharListTesto = document.getElementById("divCharListTesto");
	var btnCharListChangePassWord = document.getElementById("btnCharListChangePassWord");
	var btnCharListCancel = document.getElementById("btnCharListCancel");
	var btnCharListSelectChar = document.getElementsByName("btnCharListSelectChar");
	var btnCharListDeleteChar = document.getElementsByName("btnCharListDeleteChar");
	var btnCharListMakerChar = document.getElementsByName("btnCharListMakerChar");
	
	//selchar?1&Lunovox&99&0:0,0:S:S&1&0&30/30&30/30&3:1,12:1,13:1&12,13
	partes = myTesto.split("?");
	if(partes.length==2 && partes[0]=="selchar"){
		prop = partes[1].split("&");
		
		if(prop.length<=2){
			if(prop.length==1){
				showAviso(divCharListTesto,"Falha do servidor ao selecionar seu avatar!");
			}else if(prop.length==2 && prop[1]!=""){
				showAviso(divCharListTesto,unescape(prop[1]));
			}
			showForm("frmCharList");
			if(serverXML.getClientBackground().toString()!=""){ loadBackground(serverXML.getPath()+serverXML.getClientBackground()); }
			if(serverXML.getClientMusic().toString()!=""){ loadBmgByUrl(serverXML.getPath()+serverXML.getClientMusic()); }
		}else{
			setAtributos(partes[1]); //← Está em ClassAtributos.js
			
			showAviso(divCharListTesto,"");
			showForm("");
			loadBackground("");
			startEngine();
			
		}
		btnCharListChangePassWord.disabled=false;
		btnCharListCancel.disabled=false;
		for($i=0;$i<btnCharListMakerChar.length;$i++){
			btnCharListSelectChar[$i].disabled=false;
			btnCharListDeleteChar[$i].disabled=false;
			btnCharListMakerChar[$i].disabled=false;
		}
		document.body.style.cursor='default';
	}/**/
}
function onSay(myTesto){
	if(loopPalco!=0){
		var frmDialog = document.getElementById("frmDialog");
		var divDialog = document.getElementById("divDialog");
		
		var partes = myTesto.split("?");
		if(partes.length==2 && partes[0]=="say"){
			var prop = partes[1].split("&");
			if(prop.length<=2){
				var $quem = prop[0];
				var $MensagemNova = unescape(prop[1]);
				var $final = (divDialog.scrollTop == divDialog.scrollHeight?true:divDialog.scrollHeight);
				var $MensagemAnterior = divDialog.innerHTML;
				//document.title = $quem+": "+$MensagemNova;
				document.title = $quem+": "+stripTags($MensagemNova);
				divDialog.innerHTML = $MensagemAnterior +"<br/>"+
				//"<font color='#888888' size='1'>(2012-08-29 15:03/02)</font> "+
				"<font color='#888888' size='1'>("+getAgora()+")</font> "+
				"<font color='FFFF00'><b>"+$quem+":</b></font> "+$MensagemNova+"";
				//alert($final);
				//document.title=$final;
				if($final==true){
					divDialog.scrollTop = divDialog.scrollHeight;
				}else{
					divDialog.scrollTop = $final;
				}
			}
		}
	}
}
function onBroadCast(myTesto){
	if(loopPalco!=0){
		var frmDialog = document.getElementById("frmDialog");
		var divDialog = document.getElementById("divDialog");
		
		var partes = myTesto.split("?");
		if(partes.length==2 && partes[0]=="broadcast"){
			//→ /broadcast Quem estiver interessando em colaborar com o desenvolvimento do jogo mande um e-mail para <a href="mailto:lunovox@tuatec.com.br">lunovox@tuatec.com.br</a>!
			var prop = partes[1].split("&");
			if(prop.length<=2){
				var $quem = prop[0];
				var $MensagemNova = unescape(prop[1]);
				var $final = (divDialog.scrollTop == divDialog.scrollHeight?true:divDialog.scrollHeight);
				var $MensagemAnterior = divDialog.innerHTML;
				//document.title = $quem+": "+$MensagemNova;
				document.title = $quem+": "+stripTags($MensagemNova);
				divDialog.innerHTML = $MensagemAnterior +"<br/>"+
				//"<font color='#888888' size='1'>(2012-08-29 15:03/02)</font> "+
				"<div style=\"background-color:#FFFF88; padding:5px 5px 5px 5px;\">"+
				"<font color='#888888' size='1'>("+getAgora()+")</font> "+
				"<font color='FF0000'>Anuncio geral de <b><u>"+$quem+"</u>:</b></font><br/> "+
				"<font color='#000000' size='2'>"+$MensagemNova+"</font>"+
				"</div>";
				//alert($final);
				//document.title=$final;
				if($final==true){
					divDialog.scrollTop = divDialog.scrollHeight;
				}else{
					divDialog.scrollTop = $final;
				}
			}
		}
	}
}
function onAnnounce(myTesto){
	if(loopPalco!=0){
		var frmDialog = document.getElementById("frmDialog");
		var divDialog = document.getElementById("divDialog");
		
		var partes = myTesto.split("?");
		if(partes.length==2 && partes[0]=="announce" && partes[1]!=""){
			// → /announce Quem estiver interessando em colaborar com o desenvolvimento do jogo mande um e-mail para <a href="mailto:lunovox@tuatec.com.br">lunovox@tuatec.com.br</a>!
			var $MensagemNova = unescape(partes[1]);
			//var $MensagemNova = unescape(prop[0]);
			var $final = (divDialog.scrollTop == divDialog.scrollHeight?true:divDialog.scrollHeight);
			var $MensagemAnterior = divDialog.innerHTML;
			//document.title = $MensagemNova;
			document.title = stripTags($MensagemNova);
			divDialog.innerHTML = $MensagemAnterior +"<br/>"+
			//"<font color='#888888' size='1'>(2012-08-29 15:03/02)</font> "+
			"<div style=\"background-color:#FFFF88; padding:5px 5px 5px 5px;\">"+
			//"<font color='#888888' size='1'>("+getAgora()+")</font> "+
			//"<font color='FF0000'>Anuncio geral:</b></font><br/> "+
			"<font color='000000' size='2'>"+$MensagemNova+"</font>"+
			"</div>";
			if($final==true){
				divDialog.scrollTop = divDialog.scrollHeight;
			}else{
				divDialog.scrollTop = $final;
			}
			
		}
	}
}