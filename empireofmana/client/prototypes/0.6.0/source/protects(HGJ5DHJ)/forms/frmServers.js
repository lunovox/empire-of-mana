function onBtnConectar(){
	var divConectorTesto = document.getElementById("divConectorTesto");
	var slcServidores = document.getElementById("slcServidores");
	if(slcServidores.selectedIndex>=0){
		document.body.style.cursor='wait';
		var divLoginTesto = document.getElementById("divLoginTesto");
		var btnConectorConectar = document.getElementById("btnConectorConectar");
		var btnConectorDestacar = document.getElementById("btnConectorDestacar");
		showAviso(divConectorTesto,"<b>Conectando!</b> Espere um momento...");
		slcServidores.disabled=true;
		btnConectorConectar.disabled=true;
		btnConectorDestacar.disabled=true;
		serverXML = new ClassServerXML(slcServidores.value);
		if(serverXML.getWebSocket().toString()!=""){
			wSocket = new WebSocket(serverXML.getWebSocket());
//################################################################################################################################################
			wSocket.onopen = function(){
				showAviso(divConectorTesto,"Esperando resposta do servidor...");
				document.body.style.cursor='wait';
				slcServidores.disabled=true;
				btnConectorConectar.disabled=true;
				btnConectorDestacar.disabled=true;
				wSocket.send("conect?me");
				ConectTimeOut=setTimeout("onBtnLoginCancelar()",30000); // 30.000ms (milisegundos) = 30s
			};
//################################################################################################################################################
			wSocket.onclose = onTimeOut;
			wSocket.onerror = onTimeOut;
//################################################################################################################################################
			wSocket.onmessage = function(evt){
				detectComand(evt.data);
			};
		}
	}else{
		divConectorTesto.innerHTML = "Selecione um servidor abaixo!!!";
		divConectorTesto.style.display="block";
	}
}
function onBtnDestacarJanela(){
	destacado=true;
	loadBackground(); loadMusic(); 
	showForm(''); 
	abrirJanela('../index.php',760,480);
}
function onTimeOut(){
	clearTimeout(ConectTimeOut);
	wSocket.close();
	showAviso(divConectorTesto,"Conexão com este servidor está fechada!");
	showForm("frmConector");
	slcServidores.disabled=false;
	btnConectorConectar.disabled=false;
	btnConectorDestacar.disabled=false;
	document.body.style.cursor='default';
}