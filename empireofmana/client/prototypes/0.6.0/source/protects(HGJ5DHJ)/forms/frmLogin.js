function onBtnLoginCancelar(){
	wSocket.close();
}
function onBtnLoginEntrar(){
	var divLoginTesto = document.getElementById("divLoginTesto");
	var divCharListTesto = document.getElementById("divCharListTesto");
	var btnLoginRegistrar = document.getElementById("btnLoginRegistrar");
	var btnLoginEntrar = document.getElementById("btnLoginEntrar");
	var txtLogin = document.getElementById("txtLogin");
	var txtSenha = document.getElementById("txtSenha");
	btnLoginEntrar.disabled=true;
	btnLoginRegistrar.disabled=true;
	if(txtLogin.value!=""){
		//alert(txtLogin.value+" → "+ifFormatMail(txtLogin.value));
		if(ifFormatMail(txtLogin.value)){
			if(txtSenha.value!=""){
				wSocket.send("login?"+ /*getUserIP()+"&"+*/ escape(txtLogin.value)+"&"+escape(txtSenha.value)+"");
				showAviso(divCharListTesto,"");
			}else{
				showAviso(divLoginTesto,"Favor digite sua Senha!");
				btnLoginEntrar.disabled=false;
				btnLoginRegistrar.disabled=false;
			}	
		}else{
			showAviso(divLoginTesto,"Favor digite seu <font color='#FFFF00'><b>Email Verdadeiro</b></font> !");
			btnLoginEntrar.disabled=false;
			btnLoginRegistrar.disabled=false;
		}
	}else{
		showAviso(divLoginTesto,"Favor digite seu Email!");
		btnLoginEntrar.disabled=false;
		btnLoginRegistrar.disabled=false;
	}
}
function onBtnLoginResgistrar(){
	//usersXML = serverXML.getUsersXML();
	if(
		parseInt(serverXML.getClientFrmRegisterX())>=0 && parseInt(serverXML.getClientFrmRegisterX())<640 && 
		parseInt(serverXML.getClientFrmRegisterY())>=0 && parseInt(serverXML.getClientFrmRegisterY())<480
	){
		moveForm(
			document.getElementById("frmRegister"),
			parseInt(serverXML.getClientFrmRegisterX()),
			parseInt(serverXML.getClientFrmRegisterY())
		);
		//alert(parseInt(serverXML.getFrmLoginX())+"/"+parseInt(serverXML.getFrmLoginY()));
	}
	if(document.getElementById("txtLogin").value!="") document.getElementById("txtRegisterEmail").value = document.getElementById("txtLogin").value;
	if(document.getElementById("txtSenha").value!="") document.getElementById("txtRegisterSenha").value = document.getElementById("txtSenha").value;
	
	drawCharMakerSelects();
	document.getElementById("chkContractAceito").checked=false;
	showAviso(divContractTesto,"");
	showForm('frmContract');
}
