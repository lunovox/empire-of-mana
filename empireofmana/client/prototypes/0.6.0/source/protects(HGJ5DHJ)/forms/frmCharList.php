<table id="frmCharList" class="painel" style="left:30px; top:120px;">
	<tr>
		<td colspan="6" class="titleForms"><b>AVATARES</b></td>
	</tr>
	<tr>
		<td colspan="6" align="center">
			<div id="divCharListTesto" class="aviso"></div>
		</td>
	</tr>
	<tr>
		<td align="center" valign="top">
			<center>
				<canvas name="cnvCharListAvatar" class="canvasAvatar" width="136" height="157"></canvas><br/>
				<div style="padding-top:5px; white-space:nowrap;">
					<button name="btnCharListMakerChar" onclick="onBtnCharListMakerChar();" style="width:136" >Criar Avatar</button>
					<button name="btnCharListSelectChar" onclick="onBtnCharListSelectChar(0);" style="display:none">Escolher</button>
					<button name="btnCharListDeleteChar" onclick="onBtnCharListDeleteChar(0);" style="display:none">Excluir</button>
				</div>
			</center>
			<div name="divCharListDescrition" style="display:none" align="left"></div>
		</td>
		<td align="center" valign="top">
			<center>
				<canvas name="cnvCharListAvatar" class="canvasAvatar" width="136" height="157"></canvas><br/>
				<div style="padding-top:5px; white-space:nowrap;">
					<button name="btnCharListMakerChar" onclick="onBtnCharListMakerChar();" style="width:136" >Criar Avatar</button>
					<button name="btnCharListSelectChar" onclick="onBtnCharListSelectChar(1);" style="display:none">Escolher</button>
					<button name="btnCharListDeleteChar" onclick="onBtnCharListDeleteChar(1);" style="display:none">Excluir</button>
				</div>
			</center>
			<div name="divCharListDescrition" style="display:none" align="left"></div>
		</td>
		<td align="center" valign="top">
			<center>
				<canvas name="cnvCharListAvatar" class="canvasAvatar" width="136" height="157"></canvas><br/>
				<div style="padding-top:5px; white-space:nowrap;">
					<button name="btnCharListMakerChar" onclick="onBtnCharListMakerChar();" style="width:136" >Criar Avatar</button>
					<button name="btnCharListSelectChar" onclick="onBtnCharListSelectChar(2);" style="display:none">Escolher</button>
					<button name="btnCharListDeleteChar" onclick="onBtnCharListDeleteChar(2);" style="display:none">Excluir</button>
				</div>
			</center>
			<div name="divCharListDescrition" style="display:none" align="left"></div>
		</td>
		<td align="center" valign="top">
			<center>
				<canvas name="cnvCharListAvatar" class="canvasAvatar" width="136" height="157"></canvas><br/>
				<div style="padding-top:5px; white-space:nowrap;">
					<button name="btnCharListMakerChar" onclick="onBtnCharListMakerChar();" style="width:136" >Criar Avatar</button>
					<button name="btnCharListSelectChar" onclick="onBtnCharListSelectChar(3);" style="display:none">Escolher</button>
					<button name="btnCharListDeleteChar" onclick="onBtnCharListDeleteChar(3);" style="display:none">Excluir</button>
				</div>
			</center>
			<div name="divCharListDescrition" style="display:none" align="left"></div>
		</td>
	</tr>
	<tr>
		<td colspan="6" align="center" style="background:rgba(255,255,255,0.05); border-radius:5; padding:5px 5px 5px 5px;">
			<button id="btnCharListChangePassWord"
				onclick="onBtnCharListChangePassWord();"
			>Trocar Senha</button>
			<button id="btnCharListCancel"
				onclick="btnCharListCancel();"
			>Trocar Login</button>
		</td>
	</tr>
</table>	