function onBtnServidoresConectar(){
	var divServidoresTesto = document.getElementById("divServidoresTesto");
	var slcServidoresLista = document.getElementById("slcServidoresLista");
	if(slcServidoresLista.selectedIndex>=0){
		document.body.style.cursor='wait';
		var divLoginTesto = document.getElementById("divLoginTesto");
		var btnServidoresConectar = document.getElementById("btnServidoresConectar");
		var btnServidoresAnexar = document.getElementById("btnServidoresAnexar");
		var btnServidoresDestacar = document.getElementById("btnServidoresDestacar");
		showAviso(divServidoresTesto,"<b>Conectando!</b> Espere um momento...");
		slcServidoresLista.disabled=true;
		btnServidoresConectar.disabled=true;
		btnServidoresAnexar.disabled=true;
		btnServidoresDestacar.disabled=true;
		serverXML = new ClassServerXML(slcServidoresLista.value);
		if(serverXML.getWebSocket().toString()!=""){
			myMaps= new getListMaps(); //← Importa todos os mapas de "maps/list.xml"
			//alert(serverXML.getWebSocket());
			//alert(serverXML.getPath());
			//alert(serverXML.getURL());
			wSocket = new WebSocket(serverXML.getWebSocket());
//################################################################################################################################################
			wSocket.onopen = function(){
				showAviso(divServidoresTesto,"Esperando resposta do servidor...");
				document.body.style.cursor='wait';
				slcServidoresLista.disabled=true;
				btnServidoresConectar.disabled=true;
				btnServidoresAnexar.disabled=true;
				btnServidoresDestacar.disabled=true;
				//wSocket.send("conect?me");//Não precisa pedir para se conectar.
				ConectTimeOut=setTimeout("onBtnLoginCancelar()",15000); // 15.000ms(milisegundos)=5s
			};
//################################################################################################################################################
			wSocket.onclose = onTimeOut;
			wSocket.onerror = onTimeOut;
//################################################################################################################################################
			wSocket.onmessage = function(evt){
				detectComand(evt.data);
			};
		}else{
			showAviso(divServidoresTesto,"Não foi possível encontrar as configurações <b><a target='_blank' href='"+slcServidoresLista.value+"'><font color='#FFFF00'>deste servidor</font></a>!</b>");
			//showForm("frmServidores");
			slcServidoresLista.disabled=false;
			btnServidoresConectar.disabled=false;
			btnServidoresAnexar.disabled=false;
			btnServidoresDestacar.disabled=false;
			document.body.style.cursor='default';
		}
	}else{
		//showAviso(divServidoresTesto,"<marquee>Selecione um servidor abaixo!!!</marquee>");
		showAviso(divServidoresTesto,"Selecione um servidor abaixo!!!");
	}
}
function onBtnServidoresDestacar(){
	destacado=true;
	loadBackground(); loadMusic(); 
	showForm(''); 
	abrirJanela('../index.php',760,480);
}
function onBtnServidoresAnexar(){
	var divAnexosTesto = document.getElementById("divAnexosTesto");
	var dibAnexoSiteEndereco = document.getElementById("dibAnexoSiteEndereco");
	var dibAnexoSiteCodigo = document.getElementById("dibAnexoSiteCodigo");
	var txtAnexosSiteEndereco = document.getElementById("txtAnexosSiteEndereco");
	showAviso(divAnexosTesto,"");
	dibAnexoSiteEndereco.style.display="block";
	dibAnexoSiteCodigo.style.display="none";
	txtAnexosSiteEndereco.value="";
	showForm("frmAnexos");
}
function onTimeOut(){
	var slcServidoresLista = document.getElementById("slcServidoresLista");
	var btnServidoresConectar = document.getElementById("btnServidoresConectar");
	var btnServidoresAnexar = document.getElementById("btnServidoresAnexar");
	var btnServidoresDestacar = document.getElementById("btnServidoresDestacar");
	clearTimeout(loopPalco);
	clearTimeout(ConectTimeOut);
	loopPalco=null;
	wSocket.close();
	document.getElementById("frmDialog").style.display="none";
	//showAviso(divServidoresTesto,"<marquee>Conexão com este servidor está fechada!</marquee>");
	showAviso(divServidoresTesto,"Conexão com este servidor está fechada!");
	showForm("frmServidores");
	slcServidoresLista.disabled=false;
	btnServidoresConectar.disabled=false;
	btnServidoresAnexar.disabled=false;
	btnServidoresDestacar.disabled=false;
	document.body.style.cursor='default';
}