function drawCharMakerSelects(){
	if(usersXML!=null && usersXML.getURL().toString()!=""){
		var slcCharMakerRace = document.getElementById("slcCharMakerRace");
		var slcCharMakerClass = document.getElementById("slcCharMakerClass");
		var slcCharMakerHair = document.getElementById("slcCharMakerHair");
		var slcCharMakerEye = document.getElementById("slcCharMakerEye");
		var slcCharMakerNose = document.getElementById("slcCharMakerNose");
		var slcCharMakerLip = document.getElementById("slcCharMakerLip");
		var slcCharMakerScar = document.getElementById("slcCharMakerScar");
		var slcCharMakerHairColor = document.getElementById("slcCharMakerHairColor");
		var DefaultClasses = usersXML.getClasses();
		var GenericAvatars = usersXML.getAvatars();
		//var radRegisterSex = document.getElementsByName("radRegisterSex")[0];
		
		optionRemoveAll(slcCharMakerRace);
		for(var i=0; i<GenericAvatars.races.race.length; i++){optionAdd(slcCharMakerRace,GenericAvatars.races.race[i].name, i);}
		
		optionRemoveAll(slcCharMakerClass);
		for(var c=0; c<DefaultClasses.class.length; c++){optionAdd(slcCharMakerClass,DefaultClasses.class[c].name, c);}
		
		optionRemoveAll(slcCharMakerHair);
		for(var i=0; i<GenericAvatars.hairs.hair.length; i++){optionAdd(slcCharMakerHair,GenericAvatars.hairs.hair[i].name, i);}
		
		optionRemoveAll(slcCharMakerEye);
		for(var i=0; i<GenericAvatars.eyes.eye.length; i++){optionAdd(slcCharMakerEye,GenericAvatars.eyes.eye[i].name, i);}
		
		optionRemoveAll(slcCharMakerNose);
		for(var i=0; i<GenericAvatars.noses.nose.length; i++){optionAdd(slcCharMakerNose,GenericAvatars.noses.nose[i].name, i);}
		
		optionRemoveAll(slcCharMakerLip);
		for(var i=0; i<GenericAvatars.lips.lip.length; i++){optionAdd(slcCharMakerLip,GenericAvatars.lips.lip[i].name, i);}
		
		optionRemoveAll(slcCharMakerScar);
		for(var i=0; i<GenericAvatars.scars.scar.length; i++){optionAdd(slcCharMakerScar,GenericAvatars.scars.scar[i].name, i);}
		
		optionRemoveAll(slcCharMakerHairColor);
		for(var i=0; i<GenericAvatars.haircolors.colornew.length; i++){optionAdd(slcCharMakerHairColor,GenericAvatars.haircolors.colornew[i].name, i);}
		
		onBtnCharMakerDrawAvatar();
	}
}
function onBtnCharMakerDrawAvatar(){
	var cnvCharMakerAvatar = document.getElementById("cnvCharMakerAvatar");
	var ctxCharMakerAvatar = cnvCharMakerAvatar.getContext('2d');
	var radRegisterSex = document.getElementsByName("radRegisterSex");
	var slcCharMakerRace = document.getElementById("slcCharMakerRace");
	var slcCharMakerClass = document.getElementById("slcCharMakerClass");
	var slcCharMakerHair = document.getElementById("slcCharMakerHair");
	var slcCharMakerEye = document.getElementById("slcCharMakerEye");
	var slcCharMakerNose = document.getElementById("slcCharMakerNose");
	var slcCharMakerLip = document.getElementById("slcCharMakerLip");
	
	drawAvatar(
		cnvCharMakerAvatar,
		ctxCharMakerAvatar,
		usersXML.getSexo(),
		slcCharMakerRace.selectedIndex,
		slcCharMakerClass.selectedIndex,
		slcCharMakerHair.selectedIndex,
		slcCharMakerHairColor.selectedIndex,
		slcCharMakerEye.selectedIndex,
		slcCharMakerNose.selectedIndex,
		slcCharMakerLip.selectedIndex,
		0
	);
}
function onBtnCharMakerCancel(){
	showForm('frmCharList');
}
function onBtnCharMakerNewChar(){
	var divCharListTesto = document.getElementById("divCharListTesto");
	var divCharMakerTesto = document.getElementById("divCharMakerTesto");
	
	var btnCharListChangePassWord = document.getElementById("btnCharListChangePassWord");
	var btnCharListCancel = document.getElementById("btnCharListCancel");
	var btnCharListSelectChar = document.getElementsByName("btnCharListSelectChar");
	var btnCharListDeleteChar = document.getElementsByName("btnCharListDeleteChar");
	var btnCharListMakerChar = document.getElementsByName("btnCharListMakerChar");
	
	var txtCharMakerNick = document.getElementById("txtCharMakerNick");
	var slcCharMakerRace  = document.getElementById("slcCharMakerRace");
	var slcCharMakerClass  = document.getElementById("slcCharMakerClass");
	var slcCharMakerHair  = document.getElementById("slcCharMakerHair");
	var slcCharMakerHairColor  = document.getElementById("slcCharMakerHairColor");
	var slcCharMakerEye  = document.getElementById("slcCharMakerEye");
	var slcCharMakerNose  = document.getElementById("slcCharMakerNose");
	var slcCharMakerLip  = document.getElementById("slcCharMakerLip");
	var slcCharMakerScar  = document.getElementById("slcCharMakerScar");
	
	if((txtCharMakerNick.value = toTrim(txtCharMakerNick.value))!=""){
		if(txtCharMakerNick.value.toString().toLowerCase().indexOf(" ")<=0){
			btnCharListChangePassWord.disabled=true;
			btnCharListCancel.disabled=true;
			for($i=0;$i<btnCharListMakerChar.length;$i++){
				btnCharListSelectChar[$i].disabled=true;
				btnCharListDeleteChar[$i].disabled=true;
				btnCharListMakerChar[$i].disabled=true;
			}
			showAviso(divCharListTesto,"Criando! Espere um momento...");
			document.body.style.cursor='wait';
			var protocolo = (
				"newchar?"+
				escape(txtCharMakerNick.value)+"&"+
				slcCharMakerRace.value+"&"+
				slcCharMakerClass.value+"&"+
				slcCharMakerHair.value+"&"+
				slcCharMakerHairColor.value+"&"+
				slcCharMakerEye.value+"&"+
				slcCharMakerNose.value+"&"+
				slcCharMakerLip.value+"&"+
				slcCharMakerScar.value
			);
			showForm('frmCharList');
			wSocket.send(protocolo);
		}else{
			showAviso(divCharMakerTesto,"Seu apelido deve conter apenas uma palavra!");
			txtCharMakerNick.focus();
		}
	}else{
		showAviso(divCharMakerTesto,"Favor digite seu Apelido!");
		txtCharMakerNick.focus();
	}
}