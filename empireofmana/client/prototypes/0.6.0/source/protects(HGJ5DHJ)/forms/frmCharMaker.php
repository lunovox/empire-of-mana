<table id="frmCharMaker" class="painel" style="left:65px; top:100px;">
	<tr>
		<td colspan="3" class="titleForms"><b>CRIANDO AVATAR</b></td>
	</tr>
	<tr>
		<td align="center" colspan="4">
			<div id="divCharMakerTesto" class="aviso"></div>
		</td>
	</tr>
	<tr>
		<td align="right"><font face="arial" color="White" size="2"><b>Apelido:</b></font></td>
		<td><input id="txtCharMakerNick" type="text" maxlength="30" style="width:200;" value_="Lunovox"
		/></td>
		<td align="center" valign="top" rowspan="10">
			<canvas id="cnvCharMakerAvatar" width="181" height="209" class="canvasAvatar"></canvas>
		</td>
	</tr>
	<tr>
		<td align="right"><font face="arial" color="White" size="2"><b>Raça:</b></font></td>
		<td>
			<select id="slcCharMakerRace" style="width:200;"
				onchange="onBtnCharMakerDrawAvatar();" onclick="onBtnCharMakerDrawAvatar();"
			></select>
		</td>
	</tr>
	<tr>
		<td align="right"><font face="arial" color="White" size="2"><b>Classe:</b></font></td>
		<td>
			<select id="slcCharMakerClass" style="width:200;"
				onchange="onBtnCharMakerDrawAvatar();" onclick="onBtnCharMakerDrawAvatar();"
			></select>
		</td>
	</tr>
	<tr>
		<td align="right"><font face="arial" color="White" size="2"><b>Tipo de Cabelo:</b></font></td>
		<td>
			<select id="slcCharMakerHair" style="width:200;"
				onchange="onBtnCharMakerDrawAvatar();" onclick="onBtnCharMakerDrawAvatar();"
			></select>
		</td>
	</tr>
	<tr>
		<td align="right"><font face="arial" color="White" size="2"><b>Cor de Cabelo:</b></font></td>
		<td>
			<select id="slcCharMakerHairColor" style="width:200;"
				onchange="onBtnCharMakerDrawAvatar();" onclick="onBtnCharMakerDrawAvatar();"
			></select>
		</td>
	</tr>
	<tr>
		<td align="right"><font face="arial" color="White" size="2"><b>Olho:</b></font></td>
		<td>
			<select id="slcCharMakerEye" style="width:200;"
				onchange="onBtnCharMakerDrawAvatar();" onclick="onBtnCharMakerDrawAvatar();"
			></select>
		</td>
	</tr>
	<tr>
		<td align="right"><font face="arial" color="White" size="2"><b>Nariz:</b></font></td>
		<td>
			<select id="slcCharMakerNose" style="width:200;"
				onchange="onBtnCharMakerDrawAvatar();" onclick="onBtnCharMakerDrawAvatar();"
			></select>
		</td>
	</tr>
	<tr>
		<td align="right"><font face="arial" color="White" size="2"><b>Boca:</b></font></td>
		<td>
			<select id="slcCharMakerLip" style="width:200;"
				onchange="onBtnCharMakerDrawAvatar();" onclick="onBtnCharMakerDrawAvatar();"
			></select>
		</td>
	</tr>
	<tr>
		<td align="right"><font face="arial" color="White" size="2"><b>Marca:</b></font></td>
		<td>
			<select id="slcCharMakerScar" style="width:200;"
				onchange="onBtnCharMakerDrawAvatar();" onclick="onBtnCharMakerDrawAvatar();"
			></select>
		</td>
	</tr>
	<tr>
		<td></td>
		<td align="center">
			<button id="btnCharMakerNewChar" 
				onclick="onBtnCharMakerNewChar();"
			>Criar</button>
			<button id="btnCharMakerCancel"  
				onclick="onBtnCharMakerCancel();"
			>Cancelar</button><br>
		</td>
	</tr>
</table>
