<table id="frmLogin" class="painel" style="left:100px; top:140px;">
	<tr>
		<td colspan="2" class="titleForms"><b>LOGIN</b></td>
	</tr>
	<tr>
		<td align="center" colspan="2">
			<div id="divLoginTesto" class="aviso"></div>
		</td>
	</tr>
	<tr>
		<td align="right"><font_ face="arial" color="White" size="2"><b>Email:<b></h4></td>
		<td><input id="txtLogin" type="text" maxlength="40" style="width:230px;"/></td>
	</tr>
	<tr>
		<td align="right"><font_ face="arial" color="White" size="2"><b>Senha:<b></font_></td>
		<td><input id="txtSenha" type="password" maxlength="32" style="width:230px;"/></td>
	</tr>
	<tr>
		<td align="center" colspan="2">
			<font face="arial" size="1pt" color="White">
				<div id="divLoginUsers"></div>
			</font>
		</td>
	</tr>
	<tr>
		<td align="right" colspan="2">
			<button id="btnLoginCancelar"
				onclick="onBtnLoginCancelar();"
				title="Fecha sua conexão com este servidor."
			>Fechar</button>
			<button id="btnLoginRegistrar" 
				onclick="onBtnLoginResgistrar();"
				title="Cria uma conta nova!"
			> <img src="images/icons/eom-16x16.png" align="absmiddle"> Registrar</button>
			<button disabled id="btnLoginResgatar" 
				title="Reenvia o email contendo o Painel de Usuário que recupera senha esquecida e autentica conta nova."
			>Resgatar</button>
			<button id="btnLoginEntrar" 
				onclick="onBtnLoginEntrar();"
				title="Tenta logar usando Email e Senha."
			> <img src="images/sword.gif" align="absmiddle"> Entrar</button>
		</td>
	</tr>
</table>
