// Version 3 - Basic collision (viewbox), perfectly elastic (no energy loss)
var canvas = document.getElementById("palco");
var ctx = canvas.getContext('2d');
var myScreen = {linhas:canvas.height/32, colunas:canvas.width/32};

var fps = 10;
var myTeclado = {btnNumero:0, btnTecla:"", btnCtrl:false, btnAlt:false, btnShift:false};
var myMouse = {x:0, y:0, c:0, l:0};
var seExaminar=false, seColisionVisible=false;
var contLoadImagens=0, MaxLoadImagens=2;
var mapa=null, sprite=null, sptChar=null;

document.onmousemove = onMouseMove;
document.onmousedown = onMouseDown;
document.onkeydown =   onKeyDown;
document.onkeypress =  onKeyPress;
//document.onkeypress =  onKeyPress(event);} // Não está funcionando do Chromium

function loadXML(url){
  //by Micox: micoxjcg@yahoo.com.br.
    if(window.XMLHttpRequest){
        var Loader = new XMLHttpRequest();
        Loader.open("GET", url ,false);
        Loader.send(null);
        return Loader.responseXML;
    }else if(window.ActiveXObject){
        var Loader = new ActiveXObject("Msxml2.DOMDocument.3.0");
        Loader.async = false;
        Loader.load(url);
        return Loader;
    }
}
function RascunhoDeCodigo(){
    /*var map={
        url:"z10.tmx",
        version:"1.0",
        orientation:"orthogonal",
        width:20,
        height:15,
        tilewidth:32,
        tileheight:32,
        tileset:[
            {
                firstgid:1,
                name:"collision",
                tilewidth:32,
                tileheight:32,
                image:"../tilesets/collision.png"
            },{
                firstgid:3,
                name:"woodland_ground",
                tilewidth:32,
                tileheight:32,
                image:"../tilesets/woodland_ground.png"
            }
        ],
        layer:[
            {
                name:"background1",
                width:20,
                height:15,
                opacity:1.0,
                data:[
                    [212,212,212,212,212,212,212,212,212,0  ,0  ,0  ,0  ,0  ,0  ,212,212,212,212,212],
                    [0  ,69 ,70 ,70 ,70 ,71 ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,69 ],
                    [0  ,85 ,125,86 ,86 ,87 ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,85 ],
                    [0  ,85 ,141,86 ,86 ,87 ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,85 ],
                    [0  ,85 ,86 ,86 ,86 ,87 ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,85 ],
                    [0  ,101,102,102,102,103,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,85 ],
                    [0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,85 ],
                    [0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,85 ],
                    [0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,85 ],
                    [0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,85 ],
                    [0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,85 ],
                    [0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,85 ],
                    [0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,85 ],
                    [0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,101],
                    [180,180,180,180,180,180,180,180,180,180,180,180,180,180,180,180,180,180,180,180]
                ]
            },{
                name:"collisions",
                width:20,
                height:15,
                opacity:0.74,
                data:[
                    [1  ,1  ,1  ,1  ,1  ,1  ,1  ,1  ,1  ,0  ,0  ,0  ,0  ,0  ,0  ,1  ,1  ,1  ,1  ,1  ],
                    [0  ,1  ,1  ,1  ,1  ,1  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,1  ],
                    [0  ,1  ,1  ,1  ,1  ,1  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,1  ],
                    [0  ,1  ,1  ,1  ,1  ,1  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,1  ],
                    [0  ,1  ,1  ,1  ,1  ,1  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,1  ],
                    [0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,1  ],
                    [0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,1  ],
                    [0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,1  ],
                    [0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,1  ],
                    [0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,1  ],
                    [0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,1  ],
                    [0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,1  ],
                    [0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,1  ],
                    [0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ],
                    [1  ,1  ,1  ,1  ,1  ,1  ,1  ,1  ,1  ,1  ,1  ,1  ,1  ,1  ,1  ,1  ,1  ,1  ,1  ,1  ]
                ]
            }
        ]
    };/**/
}
function loadMap(url){
    xmlNode = loadXML(url);
    for(var n=0;n<xmlNode.childNodes.length;n++){//percorrendo os filhos do nó
        novoMap=[];
        if(xmlNode.childNodes[n].nodeType == 1){//ignorar espaços em branco
            nodeMap = xmlNode.childNodes[n];
            if(nodeMap.nodeName=="map"){
                novoMap.url=url;
                for(var a=0;a<nodeMap.attributes.length;a++){
                    atrib = nodeMap.attributes[a];
                    if(atrib.nodeName=="version"){
                        novoMap.version=atrib.nodeValue;
                    }else if(atrib.nodeName=="orientation"){
                        novoMap.orientation=atrib.nodeValue;
                    }else if(atrib.nodeName=="width"){
                        novoMap.width=atrib.nodeValue;
                    }else if(atrib.nodeName=="height"){
                        novoMap.height=atrib.nodeValue;
                    }else if(atrib.nodeName=="tilewidth"){
                        novoMap.tilewidth=atrib.nodeValue;
                    }else if(atrib.nodeName=="tileheight"){
                        novoMap.tileheight=atrib.nodeValue;
                    }else if(atrib.nodeName=="tileset"){

                    }
                }

                novoMap.tileset=[];
                novoMap.layer=[];

                for(var t=0;t<nodeMap.childNodes.length;t++){
                    nodeProp = nodeMap.childNodes[t];
                    if(nodeProp.nodeType == 1){
                        if(nodeProp.nodeName=="tileset"){
                            contTile=novoMap.tileset.length;
                            novoMap.tileset[contTile]=[];
                            for(var a=0;a<nodeProp.attributes.length;a++){
                                var atrib = nodeProp.attributes[a];

                                if(atrib.nodeName=="firstgid"){
                                    novoMap.tileset[contTile].firstgid=atrib.nodeValue;
                                }else if(atrib.nodeName=="name"){
                                    novoMap.tileset[contTile].name=atrib.nodeValue;
                                }else if(atrib.nodeName=="tilewidth"){
                                    novoMap.tileset[contTile].tilewidth=atrib.nodeValue;
                                }else if(atrib.nodeName=="tilewidth"){
                                    novoMap.tileset[contTile].name=atrib.nodeValue;
                                }else if(nodeProp.childNodes.length>=1){
                                    for(var contImage=0;contImage<nodeProp.childNodes.length;contImage++){
                                        nodeImage = nodeProp.childNodes[contImage];
                                        if(nodeImage.nodeType == 1 && nodeImage.nodeName=="image"){
                                             for(var ai=0;ai<nodeImage.attributes.length;ai++){
                                                atribImage = nodeImage.attributes[ai];
                                                if(atribImage.nodeName=="source"){
                                                    novoMap.tileset[contTile].image=atribImage.nodeValue;
                                                }
                                            }

                                        }
                                    }
                                }
                            }
                        }else if(nodeProp.nodeName=="layer"){
                            contLayer=novoMap.layer.length;
                            novoMap.layer[contLayer]=[];
                            novoMap.layer[contLayer].opacity=1.0;
                            for(var al=0;al<nodeProp.attributes.length;al++){
                                atribLayer = nodeProp.attributes[al];
                                if(atribLayer.nodeName=="name"){
                                    novoMap.layer[contLayer].name=atribLayer.nodeValue;
                                    if(atribLayer.nodeValue="collision") novoMap.layercollision=contLayer;
                                }else if(atribLayer.nodeName=="opacity"){
                                    novoMap.layer[contLayer].opacity=atribLayer.nodeValue;
                                }else if(atribLayer.nodeName=="width"){
                                    novoMap.layer[contLayer].width=atribLayer.nodeValue;
                                }else if(atribLayer.nodeName=="height"){
                                    novoMap.layer[contLayer].height=atribLayer.nodeValue;
                                }
                            }
                            //document.write("novoMap.layer["+(contLayer)+"].name='"+novoMap.layer[contLayer].name+"'<br/>");

                            for(var np=0;np<nodeProp.childNodes.length;np++){
                                nodeData = nodeProp.childNodes[np];
                                if(nodeData.nodeType == 1){
                                    if(nodeData.nodeName=="data"){
                                        //document.write("nodeData.nodeName='"+nodeData.nodeName+"'<br/>");
                                        novoMap.layer[contLayer].data=[];
                                        for(var e=0;e<nodeData.childNodes.length;e++){
                                            nodeTile = nodeData.childNodes[e];
                                            if(nodeTile.nodeType == 1){
                                                if(nodeTile.nodeName=="tile"){
                                                    contData=novoMap.layer[contLayer].data.length;
                                                    for(var nd=0;nd<nodeTile.attributes.length;nd++){
                                                        atribTile=nodeTile.attributes[nd];
                                                        if(atribTile.nodeName=="gid"){
                                                            novoMap.layer[contLayer].data[contData]=atribTile.nodeValue;
                                                        }
                                                    }
                                                    //document.write("novoMap.layer["+(contLayer)+"].data["+contData+"]='"+novoMap.layer[contLayer].data[contData]+"'<br/>");
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    return novoMap;
}
function loadImagens(){
    sprite = {imagem:null, linhas:16, colunas:16};
    var imgBkg = new Image();
    imgBkg.src = "tilesets/woodland_ground.png";
    imgBkg.onload = onLoadImagem(sprite, imgBkg);

    sptChar = {
        imagem:null,
        linhas:8, colunas:9,
        c:10, l:7,
        x:canvas.width/2-16,
        y:canvas.height/2+8
    };
    var imgChar = new Image();
    imgChar.src = "sprites/body/player_white_male.png";
    imgChar.onload = onLoadImagem(sptChar, imgChar);/**/

} loadImagens();
function loadMaps(){
    /*mapa={
        url:"z10.tmx",
        version:"1.0",
        orientation:"orthogonal",
        layercollision:2,
        width:20,
        height:15,
        tilewidth:32,
        tileheight:32,
        tileset:[
            {
                firstgid:1,
                name:"collision",
                tilewidth:32,
                tileheight:32,
                image:"../tilesets/collision.png"
            },{
                firstgid:3,
                name:"woodland_ground",
                tilewidth:32,
                tileheight:32,
                image:"../tilesets/woodland_ground.png"
            }
        ],
        layer:[
            {
                name:"background1",
                width:20,
                height:15,
                opacity:1.0,
                data:[
                    [212,212,212,212,212,212,212,212,212,0  ,0  ,0  ,0  ,0  ,0  ,212,212,212,212,212],
                    [0  ,69 ,70 ,70 ,70 ,71 ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,69 ],
                    [0  ,85 ,125,86 ,86 ,87 ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,85 ],
                    [0  ,85 ,141,86 ,86 ,87 ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,85 ],
                    [0  ,85 ,86 ,86 ,86 ,87 ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,85 ],
                    [0  ,101,102,102,102,103,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,85 ],
                    [0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,85 ],
                    [0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,85 ],
                    [0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,85 ],
                    [0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,85 ],
                    [0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,85 ],
                    [0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,85 ],
                    [0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,85 ],
                    [0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,101],
                    [180,180,180,180,180,180,180,180,180,180,180,180,180,180,180,180,180,180,180,180]
                ]
            },{
                name:"collisions",
                width:20,
                height:15,
                opacity:0.74,
                data:[
                    [1  ,1  ,1  ,1  ,1  ,1  ,1  ,1  ,1  ,0  ,0  ,0  ,0  ,0  ,0  ,1  ,1  ,1  ,1  ,1  ],
                    [0  ,1  ,1  ,1  ,1  ,1  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,1  ],
                    [0  ,1  ,1  ,1  ,1  ,1  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,1  ],
                    [0  ,1  ,1  ,1  ,1  ,1  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,1  ],
                    [0  ,1  ,1  ,1  ,1  ,1  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,1  ],
                    [0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,1  ],
                    [0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,1  ],
                    [0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,1  ],
                    [0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,1  ],
                    [0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,1  ],
                    [0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,1  ],
                    [0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,1  ],
                    [0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,1  ],
                    [0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ],
                    [1  ,1  ,1  ,1  ,1  ,1  ,1  ,1  ,1  ,1  ,1  ,1  ,1  ,1  ,1  ,1  ,1  ,1  ,1  ,1  ]
                ]
            }
        ]

    };/**/
    //mapa=loadMap("maps/z10.tmx");
    mapa={
        background1:[
            [212,212,212,212,212,212,212,212,212,0  ,0  ,0  ,0  ,0  ,0  ,212,212,212,212,212], 
            [0  ,69 ,70 ,70 ,70 ,71 ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,69 ], 
            [0  ,85 ,125,86 ,86 ,87 ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,85 ], 
            [0  ,85 ,141,86 ,86 ,87 ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,85 ], 
            [0  ,85 ,86 ,86 ,86 ,87 ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,85 ], 
            [0  ,101,102,102,102,103,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,85 ], 
            [0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,85 ], 
            [0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,85 ], 
            [0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,85 ], 
            [0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,85 ], 
            [0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,85 ], 
            [0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,85 ], 
            [0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,85 ], 
            [0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,101], 
            [180,180,180,180,180,180,180,180,180,180,180,180,180,180,180,180,180,180,180,180]  
        ],
        colision:[
            [1  ,1  ,1  ,1  ,1  ,1  ,1  ,1  ,1  ,0  ,0  ,0  ,0  ,0  ,0  ,1  ,1  ,1  ,1  ,1  ], 
            [0  ,1  ,1  ,1  ,1  ,1  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,1  ], 
            [0  ,1  ,1  ,1  ,1  ,1  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,1  ], 
            [0  ,1  ,1  ,1  ,1  ,1  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,1  ], 
            [0  ,1  ,1  ,1  ,1  ,1  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,1  ], 
            [0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,1  ], 
            [0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,1  ], 
            [0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,1  ], 
            [0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,1  ], 
            [0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,1  ], 
            [0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,1  ], 
            [0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,1  ], 
            [0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,1  ], 
            [0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ,0  ], 
            [1  ,1  ,1  ,1  ,1  ,1  ,1  ,1  ,1  ,1  ,1  ,1  ,1  ,1  ,1  ,1  ,1  ,1  ,1  ,1  ]  
        ]
    }/**/
}loadMaps();

function init() {
    setInterval(update, 1000/fps);
}
function onKeyPress(event){
    myTeclado.btnNumero = event.keyCode;
    myTeclado.btnTecla = String.fromCharCode(event.charCode);
    myTeclado.btnCtrl = event.ctrlKey;
    myTeclado.btnAlt = event.altKey;
    myTeclado.btnShift = event.shiftKey;
}
function onKeyDown(event){
    myTeclado.btnNumero = event.keyCode;
    myTeclado.btnTecla = String.fromCharCode(event.charCode);
    c=Math.round((sptChar.x+32)/32)+1;
    l=Math.round((sptChar.y-32)/32);
    if(myTeclado.btnNumero == 27){ //ESC
        seColisionVisible = !seColisionVisible;
        seExaminar = !seExaminar;
    }else if(myTeclado.btnNumero == 40){ //abaixo
        sptChar.l+=1;
        if(sptChar.l>myScreen.linhas-1 || mapa.colision[sptChar.l][sptChar.c]) sptChar.l-=1;
    }else if(myTeclado.btnNumero == 38){ //acima
        sptChar.l-=1;
        if(sptChar.l<0 || mapa.colision[sptChar.l][sptChar.c]) sptChar.l+=1;
    }else if(myTeclado.btnNumero == 39){ //direita
        sptChar.c+=1;
        if(sptChar.c>myScreen.colunas-1 || mapa.colision[sptChar.l][sptChar.c]) sptChar.c-=1;
    }else if(myTeclado.btnNumero == 37){ //esqueda
        sptChar.c-=1;
        if(sptChar.c<0 || mapa.colision[sptChar.l][sptChar.c]) sptChar.c+=1;
    }
}
function onMouseDown(event){
    onMouseMove(event);
    if(!mapa.colision[myMouse.l][myMouse.c]){
        sptChar.c=myMouse.c;
        sptChar.l=myMouse.l;
    }
}
function onMouseMove(event){
    if (event.layerX || event.layerX == 0) { // Firefox
        var x = event.layerX-canvas.offsetLeft;
        var y = event.layerY-canvas.offsetTop;
    } else if (event.offsetX || event.offsetX == 0) { // Opera
        var x = event.offsetX-canvas.offsetLeft;
        var y = event.offsetY-canvas.offsetTop;
    }
    var c = Math.round(x/32)-1;
    var l = Math.round(y/32)-1;
    if(c>=0 && c<=myScreen.colunas-1 && l>=0 && l<=myScreen.linhas-1){
        myMouse.x=x; myMouse.y=y;
        myMouse.c=c; myMouse.l=l;
    }
}
function onLoadImagem(mySprite, myImagem){
    mySprite.imagem=myImagem;
    //mySprite.linhas=mySprite.imagem.height/blkHeight;
    //mySprite.colunas=mySprite.imagem.width/blkWidth;
    contLoadImagens++;
    if(contLoadImagens>=MaxLoadImagens) init();
}
function printTesto(myTesto, X, Y) {
    printTesto(myTesto, X, Y, "", "", "");
}
function printTesto(myTesto, X, Y, myFonte) {
    printTesto(myTesto, X, Y, myFonte, "", "");
}
function printTesto(myTesto, X, Y, myFonte, corFonte) {
    printTesto(myTesto, X, Y, myFonte, corFonte, "");
}
function printTesto(myTesto, X, Y, myFonte, corFonte, corAureula) {
    if(myTesto!=undefined && myTesto!=""){
        if(myFonte!=undefined && myFonte!="") ctx.font = myFonte;
        if(corAureula!=undefined && corAureula!=""){
            //myTesto+=" ["+corAureula+"]";
            ctx.fillStyle = corAureula;
            ctx.fillText(myTesto, X, Y-1);
            ctx.fillText(myTesto, X, Y+1);
            ctx.fillText(myTesto, X-1, Y);
            ctx.fillText(myTesto, X+1, Y);
        }
        if(corFonte!=undefined && corFonte!="") ctx.fillStyle = corFonte;
        ctx.fillText(myTesto, X, Y);
    }
}

function drawSprite(mySprite,x,y,bloco){
    w=mySprite.imagem.width/mySprite.colunas;
    h=mySprite.imagem.height/mySprite.linhas;
    x1=Math.round(bloco % mySprite.colunas) * w;
    y1=Math.round(bloco / mySprite.colunas) * h;
    ctx.drawImage(
        mySprite.imagem,
        x1,y1,w,h,
        x,y,w,h
    );
    //printTesto("x:"+x+" y:"+y+" ("+mySprite.colunas+"/"+mySprite.linhas+")",5, 30, "10pt Verdana", "rgba(255,255,255,1.0)");
    //printTesto("linha: "+linha+" coluna:"+coluna,5, 45, "10pt Verdana", "rgba(255,255,255,1.0)");
}
function drawTileset(x,y,bloco){
    var selTile=0;
    blocoReal=0;
    for(var t=mapa.tileset.length-1;t>=0;t++){
        if(bloco>mapa.tileset[t].firstgid){
            selTile=t+1;
            blocoReal=bloco-mapa.tileset[t].firstgid;
            break;
        }
    }
    w=mySprite.imagem.width/mapa.width;
    h=mySprite.imagem.height/mapa.width;


        tileset
    w=mySprite.imagem.width/mySprite.colunas;
    h=mySprite.imagem.height/mySprite.linhas;
    x1=Math.round(blocoReal % mySprite.colunas);
    y1=Math.round(blocoReal / mySprite.colunas);
    ctx.drawImage(
        mySprite.imagem,
        x1*w, y1*h, w, h,
         x*w,  y*h, w, h
    );
    //printTesto("x:"+x+" y:"+y+" ("+mySprite.colunas+"/"+mySprite.linhas+")",5, 30, "10pt Verdana", "rgba(255,255,255,1.0)");
    //printTesto("linha: "+linha+" coluna:"+coluna,5, 45, "10pt Verdana", "rgba(255,255,255,1.0)");
}
function drawMap(){
    ctx.save();
    ctx.fillStyle = "rgba(128,128,128, 1.0)";
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    //drawSprite(sprite,0,1,0);
    //drawSprite(sprite,1,1,0);
    var t=0;
    ctx.fillStyle = "rgb(255,255,255)";
    for(var l=0;l<myScreen.linhas;l++){
        for(var c=0;c<myScreen.colunas;c++){
            //ctx.drawImage(sprite.imagem,0,0,32,32,c*32,l*32,32,32);

            //drawTileset(l,c,Math.round(mapa.layer[0].data[l*c]));
            drawSprite(sprite,c*32,l*32,Math.round(mapa.background1[l][c]));
            

            if(seColisionVisible && mapa.colision[l][c]){
            //if(seColisionVisible && mapa.layer[mapa.layercollision].data[l*c]){
                ctx.fillStyle = "rgba(255,0,0,0.25)";
                ctx.fillRect(c*32+1,l*32+1,32-2, 32-2);
            }
            if(seExaminar){
                ctx.fillStyle = "rgba(255,255,255,0.75)";
                printTesto(""+t,(c*32)+5,(l*32)+20);
            }
        }
    }/**/
    ctx.restore();
}
function drawCusor(){
    ctx.save();
    //myMouse.c=Math.round((myMouse.x-32)/32);
    //myMouse.l=Math.round((myMouse.y-32)/32);
    if(
        myMouse.c>=0 && myMouse.c<=myScreen.colunas-1 &&
        myMouse.l>=0 && myMouse.c<=myScreen.colunas-1 &&
        !mapa.colision[myMouse.l][myMouse.c]
    ){
        ctx.fillStyle = "rgba(0,0,255,0.5)";
        ctx.fillRect(myMouse.c*32,myMouse.l*32,32,32);
        //ctx.arc(c*32+16,l*32+16,16, 0, Math.PI * 2, true); ctx.fill();
    }
    ctx.restore();
}
function drawTestos(){
    ctx.save();
    ctx.fillStyle = "rgba(0,0,0,0.5)";
    ctx.fillRect(0,0,640,24);
    printTesto(
        "FPS:"+fps+" "+
        //"Screen:" +myScreen.colunas+"/"+myScreen.linhas+" - "+
        "Key:"+myTeclado.btnNumero+(myTeclado.btnTecla.trim()!=""?"["+myTeclado.btnTecla.trim()+"]":"")+" "+
        "Mouse:"+myMouse.x+"/"+myMouse.y+"("+myMouse.c+"/"+myMouse.l+") "+
        "sptChar:"+sptChar.x+"/"+sptChar.y+"("+sptChar.c+"/"+sptChar.l+") ",
        5, 15, "10pt Verdana", "#000000", "#00FF00"
    );
    printTesto(
        "[Esc] to Debug!",
        530, 15, "10pt Verdana", "#000000", "#FFFFFF"
    );
    ctx.restore();
}
function update(){
    ctx.save();
    // Clear display
    //ctx.drawImage(imgTileset,0,0,canvas.width,canvas.height);
    drawMap();
    //ctx.globalAlpha = 0.1;

    drawCusor();

    drawSprite(sptChar,sptChar.c*32-16,sptChar.l*32-40,0);

    if(!seExaminar)drawTestos()
    
    //
    //ctx.beginPath();

    //ctx.fillStyle = ball.color;
    
    //ctx.fillStyle = ball.color;
    //ctx.arc(0, 0, ball.r, 0, Math.PI * 2, true);

    //ctx.drawImage(imgBola,0,0,imgBola.width,imgBola.height);
    //ctx.closePath();
    //ctx.fill();
    
    ctx.restore();

    //if(seTxtVisivel==true) showTesto();
}

//init();
