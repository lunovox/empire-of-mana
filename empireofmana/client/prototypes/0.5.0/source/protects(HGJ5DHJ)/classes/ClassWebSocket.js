function testeWebSocket(){
	var divServidoresTesto = document.getElementById("divServidoresTesto");
	var slcServidoresLista = document.getElementById("slcServidoresLista");
	var btnServidoresConectar = document.getElementById("btnServidoresConectar");
	var btnServidoresAnexar = document.getElementById("btnServidoresAnexar");
	var btnServidoresDestacar = document.getElementById("btnServidoresDestacar");
	if(window.WebSocket){
		slcServidoresLista.disabled=false;
		btnServidoresConectar.disabled=false;
		btnServidoresAnexar.disabled=false;
		btnServidoresDestacar.disabled=false;
	}else{
		slcServidoresLista.disabled=true;
		btnServidoresConectar.disabled=true;
		btnServidoresAnexar.disabled=true;
		btnServidoresDestacar.disabled=true;
		showAviso(divServidoresTesto,
			"<b>Seu navegador não suporta WebSocket!</b><br/>"+
			"Lista de navegadores habilitados:<br/>"+
			"<a target='_blank' href='http://www.google.com/chrome/?hl=pt-BR' title='Clique para baixar o Google Chrome'>"+
				"<img src='images/selo_chrome.jpg' border='0'>"+
			"</a> "+
			"<a target='_blank' href='http://www.apple.com/br/safari/download/' title='Clique para baixar o Apple Safari'>"+
				"<img src='images/selo_safari.png' border='0'>"+
			"</a> "+
			"<a target='_blank' href='http://packages.debian.org/pt-br/squeeze/epiphany-browser' title='Clique para baixar o Gnome Epiphany'>"+
				"<img src='images/selo_epiphany.png' border='0'>"+
			"</a> "
		);
	}
	document.body.style.cursor='default';
}