/*
 Aviso: a função setXML() só consegue ler um XML se tiver no site local.
 Exemplo
	var serverXML = new ClassServerXML();
	Errado → serverXML.setXML("http://www.tuatec.com.br/empireofmana/server/prototypes/0.2.0/server.xml");
	Correto → serverXML.setXML("../../../server/prototypes/0.2.0/server.xml");
*/
function ClassServerXML($url) {
	var url="";
	var path="";
	var nome="";
	var websocket="";
	var clientBackground="";
	var clientMusic="";
	var clientFrmLoginX=160;
	var clientFrmLoginY=160;
	var clientFrmRegisterX=80;
	var clientFrmRegisterY=100;
	
	var usersmax=30;
	var urlUsersXML="";
	var serverContract="";
	
	if($url!=null){
		setXML($url/*+"?"+(new Date())/**/);
	}
	
	this.getURL = function(){return url;};
	this.getPath = function(){return path;};
	this.getNome = function(){return nome;};
	this.getWebSocket = function(){return websocket;};
	this.getClientBackground = function(){return clientBackground;};
	this.getClientMusic = function(){return clientMusic;};
	this.getClientFrmLoginX = function(){return clientFrmLoginX;};
	this.getClientFrmLoginY = function(){return clientFrmLoginY;};
	this.getClientFrmRegisterX = function(){return clientFrmRegisterX;};
	this.getClientFrmRegisterY = function(){return clientFrmRegisterY;};
	this.getUsersMax = function(){return usersmax;};
	this.getUsersXML = function(){return urlUsersXML; };
	this.getUsersClass = function(){ return usersXML; /* ← Esta variável está em 'engine.js' */	};
	
	this.setURL = setXML;
	/*this.setPath = function (novoPath){
		path=novoPath;
		setXML(novaURL+"server.xml?"+(new Date()));
	};/**/
	this.setNome = function (novoNome){nome=novoNome;};
	this.setWebSocket = function (novoWebSocket){websocket=novoWebSocket;};
	this.setClientBackground = function (novoClientBackground){clientBackground=novoClientBackground;};
	this.setClientMusic = function (novoClientMusic){clientMusic=novoClientMusic;};
	this.setClientFrmLoginX = function (novoFrmLoginX){clientFrmLoginX=novoFrmLoginX;};
	this.setClientFrmLoginY = function (novoFrmLoginX){clientFrmLoginY=novoFrmLoginY;};
	this.setClientFrmRegisterX = function (novoFrmRegisterX){clientFrmRegisterX=novoFrmRegisterX;};
	this.setClientFrmRegisterY = function (novoFrmRegisterX){clientFrmRegisterY=novoFrmRegisterY;};
	this.setUsersMax = function (novoUsersMax){usersmax=novoUsersMax;};

	function setXML($novaURL){
		$partes = $novaURL.split('/');
		$partes[$partes.length-1]="";
		path=$partes.join('/');
		//by Micox: micoxjcg@yahoo.com.br.
		url=$novaURL;
		
		var xmlNode = null;
		$import="importFile.php?mimetype=application/xml&url="+$novaURL;
		//alert($import);
		if(window.ActiveXObject){
			xmlNode = new ActiveXObject("Msxml2.DOMDocument.3.0");
			xmlNode.async = false;
			xmlNode.load($import);
			//xmlNode.load($novaURL);
		}else if(window.XMLHttpRequest){
			var Loader = new XMLHttpRequest();
			Loader.open("GET", $import, false);
			//Loader.open("GET", $novaURL ,false);
			Loader.send(null);
			xmlNode = Loader.responseXML;
		}
		for(var n=0;n<xmlNode.childNodes.length;n++){//percorrendo os filhos do nó
			if(xmlNode.childNodes[n].nodeType == 1){//ignorar espaços em branco
				nodeProperties = xmlNode.childNodes[n];
				//alert(nodeProperties.nodeName);
				if(nodeProperties.nodeName=="properties"){
					//alert(nodeProperties.childNodes.length);
					for(var t=0;t<nodeProperties.childNodes.length;t++){
						nodeProperty = nodeProperties.childNodes[t];
						if(nodeProperty.nodeType == 1){
							//alert(nodeProperty.nodeName);
							if(nodeProperty.nodeName=="name"){
								for(var a=0;a<nodeProperty.attributes.length;a++){
									atrib = nodeProperty.attributes[a];
									if(atrib.nodeName=="value"){nome = atrib.nodeValue; break;}
								}
							}
							if(nodeProperty.nodeName=="websocket"){
								for(var a=0;a<nodeProperty.attributes.length;a++){
									atrib = nodeProperty.attributes[a];
									if(atrib.nodeName=="value"){
										websocket = atrib.nodeValue;
									}else if(atrib.nodeName=="update"){
										lastUpdate = atrib.nodeValue;
									}
								}
							}
							if(nodeProperty.nodeName=="client"){
								for(var a=0;a<nodeProperty.attributes.length;a++){
									atrib = nodeProperty.attributes[a];
									if(atrib.nodeName=="background"){
										clientBackground = atrib.nodeValue;
									}else if(atrib.nodeName=="music"){
										clientMusic = atrib.nodeValue;
									}else if(atrib.nodeName=="frmlogin"){
										clientFrmLoginX = atrib.nodeValue.split(",")[0];
										clientFrmLoginY = atrib.nodeValue.split(",")[1];
										//alert("frmlogin[X,Y]=["+clientFrmLoginX+","+clientFrmLoginY+"]");
									}else if(atrib.nodeName=="frmloginx"){
										clientFrmLoginX = atrib.nodeValue;
									}else if(atrib.nodeName=="frmloginy"){
										clientFrmLoginY = atrib.nodeValue;
									}else if(atrib.nodeName=="frmregister"){
										clientFrmRegisterX = atrib.nodeValue.split(",")[0];
										clientFrmRegisterY = atrib.nodeValue.split(",")[1];
										//alert("frmregister[X,Y]=["+clientFrmRegisterX+","+clientFrmRegisterY+"]");
									}else if(atrib.nodeName=="frmregisterx"){
										clientFrmRegisterX = atrib.nodeValue;
									}else if(atrib.nodeName=="frmregistery"){
										clientFrmRegisterY = atrib.nodeValue;
									}else if(atrib.nodeName=="frmchangepassword"){
										clientFrmChangePassWordX = atrib.nodeValue.split(",")[0];
										clientFrmChangePassWordY = atrib.nodeValue.split(",")[1];
										//alert("frmregister[X,Y]=["+clientFrmRegisterX+","+clientFrmRegisterY+"]");
									}else if(atrib.nodeName=="frmchangepasswordx"){
										clientFrmChangePassWordX = atrib.nodeValue;
									}else if(atrib.nodeName=="frmchangepasswordy"){
										clientFrmChangePassWordY = atrib.nodeValue;
									}
								}
							}
							if(nodeProperty.nodeName=="usersmax"){
								for(var a=0;a<nodeProperty.attributes.length;a++){
									atrib = nodeProperty.attributes[a];
									if(atrib.nodeName=="value"){usersmax = atrib.nodeValue; break;}
								}
							}
							if(nodeProperty.nodeName=="users"){
								for(var a=0;a<nodeProperty.attributes.length;a++){
									atrib = nodeProperty.attributes[a];
									if(atrib.nodeName=="value"){
										urlUsersXML = path+atrib.nodeValue; 
										if(urlUsersXML!=""){
											usersXML = new ClassUsersXML(urlUsersXML /*+"?"+(new Date()) /**/);
										} 
										break;
									}
								}
							}
							if(nodeProperty.nodeName=="contract"){
								for(var a=0;a<nodeProperty.attributes.length;a++){
									atrib = nodeProperty.attributes[a];
									if(atrib.nodeName=="value"){
										try{
											var xmlhttp;
											var $ContractURL="importFile.php?mimetype=text/plain&url="+path+atrib.nodeValue;
											if (window.XMLHttpRequest){ //code for IE7+, Firefox, Chrome, Opera, Safari
												xmlhttp=new XMLHttpRequest();
											}else{ //code for IE6, IE5
												xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
											}
											xmlhttp.onreadystatechange=function(){
												if (xmlhttp.readyState==4 && xmlhttp.status==200){
													document.getElementById("txtContractClasulas").value = xmlhttp.responseText;
												}
											}
											xmlhttp.open("GET",$ContractURL,false); //Assincronismo = false (Espera resposta)
											xmlhttp.send();
										}catch(e){ 
											alert(e);
										}
										break;
									}
								}
							}
						}
					}
				}
			}
		}
	}
}