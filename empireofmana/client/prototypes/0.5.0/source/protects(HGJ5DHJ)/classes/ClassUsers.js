/*
 Aviso: a função setXML() só consegue ler um XML se tiver no site local.
 Exemplo
	var serverXML = new ClassServerXML();
	Errado → serverXML.setXML("http://www.tuatec.com.br/empireofmana/server/prototypes/0.2.0/server.xml");
	Correto → serverXML.setXML("../../../server/prototypes/0.2.0/server.xml");
*/
function ClassUsersXML(endereco) {
	var url="";
	var path="";
	//var classes = new Array();
	var classes = [];
	var avatars = [];
	var Email="";
	var Sexo="";
	var chars = new Array();
	var nicks = new Array();
	//$Race, $Class, $HairType, $HairColor, $Eye, $Nose, $Lip, $Mark
	this.getEmail = function(){return Email;};
	this.setEmail = function($NewEmail){Email = $NewEmail;};
	this.getSexo = function(){return Sexo;};
	this.setSexo = function($NewSexo){Sexo = $NewSexo;};
	this.delAllChars = function(){
		chars = new Array();
		nicks = new Array();
	};
	this.addChar = function($Nick, $Sex, $Race, $Class, $HairType, $HairColor, $Eye, $Nose, $Lip, $Mark, $Level){
		if(nicks[$Nick]==undefined || nicks[$Nick]==null){
			chars[chars.length]=[];// ← Isso adiciona e prepara. Por isso que não está "chars.length-1"
			chars[chars.length-1].nick=$Nick;
			chars[chars.length-1].sex=$Sex;
			chars[chars.length-1].race=$Race;
			chars[chars.length-1].class=$Class;
			chars[chars.length-1].hairtype=$HairType;
			chars[chars.length-1].haircolor=$HairColor;
			chars[chars.length-1].eye=$Eye;
			chars[chars.length-1].nose=$Nose;
			chars[chars.length-1].lip=$Lip;
			chars[chars.length-1].mark=$Mark;
			chars[chars.length-1].level=$Level;
			nicks[$Nick]=chars.length-1;
			return true;
		}else{
			return false;
		}
	};
	this.delCharsByNick = function($Nick){
		if(nicks[$Nick]!=undefined && nicks[$Nick]!=null){
			$i=nicks[$Nick];
			chars[$i]=null;
			nicks[$Nick]=null;
			return true;
		}
		return false;
	};
	this.delCharsByIndex = function($Index){
		if(chars[$Index]!=undefined && chars[$Index]!=null){
			$c=chars[$Index].nick;
			nicks[$c]=null;
			chars[$Index]=null;
			return true;
		}
		return false;
	};
	this.getChars = function(){return chars;};
	this.getCharByIndex = function($Index){
		if(chars[$Index]!=undefined && chars[$Index]!=null){
			return chars[$Index];
		}
		return false;
	};
	this.getIndexByNick = function($Nick){
		if(nicks[$Nick]!=undefined && nicks[$Nick]!=null){
			return nicks[$Nick];
		}
		return false;
	};

	
	this.getURL = function(){return url;};
	this.getPath = function(){return path;};
	this.getClasses = function(){return classes;};
	this.getAvatars = function(){return avatars;};
	this.setURL = setXML;
	
	if(endereco!=null) setXML(endereco);
	function setXML($novaURL){
		//by Micox: micoxjcg@yahoo.com.br.
		$partes = $novaURL.split('/');
		$partes[$partes.length-1]="";
		path=$partes.join('/');
		url=$novaURL;
		var xmlNode = null;
		$import="importFile.php?mimetype=application/xml&url="+$novaURL;
		//alert($import);
		if(window.ActiveXObject){
			xmlNode = new ActiveXObject("Msxml2.DOMDocument.3.0");
			xmlNode.async = false;
			xmlNode.load($import);
			//xmlNode.load($novaURL);
		}else if(window.XMLHttpRequest){
			var Loader = new XMLHttpRequest();
			Loader.open("GET", $import, false);
			//Loader.open("GET", $novaURL ,false);
			Loader.send(null);
			xmlNode = Loader.responseXML;
		}
		for(var n=0;n<xmlNode.childNodes.length;n++){//percorrendo os filhos do nó
			if(xmlNode.childNodes[n].nodeType == 1){//ignorar espaços em branco
				nodeUsers = xmlNode.childNodes[n];
				//alert(nodeUsers.nodeName);
				if(nodeUsers.nodeName=="users"){
					//alert(nodeUsers.childNodes.length);
					for(var u=0; u<nodeUsers.childNodes.length; u++){
						nodeClassesAndAvatars = nodeUsers.childNodes[u];
						if(nodeClassesAndAvatars.nodeType == 1 && nodeClassesAndAvatars.nodeName=="classes"){
							for(var a=0;a<nodeClassesAndAvatars.attributes.length;a++){
								atrib = nodeClassesAndAvatars.attributes[a];
								if(atrib.nodeName=="path"){
									classes.path=atrib.nodeValue;
									if(atrib.nodeValue!=""){
										classes.sprite=new Image();
										classes.sprite.src=path+atrib.nodeValue+getIfUptadeFiles();
									}else{ break; }
								}else if(atrib.nodeName=="boxes"){
									classes.boxes=atrib.nodeValue; 
									if(atrib.nodeValue=="" || atrib.nodeValue=="?,?") break;
								}
							}
							var contClass=-1;
							classes.class = new Array();
							for(var cs=0; cs<nodeClassesAndAvatars.childNodes.length; cs++){
								nodeClasses = nodeClassesAndAvatars.childNodes[cs];
								if(nodeClasses.nodeType == 1 && nodeClasses.nodeName=="class"){
									//alert("nodeClasses["+nodeClasses.nodeName+"].childNodes.length="+nodeClasses.childNodes.length);
									classes.class[++contClass]=[];
									//alert("contClass="+contClass);
									for(var a=0;a<nodeClasses.attributes.length;a++){
										atrib = nodeClasses.attributes[a];
										if(atrib.nodeName=="name"){
											//alert("classes["+contClass+"].nome="+atrib.nodeValue);
											classes.class[contClass].name=atrib.nodeValue; break;
										}
									}
									for(var c=0; c<nodeClasses.childNodes.length; c++){
										nodeSinglerClass = nodeClasses.childNodes[c];
										if(nodeSinglerClass.nodeType == 1 && (nodeSinglerClass.nodeName=="strength" || nodeSinglerClass.nodeName=="str")){
											//alert("nodeSinglerClass.nodeName="+nodeSinglerClass.nodeName);
											classes.class[contClass].str=[];
											for(var a=0;a<nodeSinglerClass.attributes.length;a++){
												atrib = nodeSinglerClass.attributes[a];
												//alert("("+classes.class[contClass].name+")."+nodeSinglerClass.nodeName+"."+atrib.nodeName+"="+atrib.nodeValue);
												if(atrib.nodeName=="name"){
													classes.class[contClass].str.name = atrib.nodeValue;
													//alert("classes["+contClass+"].str.nome="+classes.class[contClass].str.nome);
												}else if(atrib.nodeName=="locked"){
													classes.class[contClass].str.locked=atrib.nodeValue;
												}else if(atrib.nodeName=="value"){
													classes.class[contClass].str.value=atrib.nodeValue;
												}
											}
										}else if(nodeSinglerClass.nodeType == 1 && (nodeSinglerClass.nodeName=="agility" || nodeSinglerClass.nodeName=="agi")){
											classes.class[contClass].agi=[];
											for(var a=0;a<nodeSinglerClass.attributes.length;a++){
												atrib = nodeSinglerClass.attributes[a];
												if(atrib.nodeName=="name"){
													classes.class[contClass].agi.name = atrib.nodeValue;
												}else if(atrib.nodeName=="locked"){
													classes.class[contClass].agi.locked=atrib.nodeValue;
												}else if(atrib.nodeName=="value"){
													classes.class[contClass].agi.value=atrib.nodeValue;
												}
											}
										}else if(nodeSinglerClass.nodeType == 1 && (nodeSinglerClass.nodeName=="vitality" || nodeSinglerClass.nodeName=="vit")){
											classes.class[contClass].vit=[];
											for(var a=0;a<nodeSinglerClass.attributes.length;a++){
												atrib = nodeSinglerClass.attributes[a];
												if(atrib.nodeName=="name"){
													classes.class[contClass].vit.name = atrib.nodeValue;
												}else if(atrib.nodeName=="locked"){
													classes.class[contClass].vit.locked=atrib.nodeValue;
												}else if(atrib.nodeName=="value"){
													classes.class[contClass].vit.value=atrib.nodeValue;
												}
											}
										}else if(nodeSinglerClass.nodeType == 1 && (nodeSinglerClass.nodeName=="intelligence" || nodeSinglerClass.nodeName=="int")){
											classes.class[contClass].int=[];
											for(var a=0;a<nodeSinglerClass.attributes.length;a++){
												atrib = nodeSinglerClass.attributes[a];
												if(atrib.nodeName=="name"){
													classes.class[contClass].int.name = atrib.nodeValue;
												}else if(atrib.nodeName=="locked"){
													classes.class[contClass].int.locked=atrib.nodeValue;
												}else if(atrib.nodeName=="value"){
													classes.class[contClass].int.value=atrib.nodeValue;
												}
											}
										}else if(nodeSinglerClass.nodeType == 1 && (nodeSinglerClass.nodeName=="dexterity" || nodeSinglerClass.nodeName=="dex")){
											classes.class[contClass].dex=[];
											for(var a=0;a<nodeSinglerClass.attributes.length;a++){
												atrib = nodeSinglerClass.attributes[a];
												if(atrib.nodeName=="name"){
													classes.class[contClass].dex.name = atrib.nodeValue;
												}else if(atrib.nodeName=="locked"){
													classes.class[contClass].dex.locked=atrib.nodeValue;
												}else if(atrib.nodeName=="value"){
													classes.class[contClass].dex.value=atrib.nodeValue;
												}
											}
										}else if(nodeSinglerClass.nodeType == 1 && (nodeSinglerClass.nodeName=="lucky" || nodeSinglerClass.nodeName=="luc")){
											classes.class[contClass].luc=[];
											for(var a=0;a<nodeSinglerClass.attributes.length;a++){
												atrib = nodeSinglerClass.attributes[a];
												if(atrib.nodeName=="name"){
													classes.class[contClass].luc.name = atrib.nodeValue;
												}else if(atrib.nodeName=="locked"){
													classes.class[contClass].luc.locked=atrib.nodeValue;
												}else if(atrib.nodeName=="value"){
													classes.class[contClass].luc.value=atrib.nodeValue;
												}
											}
										}else if(nodeSinglerClass.nodeType == 1 && nodeSinglerClass.nodeName=="male"){
											classes.class[contClass].male=[];
											for(var a=0;a<nodeSinglerClass.attributes.length;a++){
												atrib = nodeSinglerClass.attributes[a];
												if(atrib.nodeName=="map"){
													classes.class[contClass].male.map = atrib.nodeValue;
												}else if(atrib.nodeName=="x"){
													classes.class[contClass].male.x=atrib.nodeValue;
												}else if(atrib.nodeName=="y"){
													classes.class[contClass].male.y=atrib.nodeValue;
												}else if(atrib.nodeName=="sit"){
													classes.class[contClass].male.sit=atrib.nodeValue;
												}else if(atrib.nodeName=="direction" || atrib.nodeName=="dir"){
													classes.class[contClass].male.dir=atrib.nodeValue;
													classes.class[contClass].male.direction=atrib.nodeValue;
												}else if(atrib.nodeName=="name"){
													classes.class[contClass].male.name=atrib.nodeValue;
												}else if(atrib.nodeName=="equipments" || atrib.nodeName=="equips"){
													classes.class[contClass].male.equips=atrib.nodeValue;
													classes.class[contClass].male.equipments=atrib.nodeValue;
												}else if(atrib.nodeName=="box"){
													classes.class[contClass].male.box=atrib.nodeValue;
												}
											}
										}else if(nodeSinglerClass.nodeType == 1 && nodeSinglerClass.nodeName=="female"){
											classes.class[contClass].female=[];
											for(var a=0;a<nodeSinglerClass.attributes.length;a++){
												atrib = nodeSinglerClass.attributes[a];
												if(atrib.nodeName=="map"){
													classes.class[contClass].female.map = atrib.nodeValue;
												}else if(atrib.nodeName=="x"){
													classes.class[contClass].female.x=atrib.nodeValue;
												}else if(atrib.nodeName=="y"){
													classes.class[contClass].female.y=atrib.nodeValue;
												}else if(atrib.nodeName=="sit"){
													classes.class[contClass].female.sit=atrib.nodeValue;
												}else if(atrib.nodeName=="direction" || atrib.nodeName=="dir"){
													classes.class[contClass].female.dir=atrib.nodeValue;
													classes.class[contClass].female.direction=atrib.nodeValue;
												}else if(atrib.nodeName=="name"){
													classes.class[contClass].female.name=atrib.nodeValue;
												}else if(atrib.nodeName=="equipments" || atrib.nodeName=="equips"){
													classes.class[contClass].female.equips=atrib.nodeValue;
													classes.class[contClass].female.equipments=atrib.nodeValue;
												}else if(atrib.nodeName=="box"){
													classes.class[contClass].female.box=atrib.nodeValue;
												}
											}
										}
									}
								}
							}
						}else if(nodeClassesAndAvatars.nodeType == 1 && nodeClassesAndAvatars.nodeName=="genericavatars"){
							for(var a=0;a<nodeClassesAndAvatars.attributes.length;a++){
								atrib = nodeClassesAndAvatars.attributes[a];
								if(atrib.nodeName=="folder"){ avatars.folder=atrib.nodeValue;}
							}
							for(var cs=0; cs<nodeClassesAndAvatars.childNodes.length; cs++){
								nodeAvatars = nodeClassesAndAvatars.childNodes[cs];
								if(nodeAvatars.nodeType == 1 && nodeAvatars.nodeName=="races"){
									avatars.races=[];
									for(var a=0;a<nodeAvatars.attributes.length;a++){
										atrib = nodeAvatars.attributes[a];
										if(atrib.nodeName=="path"){
											avatars.races.path=atrib.nodeValue;
											if(atrib.nodeValue!=""){
												avatars.races.sprite=new Image();
												avatars.races.sprite.src=path+atrib.nodeValue+getIfUptadeFiles();
											}else{ break; }
										}else if(atrib.nodeName=="boxes"){
											avatars.races.boxes=atrib.nodeValue;
											if(atrib.nodeValue=="" || atrib.nodeValue=="?,?") break;
										}
									}
									var contRaces=0;
									avatars.races.race = new Array();
									for(var h=0; h<nodeAvatars.childNodes.length; h++){
										nodeRaces = nodeAvatars.childNodes[h];
										if(nodeRaces.nodeType == 1 && nodeRaces.nodeName=="race"){
											contRaces++;
											avatars.races.race[contRaces-1]=[];
											avatars.races.race[contRaces-1].top=[];
											avatars.races.race[contRaces-1].top.box=0;
											for(var a=0;a<nodeRaces.attributes.length;a++){
												atrib = nodeRaces.attributes[a];
												if(atrib.nodeName=="name"){ 
													avatars.races.race[contRaces-1].name=atrib.nodeValue;
													//alert(avatars.races.race[contRaces-1].name);
												}
											}
											for(var s=0; s<nodeRaces.childNodes.length; s++){
												nodeSex = nodeRaces.childNodes[s];
												if(nodeSex.nodeType == 1 && (nodeSex.nodeName=="male" || nodeSex.nodeName=="female")){
													if(nodeSex.nodeName=="male"){ avatars.races.race[contRaces-1].male=[];}
													else if(nodeSex.nodeName=="female"){ avatars.races.race[contRaces-1].female=[];}
													for(var a=0;a<nodeSex.attributes.length;a++){
														atrib = nodeSex.attributes[a];
														if(atrib.nodeName=="name"){
															     if(nodeSex.nodeName=="male")  { avatars.races.race[contRaces-1].male.name   = atrib.nodeValue;}
															else if(nodeSex.nodeName=="female"){ avatars.races.race[contRaces-1].female.name = atrib.nodeValue;}
														}else if(atrib.nodeName=="box"){
															     if(nodeSex.nodeName=="male")  { avatars.races.race[contRaces-1].male.box   = atrib.nodeValue;}
															else if(nodeSex.nodeName=="female"){ avatars.races.race[contRaces-1].female.box = atrib.nodeValue;}
														}
													}
												}else if(nodeSex.nodeType == 1 && nodeSex.nodeName=="top"){
													avatars.races.race[contRaces-1].top=[];
													for(var a=0;a<nodeSex.attributes.length;a++){
														atrib = nodeSex.attributes[a];
														if(atrib.nodeName=="box"){
															avatars.races.race[contRaces-1].top.box = atrib.nodeValue;
															//alert("avatars.races.race["+(contRaces-1)+"].top.box="+avatars.races.race[contRaces-1].top.box);
														}
													}
												}
											}
										}
									}
								}else if(nodeAvatars.nodeType == 1 && nodeAvatars.nodeName=="hairs"){
									avatars.hairs=[];
									for(var a=0;a<nodeAvatars.attributes.length;a++){
										atrib = nodeAvatars.attributes[a];
										if(atrib.nodeName=="path"){
											avatars.hairs.path=atrib.nodeValue;
											if(atrib.nodeValue!=""){
												avatars.hairs.sprite=new Image();
												avatars.hairs.sprite.src=path+atrib.nodeValue+getIfUptadeFiles();
											}else{ break; }
										}else if(atrib.nodeName=="boxes"){
											avatars.hairs.boxes=atrib.nodeValue;
											if(atrib.nodeValue=="" || atrib.nodeValue=="?,?") break;
										}
									}
									var contHairs=0;
									avatars.hairs.hair = new Array();
									for(var h=0; h<nodeAvatars.childNodes.length; h++){
										nodeHairs = nodeAvatars.childNodes[h];
										if(nodeHairs.nodeType == 1 && nodeHairs.nodeName=="hair"){
											contHairs++;
											avatars.hairs.hair[contHairs-1]=[];
											for(var a=0;a<nodeHairs.attributes.length;a++){
												atrib = nodeHairs.attributes[a];
												if(atrib.nodeName=="name"){ 
													avatars.hairs.hair[contHairs-1].name=atrib.nodeValue;
													//alert(avatars.hairs.hair[contHairs-1].name);
												}
											}
											for(var s=0; s<nodeHairs.childNodes.length; s++){
												nodeSex = nodeHairs.childNodes[s];
												if(nodeSex.nodeType == 1 && (nodeSex.nodeName=="male" || nodeSex.nodeName=="female")){
													if(nodeSex.nodeName=="male"){ avatars.hairs.hair[contHairs-1].male=[];}
													else if(nodeSex.nodeName=="female"){ avatars.hairs.hair[contHairs-1].female=[];}
													for(var a=0;a<nodeSex.attributes.length;a++){
														atrib = nodeSex.attributes[a];
														if(atrib.nodeName=="box"){
															     if(nodeSex.nodeName=="male")  { avatars.hairs.hair[contHairs-1].male.box   = atrib.nodeValue;}
															else if(nodeSex.nodeName=="female"){ avatars.hairs.hair[contHairs-1].female.box = atrib.nodeValue;}
														}
													}
												}
											}
										}
									}
								}else if(nodeAvatars.nodeType == 1 && nodeAvatars.nodeName=="eyes"){
									avatars.eyes=[];
									for(var a=0;a<nodeAvatars.attributes.length;a++){
										atrib = nodeAvatars.attributes[a];
										if(atrib.nodeName=="path"){
											avatars.eyes.path=atrib.nodeValue;
											if(atrib.nodeValue!=""){
												avatars.eyes.sprite=new Image();
												avatars.eyes.sprite.src=path+atrib.nodeValue+getIfUptadeFiles();
											}else{ break; }
										}else if(atrib.nodeName=="boxes"){
											avatars.eyes.boxes=atrib.nodeValue;
											if(atrib.nodeValue=="" || atrib.nodeValue=="?,?") break;
										}
									}
									var contEyes=0;
									avatars.eyes.eye = new Array();
									for(var h=0; h<nodeAvatars.childNodes.length; h++){
										nodeEyes = nodeAvatars.childNodes[h];
										if(nodeEyes.nodeType == 1 && nodeEyes.nodeName=="eye"){
											contEyes++;
											avatars.eyes.eye[contEyes-1]=[];
											for(var a=0;a<nodeEyes.attributes.length;a++){
												atrib = nodeEyes.attributes[a];
												if(atrib.nodeName=="name"){ 
													avatars.eyes.eye[contEyes-1].name=atrib.nodeValue;
													//alert(avatars.eyes.eye[contEyes-1].name);
												}
											}
											for(var s=0; s<nodeEyes.childNodes.length; s++){
												nodeSex = nodeEyes.childNodes[s];
												if(nodeSex.nodeType == 1 && (nodeSex.nodeName=="male" || nodeSex.nodeName=="female")){
													if(nodeSex.nodeName=="male"){ avatars.eyes.eye[contEyes-1].male=[];}
													else if(nodeSex.nodeName=="female"){ avatars.eyes.eye[contEyes-1].female=[];}
													for(var a=0;a<nodeSex.attributes.length;a++){
														atrib = nodeSex.attributes[a];
														if(atrib.nodeName=="box"){
															     if(nodeSex.nodeName=="male")  { avatars.eyes.eye[contEyes-1].male.box   = atrib.nodeValue;}
															else if(nodeSex.nodeName=="female"){ avatars.eyes.eye[contEyes-1].female.box = atrib.nodeValue;}
														}
													}
												}
											}
										}
									}
								}else if(nodeAvatars.nodeType == 1 && nodeAvatars.nodeName=="noses"){
									avatars.noses=[];
									for(var a=0;a<nodeAvatars.attributes.length;a++){
										atrib = nodeAvatars.attributes[a];
										if(atrib.nodeName=="path"){
											avatars.noses.path=atrib.nodeValue;
											if(atrib.nodeValue!=""){
												avatars.noses.sprite=new Image();
												avatars.noses.sprite.src=path+atrib.nodeValue+getIfUptadeFiles();
											}else{ break; }
										}else if(atrib.nodeName=="boxes"){
											avatars.noses.boxes=atrib.nodeValue;
											if(atrib.nodeValue=="" || atrib.nodeValue=="?,?") break;
										}
									}
									var contNoses=0;
									avatars.noses.nose = new Array();
									for(var h=0; h<nodeAvatars.childNodes.length; h++){
										nodeNoses = nodeAvatars.childNodes[h];
										if(nodeNoses.nodeType == 1 && nodeNoses.nodeName=="nose"){
											contNoses++;
											avatars.noses.nose[contNoses-1]=[];
											for(var a=0;a<nodeNoses.attributes.length;a++){
												atrib = nodeNoses.attributes[a];
												if(atrib.nodeName=="name"){ 
													avatars.noses.nose[contNoses-1].name=atrib.nodeValue;
													//alert(avatars.noses.nose[contNoses-1].name);
												}
											}
											for(var s=0; s<nodeNoses.childNodes.length; s++){
												nodeSex = nodeNoses.childNodes[s];
												if(nodeSex.nodeType == 1 && (nodeSex.nodeName=="male" || nodeSex.nodeName=="female")){
													if(nodeSex.nodeName=="male"){ avatars.noses.nose[contNoses-1].male=[];}
													else if(nodeSex.nodeName=="female"){ avatars.noses.nose[contNoses-1].female=[];}
													for(var a=0;a<nodeSex.attributes.length;a++){
														atrib = nodeSex.attributes[a];
														if(atrib.nodeName=="box"){
															     if(nodeSex.nodeName=="male")  { avatars.noses.nose[contNoses-1].male.box   = atrib.nodeValue;}
															else if(nodeSex.nodeName=="female"){ avatars.noses.nose[contNoses-1].female.box = atrib.nodeValue;}
														}
													}
												}
											}
										}
									}
								}else if(nodeAvatars.nodeType == 1 && nodeAvatars.nodeName=="lips"){
									avatars.lips=[];
									for(var a=0;a<nodeAvatars.attributes.length;a++){
										atrib = nodeAvatars.attributes[a];
										if(atrib.nodeName=="path"){
											avatars.lips.path=atrib.nodeValue;
											if(atrib.nodeValue!=""){
												avatars.lips.sprite=new Image();
												avatars.lips.sprite.src=path+atrib.nodeValue+getIfUptadeFiles();
											}else{ break; }
										}else if(atrib.nodeName=="boxes"){
											avatars.lips.boxes=atrib.nodeValue;
											if(atrib.nodeValue=="" || atrib.nodeValue=="?,?") break;
										}
									}
									var contLips=0;
									avatars.lips.lip = new Array();
									for(var h=0; h<nodeAvatars.childNodes.length; h++){
										nodeLips = nodeAvatars.childNodes[h];
										if(nodeLips.nodeType == 1 && nodeLips.nodeName=="lip"){
											contLips++;
											avatars.lips.lip[contLips-1]=[];
											for(var a=0;a<nodeLips.attributes.length;a++){
												atrib = nodeLips.attributes[a];
												if(atrib.nodeName=="name"){ 
													avatars.lips.lip[contLips-1].name=atrib.nodeValue;
													//alert(avatars.lips.lip[contLips-1].name);
												}
											}
											for(var s=0; s<nodeLips.childNodes.length; s++){
												nodeSex = nodeLips.childNodes[s];
												if(nodeSex.nodeType == 1 && (nodeSex.nodeName=="male" || nodeSex.nodeName=="female")){
													if(nodeSex.nodeName=="male"){ avatars.lips.lip[contLips-1].male=[];}
													else if(nodeSex.nodeName=="female"){ avatars.lips.lip[contLips-1].female=[];}
													for(var a=0;a<nodeSex.attributes.length;a++){
														atrib = nodeSex.attributes[a];
														if(atrib.nodeName=="box"){
															     if(nodeSex.nodeName=="male")  { avatars.lips.lip[contLips-1].male.box   = atrib.nodeValue;}
															else if(nodeSex.nodeName=="female"){ avatars.lips.lip[contLips-1].female.box = atrib.nodeValue;}
														}
													}
												}
											}
										}
									}
								}else if(nodeAvatars.nodeType == 1 && nodeAvatars.nodeName=="scars"){
									avatars.scars=[];
									for(var a=0;a<nodeAvatars.attributes.length;a++){
										atrib = nodeAvatars.attributes[a];
										if(atrib.nodeName=="path"){
											avatars.scars.path=atrib.nodeValue;
											if(atrib.nodeValue!=""){
												avatars.scars.sprite=new Image();
												avatars.scars.sprite.src=path+atrib.nodeValue+getIfUptadeFiles();
											}else{ break; }
										}else if(atrib.nodeName=="boxes"){
											avatars.scars.boxes=atrib.nodeValue;
											if(atrib.nodeValue=="" || atrib.nodeValue=="?,?") break;
										}
									}
									var contScars=0;
									avatars.scars.scar = new Array();
									for(var h=0; h<nodeAvatars.childNodes.length; h++){
										nodeScars = nodeAvatars.childNodes[h];
										if(nodeScars.nodeType == 1 && nodeScars.nodeName=="scar"){
											contScars++;
											avatars.scars.scar[contScars-1]=[];
											for(var a=0;a<nodeScars.attributes.length;a++){
												atrib = nodeScars.attributes[a];
												if(atrib.nodeName=="name"){ 
													avatars.scars.scar[contScars-1].name=atrib.nodeValue;
													//alert(avatars.scars.scar[contScars-1].name);
												}
											}
											for(var s=0; s<nodeScars.childNodes.length; s++){
												nodeSex = nodeScars.childNodes[s];
												if(nodeSex.nodeType == 1 && (nodeSex.nodeName=="male" || nodeSex.nodeName=="female")){
													if(nodeSex.nodeName=="male"){ avatars.scars.scar[contScars-1].male=[];}
													else if(nodeSex.nodeName=="female"){ avatars.scars.scar[contScars-1].female=[];}
													for(var a=0;a<nodeSex.attributes.length;a++){
														atrib = nodeSex.attributes[a];
														if(atrib.nodeName=="box"){
															     if(nodeSex.nodeName=="male")  { avatars.scars.scar[contScars-1].male.box   = atrib.nodeValue;}
															else if(nodeSex.nodeName=="female"){ avatars.scars.scar[contScars-1].female.box = atrib.nodeValue;}
														}
													}
												}
											}
										}
									}
								}else if(nodeAvatars.nodeType == 1 && nodeAvatars.nodeName=="haircolors"){
									avatars.haircolors=[];
									for(var a=0;a<nodeAvatars.attributes.length;a++){
										atrib = nodeAvatars.attributes[a];
										if(atrib.nodeName=="blackcolor"){
											avatars.haircolors.blackcolor = atrib.nodeValue;
										}else if(atrib.nodeName=="blackcolor"){
											avatars.haircolors.blackcolor = atrib.nodeValue;
										}else if(atrib.nodeName=="whitecolor"){
											avatars.haircolors.whitecolor = atrib.nodeValue;
										}else if(atrib.nodeName=="antialiase"){
											avatars.haircolors.antialiase = atrib.nodeValue;
										}
									}
									var contRecolors=0;
									avatars.haircolors.colornew= new Array();
									for(var h=0; h<nodeAvatars.childNodes.length; h++){
										nodeRecolor = nodeAvatars.childNodes[h];
										if(nodeRecolor.nodeType == 1 && nodeRecolor.nodeName=="colornew"){
											contRecolors++;
											avatars.haircolors.colornew[contRecolors-1]=[];
											for(var a=0;a<nodeRecolor.attributes.length;a++){
												atrib = nodeRecolor.attributes[a];
												if(atrib.nodeName=="name"){
													avatars.haircolors.colornew[contRecolors-1].name = atrib.nodeValue;
												}else if(atrib.nodeName=="recolor"){
													avatars.haircolors.colornew[contRecolors-1].recolor = atrib.nodeValue;
												}else if(atrib.nodeName=="gama"){
													avatars.haircolors.colornew[contRecolors-1].gama = atrib.nodeValue;
												}
											}
										}
									}/**/
								}
							}
						}
					}
				}
			}
		}
	}
	/*alert(
		"path ("+avatars.races.boxes+"): "+path+avatars.races.path+"\n"+
		"races: "+avatars.races.race[avatars.races.race.length-1].name +"\n"+
		"(male) → "+ avatars.races.race[avatars.races.race.length-1].male.box +"\n"+
		"(female) → "+ avatars.races.race[avatars.races.race.length-1].female.box
	);/**/
	/*alert(
		"hairscolor: "+
		avatars.haircolors.colorold +" → "+ 
		avatars.haircolors.colornew[avatars.haircolors.colornew.length-1].color +")"
	);/**/
}