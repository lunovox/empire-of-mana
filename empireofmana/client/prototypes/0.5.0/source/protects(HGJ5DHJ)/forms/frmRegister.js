function onBtnRegisterCancel(){
	showForm('frmLogin');
	document.body.style.cursor='default';
}
function onBtnRegisterDone(){
	var divLoginTesto = document.getElementById("divLoginTesto");
	var divRegisterTesto = document.getElementById("divRegisterTesto");
	var btnLoginRegistrar = document.getElementById("btnLoginRegistrar");
	var btnRegisterDone = document.getElementById("btnRegisterDone");
	var txtRegisterEmail = document.getElementById("txtRegisterEmail");
	var txtRegisterSenha = document.getElementById("txtRegisterSenha");
	var radRegisterSex = document.getElementsByName("radRegisterSex");
	
	
	if((txtRegisterEmail.value = toTrim(txtRegisterEmail.value))!=""){
		if(ifFormatMail(txtRegisterEmail.value)){
			if(txtRegisterSenha.value!=""){
				if(txtRegisterSenha.value.length>=8){
					if(txtRegisterSenhaConfirmada.value!=""){
						if(txtRegisterSenha.value==txtRegisterSenhaConfirmada.value){
							if(radRegisterSex[0].checked || radRegisterSex[1].checked){
								btnLoginRegistrar.disabled=true;
								//setOpacity(btnLoginRegistrar, 50);
								btnRegisterDone.disabled=true;
								//setOpacity(btnRegisterDone, 50);
								divLoginTesto.innerHTML = "Registrando! Espere um momento...";
								divLoginTesto.style.display="block";
								document.body.style.cursor='wait';
								var protocolo = (
									"register?"+
									escape(txtRegisterEmail.value)+"&"+
									escape(txtRegisterSenha.value)+"&"+
									(radRegisterSex[0].checked?"M":"F")
								);
								showForm('frmLogin');
								wSocket.send(protocolo);
							}else{
								showAviso(divRegisterTesto,"<marquee>Selecione  o seu '<font color='#FFFFFF'><b>Gênero Sexo</b></font>'!</marquee>");
							}
						}else{
							showAviso(divRegisterTesto,"<marquee>A '<font color='#FFFFFF'><b>Senha</b></font>' e a '<font color='#FFFFFF'><b>Repetição de Senha</b></font>' devem ser iguais!</marquee>");
							txtRegisterSenhaConfirmada.value="";
							txtRegisterSenhaConfirmada.focus();
						}
					}else{
						showAviso(divRegisterTesto,"<marquee>Favor digite a '<font color='#FFFFFF'><b>Repetição de Senha</b></font>'!</marquee>");
						txtRegisterSenhaConfirmada.focus();
					}
				}else{
					showAviso(divRegisterTesto,"<marquee>Favor digite a senha de tamanho <font color='#FFFFFF'><b>mínimo de 8 caracteres</b></font>!</marquee>");
					txtRegisterSenha.focus();
				}
			}else{
				showAviso(divRegisterTesto,"<marquee>Favor digite sua '<font color='#FFFFFF'><b>Senha</b></font>'!</marquee>");
				txtRegisterSenha.focus();
			}
		}else{
			showAviso(divRegisterTesto,"<marquee>Favor digite seu <font color='#FFFFFF'><b>Email Verdadeiro</b></font>!</marquee>");
			txtRegisterEmail.focus();
		}
	}else{
		showAviso(divRegisterTesto,"<marquee>Favor digite seu <font color='#FFFFFF'><b>Email</b></font>!</marquee>");
		txtRegisterEmail.focus();
	}
}