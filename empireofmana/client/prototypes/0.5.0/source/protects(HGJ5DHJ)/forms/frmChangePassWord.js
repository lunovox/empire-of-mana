function onBtnChangePassWordCancel(){
	showForm('frmCharList');
}
function onBtnChangePassWordDone(){
	var divCharListTesto = document.getElementById("divCharListTesto");
	var divChangePassWord = document.getElementById("divChangePassWord");
	var txtLogin = document.getElementById("txtLogin");
	var txtChangePassWordSenhaAntiga = document.getElementById("txtChangePassWordSenhaAntiga");
	var txtChangePassWordSenha = document.getElementById("txtChangePassWordSenha");
	var txtChangePassWordSenhaConfirmada = document.getElementById("txtChangePassWordSenhaConfirmada");
	var btnCharListChangePassWord = document.getElementById("btnCharListChangePassWord");
	var btnCharListCancel = document.getElementById("btnCharListCancel");
	var btnCharListSelectChar = document.getElementsByName("btnCharListSelectChar");
	var btnCharListDeleteChar = document.getElementsByName("btnCharListDeleteChar");
	var btnCharListMakerChar = document.getElementsByName("btnCharListMakerChar");
	//alert(btnCharListMakerChar.length);
	if(txtChangePassWordSenhaAntiga.value!=""){
		if(txtChangePassWordSenha.value!=""){
			if(txtChangePassWordSenhaConfirmada.value!=""){
				if(txtChangePassWordSenha.value==txtChangePassWordSenhaConfirmada.value){
					btnCharListChangePassWord.disabled=true;
					btnCharListCancel.disabled=true;
					for($i=0;$i<btnCharListMakerChar.length;$i++){
						btnCharListSelectChar[$i].disabled=true;
						btnCharListDeleteChar[$i].disabled=true;
						btnCharListMakerChar[$i].disabled=true;
					}
					document.body.style.cursor='wait';
					showAviso(divCharListTesto,"<b>TROCANDO SENHA!</b><br/>Espere um momento...");
					var protocolo = (
						"newpass?"+
						escape(txtLogin.value)+"&"+
						escape(txtChangePassWordSenhaAntiga.value)+"&"+
						escape(txtChangePassWordSenha.value)
					);
					showForm('frmCharList');
					wSocket.send(protocolo);
				}else{
					showAviso(divChangePassWord,"Digite a <b><u>Senha Nova</u></b> e a <b><u>Repetição</u></b> devem ser iguais!");
					txtChangePassWordSenhaConfirmada.value="";
					txtChangePassWordSenhaConfirmada.focus();
				}
			}else{
				showAviso(divChangePassWord,"Digite a <b><u>Repetição de Nova Senha</u></b>!");
				txtChangePassWordSenhaConfirmada.focus();
			}
		}else{
			showAviso(divChangePassWord,"Digite a <b><u>Senha Nova</u></b>!");
			txtChangePassWordSenha.focus();
		}
	}else{
		txtChangePassWordSenhaAntiga.focus();
		showAviso(divChangePassWord,"Digite a <b><u>Senha Atual</u></b>!");
	}
}