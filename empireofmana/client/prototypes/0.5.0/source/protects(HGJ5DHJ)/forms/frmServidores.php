<table id="frmServidores" class="painel" style="left:40px; top:200px; display:block;">
	<tr>
		<td colspan="2" class="titleForms"><b>SERVIDORES</b></td>
	</tr>
	<tr>
		<td align="center" colspan="2">
			<div id="divServidoresTesto" class="aviso"></div>
		</td>
	</tr>
	<tr>
		<td>
			<select id="slcServidoresLista" size="5" class="bigselect" style="width:250px; height:75px;" disabled>
				<option class="server"
					style="padding:4px; padding-left:24px; background:url(images/sword.gif) 2px 5px no-repeat;"
					value="http://www.tuatec.com.br/empireofmana/server/prototypes/0.2.0/server-8888-remote.xml"
				><b>Pandora</b> (EOM Oficial)</option>
				<option 
					style="padding:4px; padding-left:24px; background:url(images/tool.png) 2px 5px no-repeat;"
					value="../../../../server/prototypes/0.2.0/server-8888-local.xml"
				><b>Área de Teste</b> (LocalHost)</option>
			</select>
		</td>
		<td align="center">
			<button disabled id="btnServidoresConectar" style="width:100"
				title="Conecta a um dos servidores ativos que foram selecionados!"
				onclick="onBtnServidoresConectar();"
			><img src="images/icons/icon-connect-16x16.png" align="top"> Conectar</button><br/>
			<button disabled id="btnServidoresAnexar"  style="width:100"
				title="Ponha o [Empire of Mana] em seu site!"
				onclick="onBtnServidoresAnexar();"
			><img src="images/icons/icon-anexar-9x15.gif" align="top"> Anexar</button><br/>
			<button disabled id="btnServidoresDestacar"  style="width:100"
				title="Destaca a janela ou atualiza janela destacada!"
				onclick="onBtnServidoresDestacar();"
			><img src="images/icons/icon-cut-16x16.png" align="top"> Destacar</button> 
		</td>
	</tr>
	<tr>
		<td align="center" colspan="2">
			<div id="divServidoresMarceting" style="display:block; background:rgba(255,255,255,0.15); border-radius:5; padding:5px 5px 5px 5px;">
				<a target='_blank' href='http://www.ubuntu-br.org/download' title='Adaptado ao Linux'><img src="images/selo_linux.png"></a>
				<img src="images/selo_windows.png" title="Adaptado ao Windows">
				<a target='_blank' href='http://code.google.com/p/empire-of-mana/wiki/P2W' title='Massively Multiplayer Online Role-Playing Game → Semi-Gratuito para Navegadores'><img src="images/selo_mmorpg-free-p2w.png"></a><br/>
				<!-- <a target='_blank' href='http://pt.wikipedia.org/wiki/Opensource' title='Veja a licensa deste Jogo'><img src="images/selo_opensource.png"></a> -->
				<a target='_blank' href='http://fusion.google.com/add?source=atgs&moduleurl=http%3A//tuatec.com.br/empireofmana/client/prototypes/0.5.0/gadget-eom.xml%3Fupdate%3D2011-03-27-21-02' title='Clique para adicionar o jogo ao seu iGoogle'><img src="images/selo_add2iGoogle.gif"></a>
				<a target='_blank' href='http://www.tuatec.com.br' title='Clique para ir ao site dos desenvolvedores.'><img src="images/selo_tuatec.jpg"></a>
			</div>
		</td>
	</tr>
</table>