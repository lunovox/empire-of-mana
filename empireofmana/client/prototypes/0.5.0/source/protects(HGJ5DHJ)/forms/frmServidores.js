function onBtnServidoresConectar(){
	var divServidoresTesto = document.getElementById("divServidoresTesto");
	var slcServidoresLista = document.getElementById("slcServidoresLista");
	if(slcServidoresLista.selectedIndex>=0){
		document.body.style.cursor='wait';
		var divLoginTesto = document.getElementById("divLoginTesto");
		var btnServidoresConectar = document.getElementById("btnServidoresConectar");
		var btnServidoresDestacar = document.getElementById("btnServidoresDestacar");
		showAviso(divServidoresTesto,"<b>Conectando!</b> Espere um momento...");
		slcServidoresLista.disabled=true;
		btnServidoresConectar.disabled=true;
		btnServidoresDestacar.disabled=true;
		serverXML = new ClassServerXML(slcServidoresLista.value);
		if(serverXML.getWebSocket().toString()!=""){
			wSocket = new WebSocket(serverXML.getWebSocket());
//################################################################################################################################################
			wSocket.onopen = function(){
				showAviso(divServidoresTesto,"Esperando resposta do servidor...");
				document.body.style.cursor='wait';
				slcServidoresLista.disabled=true;
				btnServidoresConectar.disabled=true;
				btnServidoresDestacar.disabled=true;
				//wSocket.send("conect?me");//Não precisa pedir para se conectar.
				ConectTimeOut=setTimeout("onBtnLoginCancelar()",5000); // 5.000ms(milisegundos)=5s
			};
//################################################################################################################################################
			wSocket.onclose = onTimeOut;
			wSocket.onerror = onTimeOut;
//################################################################################################################################################
			wSocket.onmessage = function(evt){
				detectComand(evt.data);
			};
		}
	}else{
		showAviso(divServidoresTesto,"<marquee>Selecione um servidor abaixo!!!</marquee>");
	}
}
function onBtnServidoresDestacar(){
	destacado=true;
	loadBackground(); loadMusic(); 
	showForm(''); 
	abrirJanela('../index.php',760,480);
}
function onBtnServidoresAnexar(){
	var divAnexosTesto = document.getElementById("divAnexosTesto");
	var dibAnexoSiteEndereco = document.getElementById("dibAnexoSiteEndereco");
	var dibAnexoSiteCodigo = document.getElementById("dibAnexoSiteCodigo");
	var txtAnexosSiteEndereco = document.getElementById("txtAnexosSiteEndereco");
	showAviso(divAnexosTesto,"");
	dibAnexoSiteEndereco.style.display="block";
	dibAnexoSiteCodigo.style.display="none";
	txtAnexosSiteEndereco.value="";
	showForm("frmAnexos");
}
function onTimeOut(){
	clearTimeout(ConectTimeOut);
	wSocket.close();
	showAviso(divServidoresTesto,"<marquee>Conexão com este servidor está fechada!</marquee>");
	showForm("frmServidores");
	slcServidoresLista.disabled=false;
	btnServidoresConectar.disabled=false;
	btnServidoresDestacar.disabled=false;
	document.body.style.cursor='default';
}