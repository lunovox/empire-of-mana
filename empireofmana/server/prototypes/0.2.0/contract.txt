	O usuário deste software doravante é chamado de 
simplesmente de "JOGADOR".

O JOGADOR se compromete a:

   * Não ofender dos demais jogadores usando palavras 
     imorais ou preconceituosas.

   * Não ofender os organizadores e colaboradores deste 
     software.

   * Não promover preconceitos raciais, sexuais, étnicos, 
     religiosos.

   * Não promover uso de narcóticos, cigarros ou bebidas 
     alcoólicas.

   * Não promover uso de outros servidores (Salvo jogos 
     Open Source).

   * Não repassar senhas para outros jogadores sobre 
     nenhuma hipótese.

   * Não utilizar softwares BOTs e softwares facilitadores 
     de nenhum tipo.

O JOGADOR possui o direito de:

   * Repassar ou vender seus itens para qualquer outro 
     jogador ou NPC, a qualquer momento, por qualquer 
     motivo. (Salvo itens do tipo “Soul-Bound”)

   * Socializar-se com um ou mais jogadores.

   * Criar grupos sociais doravante chamados de Guilda.

   * Atacar e saquear virtualmente o personagem de 
     qualquer jogador, a qualquer momento, por qualquer 
     motivo.

Os responsáveis por este servidor se comprometem a:

   * Não vender e nem divulgar informações pessoais dos 
     jogadores para terceiros.

Os responsáveis por este servidor possuem o direto de:

   * Não vender Mithrils(Moeda VIP) enquanto este servidor
     possuir banco de dados instável.

   * Não obrigar devoluções de itens emprestados que 
     qualquer jogador tem feito a outro.

   * Tratar qualquer jogador financiador de forma 
     diferenciada.

   * Vender ou alugar acesso de itens, mapas, serviços e 
     privilégios a jogadores financiadores.

   * Apagar Contas ou Personagens que não tiverem sido 
     acessado por tempo superior a 3 meses.

   * alterar a conta e os personagens de qualquer jogador
     por motivo de manutensão do servidor.

   * Prender, bloquear, apagar a conta e os personagens 
     de qualquer jogador infrator das regras deste servidor 
     sem qualquer aviso prévio.

   * Não se responsabilizar pela estabilidade das contas 
     do servidor em faze de desenvolvimento.
