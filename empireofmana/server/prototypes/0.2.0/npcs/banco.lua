---------------------------------------------------------------------------------------
-- Fun��o de Banco                                                                   --
---------------------------------------------------------------------------------------
--  Copyright 2004-2010 - Projeto Mundo dos Drag�es - TIME MDD                       --
--                                                                                   --
--  Esse arquivo faz parte do Projeto Mundo dos Drag�es.                             --
--                                                                                   --
--  Arquivo Alterado do seu original do Projeto TMW ( themanaworld.sourceforge.net ) --
--                                                                                   --                                                                           --
--  O Projeto Mundo dos Drag�es � um software livre; voc� pode redistribu�-lo e/ou   -- 
--  modific�-lo sob os termos da GNU General  Public License como foi publicada pela --
--  Free Software Foundation; na vers�o 2 da Licen�a.                                -- 
---------------------------------------------------------------------------------------

function Banqueiro(npc, ch)
    do_message(npc, ch, "Bem-Vindo ao Banco!")
    local account = tonumber(get_quest_var(ch, "BankAccount"))
    local result = -1
    do_wait()

    if (account == nil) then --Cria��o Inicial da conta, se necess�rio
        do_message(npc, ch, "Ol�! Deseja Criar uma Conta no Banco ?")
        result = do_choice(npc, ch, "Sim", "N�o")
        if (result == 1) then
            mana.chr_set_quest(ch, "BankAccount", 5)
            do_message(npc, ch, "A sua Conta foi Criada. Voc� ganhou um Bonus de 5GP.")
            account = 5
        end
    end

    if (account ~= nil) then --Se o Jogador j� possui uma conta
        local money = 0
        local input = 0
        result = 1
        while (result < 3) do --Enquanto eles escolherem uma op��o v�lida isso n�o � "Esque�a"
            account = tonumber(get_quest_var(ch, "BankAccount")) --Why do I need to convert this?
            do_message(npc, ch, "Seu Balan�o: " .. account .. ".\nSeu Dinheiro: " .. mana.chr_money(ch) .. ".")
            result = do_choice(npc, ch, "Depositar", "Retirar", "Esque�a")
            if (result == 1) then --Depositar
                money = mana.chr_money(ch);
                if (money > 0) then --Certifique-se de que o jogador possua a quantia a ser depositada
                    do_message(npc, ch, "Quanto deseja depositar ? (0 ir� cancelar)")
                    input = do_ask_integer(npc, ch, 0, money, 1)
                    do_wait()
                    money = mana.chr_money(ch)
                    if (input > 0 and input <= money) then --Certifique-se de que nada estranho ir� acontecer caso eles tentem depositar mais do que possuam
                        mana.chr_money_change(ch, -input)
                        mana.chr_set_quest(ch, "BankAccount", account + input)
                        do_message(npc, ch, input .. " Moedas depositadas.")
                    elseif (input > money) then --Escolhido mais do que eles tinham
                        do_message(npc, ch, "Voc� n�o possui essa quantia. Mas voc� apenas tentou ....")
                    end
                else
                    do_message(npc, ch, "Voc� n�o possui nenhum dinheiro para depositar !")
                end
            elseif (result == 2) then --Retirada
                if (account > 0) then --Certifique-se de eles possuam dinheiro a ser retirado.
                    do_message(npc, ch, "Quanto voc� deseja que seja retirado ? (0 ir� cancelar)")
                    input = do_ask_integer(npc, ch, 0, account, 1)
                    if (input > 0 and input <= account) then --Certifique-se de que nada estranho ir� acontecer caso eles tentem retirar mais do que possuam
                        mana.chr_money_change(ch, input)
                        mana.chr_set_quest(ch, "BankAccount", account - input)
                        do_message(npc, ch, input .. " Moedas Retiradas.")
                    elseif (input > account) then --Escolhido mais do que eles possu�am
                        do_message(npc, ch, "Voc� n�o possui essa quantia em sua conta. Mas voc� apenas tentou ...")
                    end
                else
                    do_message(npc, ch, "A sua conta est� vazia!")
                end
            end
        end --Isso acaba com o loop do while
    end

    do_message(npc, ch, "Obrigado. Volte Sempre!")
    do_npc_close(npc, ch)
end