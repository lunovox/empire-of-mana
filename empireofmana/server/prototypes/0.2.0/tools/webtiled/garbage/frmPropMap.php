<!-- ############# frmPropMap.php ################################################################# -->
<div id="frmTilesetNovo" class="frmPropMap">
	<script>
		function frmPropMapShowAba($idAba){
			frmPropMapTitle.style.display='none';
			frmPropMapBackGroundLayer.style.display='none';
			frmPropMapOverGroundLayer.style.display='none';
			frmPropMapBackGroundMusic.style.display='none';
			$idAba.style.display='block';
		}
	</script>
	<div class="PainelPropriedadeTitle"><font size="3"><b>Propriedades</b></font></div>
	<table border="0">
		<TR>
			<td valign="top">
				<div style="float:right; padding:3px 3px 0px 0px;">
					<button style="width:150px" onclick="frmPropMapShowAba(frmPropMapTitle)">Propriedade do Mapa</button><br/>
					<button style="width:150px" onclick="frmPropMapShowAba(frmPropMapBackGroundLayer)">Imagem de Fundo</button><br/>
					<button style="width:150px" onclick="frmPropMapShowAba(frmPropMapOverGroundLayer)">Imagem de Cobertura</button><br/>
					<button style="width:150px" onclick="frmPropMapShowAba(frmPropMapBackGroundMusic)">Música de Fundo</button><br/>
					<button style="width:150px" disabled>pvparea</button><br/>
					<button style="width:150px" disabled>tradearea</button><br/>
					<button style="width:150px" disabled>magicarea</button><br/>
				</div>
			</td>
		</TR>
	</table>
	<select id="frmTilesetNovoArquivo" style="width:250px">
		<option value="">Nenhum</option>
		<?php
			//echo "<!-- ".$_SERVER['PHP_SELF']." -->\n";
			$PastaReal="../musics/";
			$Capsula=@opendir($PastaReal.$Local);
			unset($lista);
			while ($Conteudo=@readdir($Capsula)) {
				//echo "<!-- ".$PastaReal.$Conteudo."-->\n";
				if($Conteudo!="." and $Conteudo!=".." and $Conteudo!="db"){
					$URL=$PastaReal.$Conteudo;
					if(is_file($URL)){
						$Partes=explode(".",$Conteudo);
						$Tipo=strtolower($Partes[count($Partes)-1]);
						if($Tipo=="ogg" or $Tipo=="ogv" or $Tipo=="mp3"){
							$lista[]=$Conteudo;
						}
					}
				}
			}
			sort($lista);
			for($i=0;$i<sizeof($lista);$i++){
				echo "\n\t<option value=\"".$lista[$i]."\">".$lista[$i]."</option>";
			}/**/
			echo "\n";
		?> 
	</select><br>
</div>
<!-- ############################################################################################################# -->