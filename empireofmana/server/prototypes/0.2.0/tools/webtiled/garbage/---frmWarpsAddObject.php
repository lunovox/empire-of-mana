<!-- ############# frmPropriedadeDeMapaDiv.php ################################################################# -->
<div id="frmPropriedadeDeWarpsAddObject" class="frmPropriedadeDeMapaDiv">
	<div class="PainelPropriedadeTitle"><font size="3"><b>Propriedades</b></font></div>
	<table border="0">
		<TR>
			<td valign="top">
				<div style="float:right; padding:3px 3px 0px 0px;">
					<button style="width:150px" onclick="frmPropriedadeDeMapaDivShowAba(frmPropriedadeDeMapaDivTitle)">Tittle</button><br/>
					<button style="width:150px" onclick="frmPropriedadeDeMapaDivShowAba(frmPropriedadeDeMapaDivBackGroundLayer)">Background Layer</button><br/>
					<button style="width:150px" onclick="frmPropriedadeDeMapaDivShowAba(frmPropriedadeDeMapaDivOverGroundLayer)">Overground Layer</button><br/>
					<button style="width:150px" onclick="frmPropriedadeDeMapaDivShowAba(frmPropriedadeDeMapaDivBackGroundMusic)">Background Music</button><br/>
					<button style="width:150px" disabled>pvparea</button><br/>
					<button style="width:150px" disabled>tradearea</button><br/>
					<button style="width:150px" disabled>magicarea</button><br/>
				</div>
			</td>
			<td valign="top">
				<div>
					<div id="frmPropriedadeDeMapaDivTitle" class="frmPropriedadeDeMapaDiv" style="display:block;">
						<b>Nome:</b> <input type="text" value="Vale dos Iniciantes" style="width:100%"><br><br>
						<b>Comentário:</b>
						<textarea rows="4" style="width:100%">Aqui fica um uma breve explicação da razão de existir este mapa. E sobre o que o jogadores deverá encontrar aqui.</textarea>
						<p align="center">
							<nobr>
								<font size="2"><b>Width:</b></font> 
								<input type="number" min="1" max="300" value="20" step="1" style="width:40px;" disabled/>
							</nobr>&nbsp;&nbsp;&nbsp;&nbsp;
							<nobr>
								<font size="2"><b>Height:</b></font> 
								<input type="number" min="1" max="300" value="15" step="1" style="width:40px;" disabled/>
							</nobr>
						</p>
					</div>
					<div id="frmPropriedadeDeMapaDivBackGroundLayer" class="frmPropriedadeDeMapaDiv">
						<div class="PainelPropriedadeTitle"><font size="3"><b>Background Layer</b></font></div>
						<select style="width:150px">
							<option value="">Nenhum</option>
							<option value="../backgrounds/sea.png">sea.png</option>
						</select><br><br>
						<p>
							<b>Left (<spam id="frmPropriedadeDeMapaDivBackGroundLayerLeftDiv">0</spam>):</b><br>
							<input 
								type="range" min="0" max="640" value="0" step="10" style="width:100%;"
								onchange="frmPropriedadeDeMapaDivBackGroundLayerLeftDiv.innerHTML=this.value;"
							/><br>
							<b>Top (<spam id="frmPropriedadeDeMapaDivBackGroundLayerTopDiv">0</spam>):</b><br>
							<input 
								type="range" min="0" max="480" value="0" step="10" style="width:100%;"
								onchange="frmPropriedadeDeMapaDivBackGroundLayerTopDiv.innerHTML=this.value;"
							/>
						</p>
						<p>
							<b>Left Speed (<spam id="frmPropriedadeDeMapaDivBackGroundLayerLeftSpeedDiv">0</spam>):</b><br>
							<input 
								type="range" min="-40" max="40" value="0" step="1" style="width:100%;"
								onchange="frmPropriedadeDeMapaDivBackGroundLayerLeftSpeedDiv.innerHTML=this.value;"
							/><br>
							<b>Top Speed (<spam id="frmPropriedadeDeMapaDivBackGroundLayerTopSpeedDiv">0</spam>):</b><br>
							<input 
								type="range" min="-40" max="40" value="0" step="1" style="width:100%;"
								onchange="frmPropriedadeDeMapaDivBackGroundLayerTopSpeedDiv.innerHTML=this.value;"
							/>
						</p>
					</div>
					<div id="frmPropriedadeDeMapaDivOverGroundLayer" class="frmPropriedadeDeMapaDiv">
						<div class="PainelPropriedadeTitle"><font size="3"><b>Overground Layer</b></font></div>
						<select style="width:150px">
							<option value="">Nenhum</option>
							<option value="../backgrounds/cloud.png">cloud.png</option>
						</select><br>
						<p>
							<b>Left (<spam id="frmPropriedadeDeMapaDivOverGroundLayerLeftDiv">0</spam>):</b><br>
							<input 
								type="range" min="0" max="640" value="0" step="10" style="width:100%;"
								onchange="frmPropriedadeDeMapaDivOverGroundLayerLeftDiv.innerHTML=this.value;"
							/><br>
							<b>Top (<spam id="frmPropriedadeDeMapaDivOverGroundLayerTopDiv">0</spam>):</b><br>
							<input 
								type="range" min="0" max="480" value="0" step="10" style="width:100%;"
								onchange="frmPropriedadeDeMapaDivOverGroundLayerTopDiv.innerHTML=this.value;"
							/>
						</p>
						<p>
							<b>Left Speed (<spam id="frmPropriedadeDeMapaDivOverGroundLayerLeftSpeedDiv">0</spam>):</b><br>
							<input 
								type="range" min="-40" max="40" value="0" step="1" style="width:100%;"
								onchange="frmPropriedadeDeMapaDivOverGroundLayerLeftSpeedDiv.innerHTML=this.value;"
							/><br>
							<b>Top Speed (<spam id="frmPropriedadeDeMapaDivOverGroundLayerTopSpeedDiv">0</spam>):</b><br>
							<input 
								type="range" min="-40" max="40" value="0" step="1" style="width:100%;"
								onchange="frmPropriedadeDeMapaDivOverGroundLayerTopSpeedDiv.innerHTML=this.value;"
							/>
						</p>
					</div>
					<div id="frmPropriedadeDeMapaDivBackGroundMusic" class="frmPropriedadeDeMapaDiv">
						<script>
							function onPlay(){
								var $endereco='../musics/' + frmPropriedadeDeMapaDivBackGroundMusicArquivo.value;
								//alert($endereco);
								//alert(frmPropriedadeDeMapaDivBackGroundMusicLoop.checked);
								frmPropriedadeDeMapaDivBackGroundMusicTocador.loop=frmPropriedadeDeMapaDivBackGroundMusicLoop.checked?"loop":"";
								//alert(frmPropriedadeDeMapaDivBackGroundMusicTocador.loop);
								frmPropriedadeDeMapaDivBackGroundMusicTocador.src=$endereco;
								frmPropriedadeDeMapaDivBackGroundMusicTocador.play();
							}
						</script>
						<div class="PainelPropriedadeTitle"><font size="3"><b>Background Music</b></font></div>
						<select id="frmPropriedadeDeMapaDivBackGroundMusicArquivo" style="width:250px" onchange="onPlay()">
							<option value="">Nenhum</option>
							
							<option value="Amure_-_01_Southern_nights.ogg">Amure - 01 Southern nights</option>
							
							<option value="Butterfly_Tea_-_01_Introduction.ogg">Butterfly Tea - 01 Introduction</option>
							<option value="Butterfly_Tea_-_02_A_Simple_Life.ogg">Butterfly Tea - 02 A Simple Life</option>
							
							<option value="Epic_Soul_Factory_-_02_TITAN.ogg">Epic Soul Factory - 02 TITAN</option>
							<option value="Epic_Soul_Factory_-_07_DUSTY_UNIVERSE.ogg">Epic Soul Factory - 07 DUSTY UNIVERSE</option>
							<option value="Epic_Soul_Factory_-_08_LA_BUSQUEDA_DE_IANNA.ogg">Epic Soul Factory - 08 LA BUSQUEDA DE IANNA</option>
							<option value="Epic_Soul_Factory_-_09_CREO_EN_TI.ogg">Epic Soul Factory - 09 CREO EN TI</option>
							
							
							<option value="Petite_Viking_-_05_THE_SOUL.ogg">Petite Viking - 05 THE SOUL</option>
							<option value="Petite_Viking_-_06_MOSCOVW.ogg">Petite Viking - 06 MOSCOVW</option>
							
							<option value="theme01.ogg">theme01</option>
							<option value="theme02.ogg">theme02</option>
							<option value="theme03.ogg">theme03</option>
							<option value="theme04.ogg">theme04</option>
						</select><br>
						<label><input id="frmPropriedadeDeMapaDivBackGroundMusicLoop" type="checkbox" checked="true" onchange="onPlay()"/>Repetir ao finalizar</label><br>
						<audio id="frmPropriedadeDeMapaDivBackGroundMusicTocador" autoplay="true">
					</div>
				</div>
			</td>
		</TR>
	</table>
	<div align="right">
		<button onclick="frmPropriedadeDeMapaDiv.style.display='none'">Ok</button>
	</div>
</div>
<!-- ############################################################################################################# -->