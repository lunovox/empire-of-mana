<!-- ############# frmSpawns.php ################################################################# -->
<div id="frmSpawns" style="display:none">
	<br>
	<div class="PainelPropriedadeTitle"><font size="2"><b>Inimigos (Spawns)</b></font></div>
	<nobr>
		<button disabled>+</button>
		<button disabled>-</button>
		<select style="width:70%">
			<option value="">Nome do Inimigo</option>
		</select>
	</nobr>
	<fieldset>
		<legend>Campo de Surgimento:</legend>
		<p align="center">
			<label>
				<font size="2"><b>Longitude (<spam id="frmSpawnsAreaLeft">10</spam>)</b></font> 
				<input 
					type="range" min="0" max="19" value="10" step="1" style="width:95%;"
					onchange="frmSpawnsAreaLeft.innerHTML=this.value;"
				/>
			</label><br>
			<label>
				<font size="2"><b>Latitude (<spam id="frmSpawnsAreaTop">7</spam>)</b></font> 
				<input 
					type="range" min="0" max="14" value="7" step="1" style="width:95%;"
					onchange="frmSpawnsAreaTop.innerHTML=this.value;"
				/>
			</label>
		</p>
		<p align="center">
			<nobr>
				<font size="2"><b>Width:</b></font> 
				<input type="number" min="1" max="20" value="1" step="1" style="width:40px;"/>
			</nobr> 
			<nobr>
				<font size="2"><b>Height:</b></font> 
				<input type="number" min="1" max="15" value="1" step="1" style="width:40px;"/>
			</nobr>
		</p>
	</fieldset>
</div>
<!-- ############################################################################################################# -->
