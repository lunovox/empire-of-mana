function getMousePos($Objeto, $Event){
	// get canvas position
	var $Canvas = $Objeto;
	var top = 0;
	var left = 0;
	while ($Canvas.tagName != 'BODY') {
		top += $Canvas.offsetTop;
		left += $Canvas.offsetLeft;
		$Canvas = $Canvas.offsetParent;
	}
	// return relative mouse position
	var mouseX = $Event.clientX - left + window.pageXOffset;
	var mouseY = $Event.clientY - top + window.pageYOffset;
	return mousePos = {
		x: mouseX,
		y: mouseY
	};
};
function getMouseTile($Objeto, $TileWidth, $TileHeight, $Event){
	var $Mouse = getMousePos($Objeto, $Event);
	var $TileX = parseInt($Mouse.x/$TileWidth)*$TileWidth;
	var $TileY = parseInt($Mouse.y/$TileHeight)*$TileHeight;/**/
	//var $t=parseInt((parseInt($TileY*($Objeto.width/$TileWidth)))+$TileX)/32;
	return parseInt((($TileY*parseInt($Objeto.width/$TileWidth))+$TileX)/$TileWidth);
}