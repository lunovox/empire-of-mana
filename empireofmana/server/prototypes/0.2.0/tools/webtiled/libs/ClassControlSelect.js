function optionAdd(objSelect, newLabel, newValue, beforeLine){
	/*
		Exemplo: 
			* optionAdd(document.getElementById('slcSexo'), "TUATECnologia.COM.BR COOP. LTDA", "www.tuatec.com.br", 0);
			* optionAdd(document.getElementById('slcSexo'), "TUATECnologia.COM.BR COOP. LTDA", "www.tuatec.com.br", null);
			* optionAdd(document.getElementById('slcSexo'), "TUATECnologia.COM.BR COOP. LTDA", "www.tuatec.com.br");
	*/
	var newOption = document.createElement('option');
	newOption.text = newLabel;
	newOption.value = newValue;
	newOption.style.color="#FFFFFF";
	newOption.style.background="#888888";
	if (beforeLine!=null && beforeLine >= 0 && beforeLine<=objSelect.length-1) {
		var objOptionOld = objSelect.options[beforeLine];  
		try {
			objSelect.add(newOption, objOptionOld); // standards compliant; doesn't work in IE
		}
		catch(ex) {
			objSelect.add(newOption, beforeLine); // IE only
		}
	}else{
		try {
			objSelect.add(newOption, null); // standards compliant; doesn't work in IE
		}catch(ex) {
			objSelect.add(newOption); // IE only
		}
	}
}
function optionRemove(objSelect, inLine){
	if (objSelect.length > 0){
		if(inLine!=null && inLine>=0 && inLine<=objSelect.length-1){
			objSelect.remove(inLine);
		}else if(inLine==null){
			objSelect.remove(objSelect.length-1);
		}
	}
	/*var objSelect = document.getElementById('selectX');
	for (var i=objSelect.length-1; i>=0; i--) {
		if (objSelect.options[i].selected) {
			objSelect.remove(i);
		}
	}/**/
}
function optionRemoveAll(objSelect){
	for (var i=objSelect.length-1; i>=0; i--) {
		objSelect.remove(i);
	}
}
function optionSort(objSelect) {
	arrTexts = new Array();
	arrValues = new Array();
	for(i=0; i<objSelect.length; i++)  {
		arrTexts[i] = objSelect.options[i].text;
		arrValues[i] = objSelect.options[i].value;
	}
	arrTexts.sort();
	for(i=0; i<objSelect.length; i++)  {
		objSelect.options[i].text = arrTexts[i];
		objSelect.options[i].value = arrValues[i];
	}
}