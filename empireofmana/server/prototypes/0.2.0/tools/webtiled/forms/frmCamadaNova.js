function addCamada(){
	//var $nome = prompt('Digite o nome da camada:');
	var $nome = txtCamadaNovaNome.value;
	if($nome!=null &&  $nome!=''){
		var $NovaCamada = document.createElement("option");
		$NovaCamada.value = $nome;
		$NovaCamada.selected = true;
		$NovaCamada.style.background=radCamadaNovaTipoTeto.checked?"rgba(128,128,255,0.5)":"rgba(0,255,0,0.5)";
		$NovaCamada.style.color="#000000";
		$NovaCamada.style.borderRadius="5"; 
		$NovaCamada.style.borderWidth="1px";
		$NovaCamada.style.marginTop="2px";
		$NovaCamada.style.marginBottom="2px";
		$NovaCamada.appendChild(document.createTextNode($nome));
		optLayers.getElementsByTagName('optgroup')[radCamadaNovaTipoTeto.checked?3:4].appendChild($NovaCamada);
		showFrmMenu(optLayers.selectedIndex)
		frmCamadaNova.style.display='none';
		
		if(radCamadaNovaTipoTeto.checked){
			layerTop[layerTop.length]=[];
			layerTop[layerTop.length-1].tile = new Array();
			for(var $i=0;$i<(20*15);$i++){
				layerTop[layerTop.length-1].tile[$i]="";
			}
		}else{
			layerGround[layerGround.length]=[];
			layerGround[layerGround.length-1].tile = new Array();
			for(var $i=0;$i<(20*15);$i++){
				layerGround[layerGround.length-1].tile[$i]="";
			}
		}
	}else{
		alert("Digite o nome da camada!");
	}
}