<!-- ############# frmPropriedade.php ################################################################# -->
<div class="PainelPropriedade">
	<p align="center">
		<!-- <font size="2"><b>Camadas:</b></font> -->
		<button onclick="frmCamadaNova.style.display='block'">+</button>
		<button onclick="delCamada()">-</button>
		<button disabled>↑</button>
		<button disabled>↓</button>
	</p>
	
	<select id="optLayers" size="1" onchange="showFrmMenu(this.value)" disabled_>
		<option value="webtiled" style="background-color:rgb(0,128,0); color:white; text-align:center;"><b>Web Tiled EOM</b></option>
		<optgroup label="INSTÂNCIAS" class="InstanciesOptGroupTitle">
			<option class="InstanciesOptGroupElements" value="gates">Portais (Gates)</option>
			<option class="InstanciesOptGroupElements" value="mobs">Máfias (Mobs)</option>
			<option class="InstanciesOptGroupElements" value="npcs">Cidadãos (NPCs)</option>
		</optgroup>
		<optgroup label="PROPRIEDADES" class="PropsOptGroupTitle">
			<option class="PropsOptGroupElements" value="title">Propriedade de Mapa</option>
			<option class="PropsOptGroupElements" value="backgroundlayer">Imagem de Fundo</option>
			<option class="PropsOptGroupElements" value="overgroundlayer">Imagem de Cobertura</option>
			<option class="PropsOptGroupElements" value="backgroundmusic">Música de Fundo</option>
			<option class="PropsOptGroupElements" value="pvparea">pvparea</option>
			<option class="PropsOptGroupElements" value="tradearea">tradearea</option>
			<option class="PropsOptGroupElements" value="magicarea">magicarea</option>
		</optgroup>
		<optgroup label="CAMADAS ESPECIAIS" class="LayersEspecialOptGroupTitle">
			<option class="LayersEspecialOptGroupElements" value="colisions">Colisões</option>
			<option class="LayersEspecialOptGroupElements" value="objects">Objetos</option>
		</optgroup>
		<optgroup label="CAMADAS DE TETO" class="LayersTetoOptGroupTitle"></optgroup>
		<optgroup label="CAMADAS DE PISO" class="LayersPisoOptGroupTitle"></optgroup>
	</select>
	<script src="forms/frmPropriedade.js"></script>
	<? 
		require_once("forms/frmMenuColisions.php");
		require_once("forms/frmMenuGates.php");
		require_once("forms/frmMenuMobs.php");
		require_once("forms/frmMenuWebTiled.php");
		require_once("forms/frmMenuTitle.php");
		require_once("forms/frmMenuBackGroundLayer.php");
		require_once("forms/frmMenuOverGroundLayer.php");
		require_once("forms/frmMenuBackGroundMusic.php");
		require_once("forms/frmMenuInConstruction.php");
		
		require_once("forms/frmCamadaNova.php");
		require_once("forms/frmTilesetNovo.php");
	?>
</div> 
<!-- ############# FINAL de frmPropriedade.php ################################################################# -->