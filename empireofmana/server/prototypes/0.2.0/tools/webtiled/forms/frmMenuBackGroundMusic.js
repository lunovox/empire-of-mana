var Tocador = new Audio();
function onPlay(){
	var $endereco='../../musics/' + frmMenuBackGroundMusicArquivo.value;
	
	/*frmMenuBackGroundMusicTocador.loop=frmMenuBackGroundMusicLoop.checked?"loop":"";
	frmMenuBackGroundMusicTocador.src=$endereco;
	frmMenuBackGroundMusicTocador.play();/**/
	
	if(frmMenuBackGroundMusicArquivo.value!=""){
		Tocador.src = $endereco;
		Tocador.loop=frmMenuBackGroundMusicLoop.checked;
		Tocador.load();
		
		//Tocador.onload = function(){btnPlayPause.disabled=false; Tocador.play();}; // Não está funcionando
		Tocador.addEventListener('canplaythrough', function(){
			frmMenuBackGroundMusicProp.style.display='block';
			btnPlayPause.disabled=false; 
			Tocador.play();
		}, false); 
		Tocador.addEventListener('ended', function(evt) { 
			if(Tocador.loop==false){
				btnPlayPause.value='PLAY';
				//document.getElementById('btnPlayPause').value='Play'; 
			}
		}, false);
	}else{
		frmMenuBackGroundMusicProp.style.display='none';
		Tocador.pause();
		btnPlayPause.value='PAUSE';
		btnPlayPause.disabled=true; 
	}
}
function onPause($obj){
	if(btnPlayPause.value=='PAUSE'){
		Tocador.pause();
		$obj.value='PLAY';
	}else{
		Tocador.play();
		$obj.value='PAUSE';
	}
}