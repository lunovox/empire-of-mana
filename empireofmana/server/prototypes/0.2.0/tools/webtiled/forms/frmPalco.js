var $IfMousePressed=false;
var $ImgPalco = new Image();
function mapaNovo($url){
	$ImgPalco.onload=drawPalco;
	$ImgPalco.src=$url;
	
	layerObject.tile = new Array();
	layerColision.local = new Array()
	for(var $i=0;$i<(20*15);$i++){
		layerObject.tile[$i]="";
		layerColision.local[$i]=false;
	}
}
function cnvPalcoOnMouseUp($Evt){
	$IfMousePressed=false;
	cnvPalcoOnMouseMove($Evt);
}
function cnvPalcoOnMouseDown($Evt){
	$IfMousePressed=true;
	cnvPalcoOnMouseMove($Evt);
}
function cnvPalcoOnMouseMove($Evt){
	if(optLayers.value=="colisions"){
		if($IfMousePressed==true){
			var $t=getMouseTile(cnvPalco, 32, 32, $Evt)
			divPalcoStatus.innerHTML="Colision("+$t+")="+layerColision.local[$t];
			if($Evt.button==0) layerColision.local[$t]=true;
			if($Evt.button==2) layerColision.local[$t]=false;
			drawPalco();
		}
	}
}
function drawPalco(){
	var $ctx = cnvPalco.getContext('2d');
	cnvPalco.width=$ImgPalco.width;
	cnvPalco.height=$ImgPalco.height;
	$ctx.clearRect(0,0,cnvPalco.width,cnvPalco.height);
	$ctx.drawImage($ImgPalco,0,0);
	
//############### Colisões #########################################################################################
	$ctx.save();
	var $Opac=rngMenuColisionsOpacidade.value/100;
	$ctx.fillStyle   = "rgba(255  ,0  ,0,"+$Opac+")";
	$ctx.strokeStyle = "rgba(255,255,0  ,"+$Opac+")";
	$ctx.lineWidth   = 1;
	for(var $i=0;$i<(frmMenuTitleWidth.value*frmMenuTitleHeight.value);$i++){
		if(layerColision.local[$i]!=0 && layerColision.local[$i]!="" && layerColision.local[$i]!=false){
			var $x=parseInt($i%frmMenuTitleWidth.value);
			var $y=parseInt($i/frmMenuTitleWidth.value);
			$ctx.fillRect(
				($x*(cnvPalco.width/frmMenuTitleWidth.value))+1, 
				($y*(cnvPalco.width/frmMenuTitleWidth.value))+1, 
				30, 30
			);
			$ctx.strokeRect(
				($x*(cnvPalco.width/frmMenuTitleWidth.value))+1, 
				($y*(cnvPalco.width/frmMenuTitleWidth.value))+1, 
				30, 30
			);
		}
	}
	$ctx.restore();
//#################################################################################################################
}
function drawGate($i){
	if($i>=0 && $i<gates.length){
		var $ctx = cnvPalco.getContext('2d');
		$ctx.save();
		$ctx.fillStyle   = 'rgba(255,0,255,0.3)';
		$ctx.strokeStyle = 'rgba(255,255,0  ,0.5)';
		$ctx.lineWidth   = 1;
		var $x = gates[$i].areaLeft*32;
		var $y = gates[$i].areaTop*32;
		var $w = gates[$i].areaWidth*32;
		var $h = gates[$i].areaHeight*32;
		$ctx.fillRect($x, $y, $w, $h);
		$ctx.strokeRect($x, $y, $w, $h);
		//$ctx.clearRect (30, 25,  90, 60);
		//$ctx.font = "14pt Verdana";
		$ctx.font = "12pt Courier New";
		$ctx.fillStyle = "rgb(255,255,255)";
		$ctx.fillText(gates[$i].nome,$x+10,$y+20);
		$ctx.restore();
	}
}
function drawGates(){
	drawPalco();
	for(var $i=0;$i<gates.length;$i++){
		if($i==optGates.selectedIndex){
			divPalcoStatus.innerHTML=gates[$i].nome+"("+gates[$i].destMap+":"+gates[$i].destLeft+","+gates[$i].destTop+":"+gates[$i].destDirection+")";
			//alert(divPalcoStatus.innerHTML);
		}
		drawGate($i);
	}
}
cnvPalco.addEventListener('mousedown',cnvPalcoOnMouseDown,false);
cnvPalco.addEventListener('mouseup',cnvPalcoOnMouseUp,false);
cnvPalco.addEventListener('mouseout',cnvPalcoOnMouseUp,false);
cnvPalco.addEventListener('mousemove',cnvPalcoOnMouseMove,false);
mapaNovo("src/background.png");