<!-- ############# frmTilesets.php ################################################################# -->
<div id="frmTilesets" style="display:none;" align="center">
	
	<div class="PainelPropriedadeTitle">
		<select id="optTilesets" onchange="SelTileset()" disabled></select>
		<button onclick="frmTilesetNovo.style.display='block'; selTilesetNovo()">+</button>
		<button disabled>-</button>
	</div>
	<canvas id="cnvTileset"></canvas>
	<p align="left"><font size="2"><b><spam id="divTilesetStatus">Clique no botão de [+] acima para adicionar novo tileset ao mapa!</spam></b></font></p>
	<script src="forms/frmTilesets.js">
		<!--/* São funcões que tem que ficar ao final dasinstancias. */-->
	</script>
</div>
<!-- ############################################################################################ -->
