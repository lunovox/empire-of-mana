<!-- ############# frmTilesetNovo.php ################################################################# -->
<div id="frmTilesetNovo" style="display:none;" class="frmPropMap">
	<script src="forms/frmTilesetNovo.js"></script>
	<center>
		<div class="PainelPropriedadeTitle"><font size="3"><b>Adicionar Tileset ao Mapa</b></font></div><br>
		<select id="optTilesetNovo" onchange="selTilesetNovo()">
			<?php
				//echo "<!-- ".$_SERVER['PHP_SELF']." -->\n";
				$PastaReal="../../tilesets/";
				$Capsula=@opendir($PastaReal.$Local);
				unset($lista);
				while ($Conteudo=@readdir($Capsula)) {
					//echo "<!-- ".$PastaReal.$Conteudo."-->\n";
					if($Conteudo!="." and $Conteudo!=".." and $Conteudo!="db"){
						$URL=$PastaReal.$Conteudo;
						if(is_file($URL)){
							$Partes=explode(".",$Conteudo);
							$Tipo=strtolower($Partes[count($Partes)-1]);
							if($Tipo=="png"){
								$lista[]=$Conteudo;
							}
						}
					}
				}
				sort($lista);
				for($i=0;$i<sizeof($lista);$i++){
					echo "\n\t<option value=\"".$lista[$i]."\">".$lista[$i]."</option>";
				}/**/
				echo "\n";
			?> 
		</select>
		<hr>
		<img id="imgTilesetNovoMiniatura" src_="../../tilesets/<?=$lista[0];?>" width="250px"><br><br>
		<nobr>
			<font size="2"><b>Width:</b></font> 
			<input id="frmTilesetNovoWidth" type="number" min="32" max="32" value="32" step="32" style="width:40px;" disabled/>
		</nobr>&nbsp;&nbsp;&nbsp;
		<nobr>
			<font size="2"><b>Height:</b></font> 
			<input id="frmTilesetNovoHeight" type="number" min="32" max="32" value="32" step="32" style="width:40px;" disabled/>
		</nobr>
		<hr>
		<button onclick="addTileset()">Adicionar</button>
		<button onclick="frmTilesetNovo.style.display='none'">Cancelar</button>
	</center>
</div>
<!-- ############# FINAL de frmTilesetNovo.php ################################################################# -->
