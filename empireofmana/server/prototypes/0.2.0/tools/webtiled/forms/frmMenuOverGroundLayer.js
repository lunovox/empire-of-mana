function selOverGroundImage($obj){
	if($obj.selectedIndex>=1){
		divOverGround.style.display='none';
		document.getElementById('imgViewOverground').src=$obj.value;
		var myImg=new Image();
		myImg.src=$obj.value;
		myImg.onload = function() {
			if(rngOverGroundLayerLeft.value>myImg.width)rngOverGroundLayerLeft.value=myImg.width; 
			rngOverGroundLayerLeft.max=myImg.width;
			divMenuOverGroundLayerLeft.innerHTML=rngOverGroundLayerLeft.value;
			if(rngOverGroundLayerTop.value>myImg.height)rngOverGroundLayerTop.value=myImg.height; 
			rngOverGroundLayerTop.max=myImg.height;
			divMenuOverGroundLayerTop.innerHTML=rngOverGroundLayerTop.value;
			divOverGround.style.display='block';
		};
	}else{
		divOverGround.style.display='none';
	}
}