function addTileset(){
	var $nome = optTilesetNovo.options[optTilesetNovo.selectedIndex].text;
	//var $nome = prompt('Digite o nome do portal:');
	if($nome!=null && $nome!=''){
		var $NomeFormatado = $nome.replace(".PNG","").replace(".png","").replace("_"," ");
		var $ifNew = true;
		for(var $i=0;$i<optTilesets.options.length;$i++){
			if(optTilesets.options[$i].text.toString()==$NomeFormatado.toString()){
				//alert(optTilesets.options[$i].text.toString()+"=="+$NomeFormatado.toString())
				$ifNew=false; break;
			}
		}
		if($ifNew){
			optTilesets.disabled=true;
			optionAdd(optTilesets, $NomeFormatado, null, optTilesets.options.length);
			optTilesets.selectedIndex=optTilesets.options.length-1;
			
			$Tilesets[optTilesets.selectedIndex]=[];
			$Tilesets[optTilesets.selectedIndex].image=new Image();
			$Tilesets[optTilesets.selectedIndex].image.onload = function(){optTilesets.disabled=false;};
			$Tilesets[optTilesets.selectedIndex].image.src="../../tilesets/"+$nome;
			$Tilesets[optTilesets.selectedIndex].tilewidth=frmTilesetNovoWidth.value;
			$Tilesets[optTilesets.selectedIndex].tileheight=frmTilesetNovoHeight.value;
			//alert(frmTilesetNovoWidth.value+"/"+frmTilesetNovoHeight.value)
			//$Tilesets[optTilesets.selectedIndex].url="../tilesets/"+optTilesets.value;
			frmTilesetNovo.style.display='none';
			SelTileset();
		}else{
			alert("O tileset '"+$NomeFormatado+"' já está adicionado!");
		}
	}
}
function selTilesetNovo(){
	var $img = new Image();
	var $url = "../../tilesets/"+optTilesetNovo.value;
	$img.onload = function(){
		document.getElementById('imgTilesetNovoMiniatura').src=$img.src;
		frmTilesetNovoWidth.disabled=true;
		frmTilesetNovoWidth.max=$img.width;
		frmTilesetNovoWidth.value=32;
		frmTilesetNovoWidth.disabled=false;
		
		frmTilesetNovoHeight.disabled=true;
		frmTilesetNovoHeight.max=$img.height;
		frmTilesetNovoHeight.value=32;
		frmTilesetNovoHeight.disabled=false;
	};
	$img.src=$url;
	
}
