var $Tile = [];
var $IfBottonPressed=false;
$Tile.Focalizado=[];
$Tile.Focalizado.ID=0;
$Tile.Focalizado.TileX=0;
$Tile.Focalizado.TileY=0;
$Tile.Selecionado=[];
$Tile.Selecionado.ID=0;
$Tile.Selecionado.TileX1=0;
$Tile.Selecionado.TileY1=0;
$Tile.Selecionado.TileX2=0;
$Tile.Selecionado.TileY2=0;

function SelTileset(){
	var $img = $Tilesets[optTilesets.selectedIndex].image;
	cnvTileset.width=$img.width;
	cnvTileset.height=$img.height;
	cnvTileset.style.width=$img.width;
	cnvTileset.style.height=$img.height;
	var $ctx = cnvTileset.getContext('2d');
	$ctx.save();
	$ctx.clearRect(0,0,cnvTileset.width,cnvTileset.height);
	$ctx.drawImage($img,0,0);
	
	//var $ctx = cnvTileset.getContext('2d');
	$ctx.fillStyle   = 'rgba(255,255,255,0.3)';
	$ctx.strokeStyle = 'rgba(255,255,0  ,0.5)';
	$ctx.lineWidth   = 1;
	$ctx.fillRect(
		$Tile.Focalizado.TileX, $Tile.Focalizado.TileY, 
		$Tilesets[optTilesets.selectedIndex].tilewidth, 
		$Tilesets[optTilesets.selectedIndex].tileheight
	);/**/
	$ctx.strokeRect(
		$Tile.Focalizado.TileX, $Tile.Focalizado.TileY, 
		$Tilesets[optTilesets.selectedIndex].tilewidth, 
		$Tilesets[optTilesets.selectedIndex].tileheight
	);
	//$ctx.clearRect (30, 25,  90, 60);
	
	var $SelW=$Tile.Selecionado.TileX2 + parseInt($Tilesets[optTilesets.selectedIndex].tilewidth) - $Tile.Selecionado.TileX1;
	var $SelH=$Tile.Selecionado.TileY2 + parseInt($Tilesets[optTilesets.selectedIndex].tileheight) - $Tile.Selecionado.TileY1;
	if($Tile.Selecionado.ID==optTilesets.selectedIndex){
		$ctx.fillStyle   = 'rgba(0  ,0  ,255,0.3)';
		$ctx.strokeStyle = 'rgba(255,255,0  ,0.5)';
		$ctx.lineWidth   = 1;
		$ctx.fillRect($Tile.Selecionado.TileX1, $Tile.Selecionado.TileY1, $SelW, $SelH);
		$ctx.strokeRect($Tile.Selecionado.TileX1, $Tile.Selecionado.TileY1, $SelW, $SelH);
		//$ctx.clearRect (30, 25,  90, 60);
	}
	$ctx.restore();

	divTilesetStatus.innerHTML=
		"Focu(X:"+$Tile.Focalizado.TileX+" Y:"+$Tile.Focalizado.TileY+") "+
		"SelT1(X:"+$Tile.Selecionado.TileX1+" Y:"+$Tile.Selecionado.TileY1+") "+
		"SelT2(X:"+$Tile.Selecionado.TileX2+" Y:"+$Tile.Selecionado.TileY2+") "+
		"Tile("+
			"W:"+$Tilesets[optTilesets.selectedIndex].tilewidth+" "+
			"H:"+$Tilesets[optTilesets.selectedIndex].tileheight+
		")<br>";
}
function cnvTilesetOnMouseMove($Evt){
	if(frmTilesets.style.display!='none' && optTilesets.options.length>=1){
		var $Mouse = getMousePos(cnvTileset, $Evt);
		var $TileWidth = $Tilesets[optTilesets.selectedIndex].tilewidth;
		var $TileHeight = $Tilesets[optTilesets.selectedIndex].tileheight;
		var $TilesetID=optTilesets.selectedIndex;
		var $TileX = parseInt($Mouse.x/$TileWidth)*$TileWidth;
		var $TileY = parseInt($Mouse.y/$TileHeight)*$TileHeight;
		if($TilesetID != $Tile.Focalizado.ID || $TileX != $Tile.Focalizado.TileX || $TileY != $Tile.Focalizado.TileY){
			$Tile.Focalizado.ID = $TilesetID;
			$Tile.Focalizado.TileX = $TileX;
			$Tile.Focalizado.TileY = $TileY;
			SelTileset();
		}
	}
}
function cnvTilesetOnMouseDown($Evt){
	if(frmTilesets.style.display!='none' && optTilesets.options.length>=1){
		if($Evt.which==1){
			$Tile.Selecionado.ID=$Tile.Focalizado.ID;
			$Tile.Selecionado.TileX1=$Tile.Focalizado.TileX;
			$Tile.Selecionado.TileY1=$Tile.Focalizado.TileY;
			$Tile.Selecionado.TileX2=$Tile.Focalizado.TileX;
			$Tile.Selecionado.TileY2=$Tile.Focalizado.TileY;
			$IfBottonPressed=true;
			SelTileset();
		}
	}
}
function cnvTilesetOnMouseUp($Evt){
	if(frmTilesets.style.display!='none' && optTilesets.options.length>=1){
		if($Evt.which==1){
			$Tile.Selecionado.ID=$Tile.Focalizado.ID;
			if($Tile.Selecionado.TileX1<=$Tile.Focalizado.TileX){
				$Tile.Selecionado.TileX2=$Tile.Focalizado.TileX;
			}else{
				$Tile.Selecionado.TileX2=$Tile.Selecionado.TileX1;
				$Tile.Selecionado.TileX1=$Tile.Focalizado.TileX;
			}
			if($Tile.Selecionado.TileY1<=$Tile.Focalizado.TileY){
				$Tile.Selecionado.TileY2=$Tile.Focalizado.TileY;
			}else{
				$Tile.Selecionado.TileY2=$Tile.Selecionado.TileY1
				$Tile.Selecionado.TileY1=$Tile.Focalizado.TileY;
			}
			$IfBottonPressed=false;
			SelTileset();
		}
	}
}
cnvTileset.addEventListener('mousemove',cnvTilesetOnMouseMove,false);
cnvTileset.addEventListener('mousedown',cnvTilesetOnMouseDown,false);
cnvTileset.addEventListener('mouseup',cnvTilesetOnMouseUp,false);