function addGate(){
	var $nome = prompt('Digite o nome do portal:');
	//alert('$nome='+$nome);
	if($nome!=null && $nome!=''){
		gates[gates.length]=[];
		//alert('gates.length='+gates.length);
		gates[gates.length-1].nome=$nome;
		gates[gates.length-1].areaLeft=10;
		gates[gates.length-1].areaTop=7;
		gates[gates.length-1].areaWidth=1;
		gates[gates.length-1].areaHeight=1;
		gates[gates.length-1].destMap='this';
		gates[gates.length-1].destLeft=10;
		gates[gates.length-1].destTop=7;
		gates[gates.length-1].destDirection='same';
		optionAdd(optGates, $nome, null, optGates.options.length);
		optGates.selectedIndex=optGates.options.length-1;
		optGates.disabled=false;
		showGate();
	}
}
function delGate(){
	if(optGates.options.length>=1){
		var $resp = confirm('Deseja apagar o portal \''+optGates.options[optGates.selectedIndex].text+'\' definitivamente?');
		if($resp){
			var $I=optGates.selectedIndex;
			var $ProxI=($I==optGates.options.length-1?optGates.options.length-2:$I)
			if(optGates.options.length>=2){
				var $NovoGatesList = new Array();
				for(var $W=0;$W<optGates.options.length;$W++){
					if($W!=$I){
						$NovoGatesList[$NovoGatesList.length]=gates[$W];
					}/*else{
						alert('DEL:gates['+$W+'].nome'+gates[$W].nome);
					}/**/
				}
				gates=$NovoGatesList;
			}else{
				gates = new Array();
			}
			//gates[optGates.options.length-1]=null;
			optionRemove(optGates, $I);
			optGates.selectedIndex=$ProxI;
			showGate();
		}
	}
}
function showGate(){
	if(optGates.options.length>=1){
		var $i=optGates.selectedIndex;
		//alert('gates['+$i+'].nome='+gates[$i].nome);
		txtGateNome.value=gates[$i].nome;
		rngGateAreaLeft.value=gates[$i].areaLeft;
		frmGatesAreaLeft.innerHTML=rngGateAreaLeft.value;
		rngGateAreaTop.value=gates[$i].areaTop;
		frmGatesAreaTop.innerHTML=rngGateAreaTop.value;
		nbrGateAreaWidth.value=gates[$i].areaWidth;
		nbrGateAreaHeight.value=gates[$i].areaHeight;
		
		txtGateDestMap.value=gates[$i].destMap;
		rngGateDestLeft.value=gates[$i].destLeft;
		frmGatesDestLeft.innerHTML=rngGateAreaLeft.value;
		rngGateDestTop.value=gates[$i].destTop;
		frmGatesDestTop.innerHTML=rngGateAreaTop.value;
		optGateDestDirection.value=gates[$i].destDirection;
		frmGatesObject.style.display='block';
		drawGates();
	}else{
		frmGatesObject.style.display='none';
	}
}