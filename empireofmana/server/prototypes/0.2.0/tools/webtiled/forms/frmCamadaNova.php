<!-- ############# frmCamadaNova.php ################################################################# -->
<div id="frmCamadaNova" style="display:none;">
	<script src="forms/frmCamadaNova.js"></script>
	<center>
		<div class="PainelPropriedadeTitle"><font size="3"><b>Adicionar Camada ao Mapa</b></font></div>
		<p>
			<font size="3">
				<nobr><b>Nome:</b> <input id="txtCamadaNovaNome" type="text" value="" style="width:150px"></nobr><br>
				<label><input id="radCamadaNovaTipoTeto" name="TipoDecamada" type="radio"/>Teto</label>
				<label><input id="radCamadaNovaTipoPiso" name="TipoDecamada" type="radio" checked/>Fundo</label>
			</font>
		</p>
		<hr>
		<button onclick="addCamada()">Adicionar</button>
		<button onclick="frmCamadaNova.style.display='none'">Cancelar</button>
	</center>
</div>
<!-- ############################################################################################################# -->
