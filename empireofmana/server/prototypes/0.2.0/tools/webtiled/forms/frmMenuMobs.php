<!-- ############# frmMobs.php ################################################################# -->
<div id="frmMenuMobs" style="display:none">
	<br>
	<!-- <div class="PainelPropriedadeTitle"><font size="2"><b>Máfias (Mobs)</b></font></div> -->
	<script src="forms/frmMenuMobs.js"></script>
	<p align="center">
		<select id="optMobs" onchange="showMob()" style="width:100%;" disabled></select>
		<button onclick="addMob()">+</button>
		<button onclick="delMob()">-</button>
	</p>
	<div id="frmMobsObject" style="display:none">
		<label>
			<font size="2"><b>Nome da Máfia:</b></font><br>
			<input id="txtMobNome" 
				type="text" value="this" style="width:100%;"
				onchange="
					mobs[optMobs.selectedIndex].nome=this.value;
					optMobs.options[optMobs.selectedIndex].text=this.value;
				"
			/>
		</label>
		<div>
			<fieldset>
				<legend>Inimígo:</legend>
				<p align="center">
					<nobr>
						<!-- <font size="2"><b>Mob ID</b></font><br> -->
						<select id="optMobEnemyID"
							style="width:100%" onchange="
							mobs[optMobs.selectedIndex].enemyID=this.value;
							//alert('mobs['+optMobs.selectedIndex+'].enemyID='+mobs[optMobs.selectedIndex].enemyID);
						">
							<option value="1">0001: Morcego</option>
							<option value="2">0002: Tartaruga</option>
						</select><br>
						<font size="2"><b>Quantia Gerada:</b></font> 
						<input id="nbrMobAmount"
							type="number" min="1" max="50" value="1" step="1" style="width:40px;"
							onchange="mobs[optMobs.selectedIndex].amount=this.value;"
						/>
					</nobr>
			</fieldset><br>
			<fieldset>
				<legend>Campo de Surgimento:</legend>
					<label>
						<font size="2"><b>Longitude (<spam id="frmMobsAreaLeft">10</spam>ts)</b></font> 
						<input id="rngMobAreaLeft"
							type="range" min="0" max="19" value="10" step="1" style="width:100%;"
							onchange="
								mobs[optMobs.selectedIndex].arenaLeft=this.value;
								frmMobsAreaLeft.innerHTML=this.value;
							"
						/>
					</label><br>
					<label>
						<font size="2"><b>Latitude (<spam id="frmMobsAreaTop">7</spam>ts)</b></font> 
						<input id="rngMobAreaTop"
							type="range" min="0" max="14" value="7" step="1" style="width:100%;"
							onchange="
								mobs[optMobs.selectedIndex].arenaTop=this.value;
								frmMobsAreaTop.innerHTML=this.value;
							"
						/>
					</label>
				</p>
				<p align="center">
					<nobr>
						<font size="2"><b>Width:</b></font> <input id="nbrMobAreaWidth" 
							type="number" min="1" max="20" value="1" step="1" style="width:40px;"
							onchange="mobs[optMobs.selectedIndex].arenaWidth=this.value;"
						/>&nbsp;&nbsp;
						<font size="2"><b>Height:</b></font> <input id="nbrMobAreaHeight" 
							type="number" min="1" max="15" value="1" step="1" style="width:40px;"
							onchange="mobs[optMobs.selectedIndex].arenaHeight=this.value;"
						/>
					</nobr>
				</p>
			</fieldset>
		</div>
	</div>
</div>
<!-- ############################################################################################################# -->
