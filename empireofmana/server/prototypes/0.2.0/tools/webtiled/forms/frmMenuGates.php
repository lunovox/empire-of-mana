<!-- ############# frmGates.php ################################################################# -->
<div id="frmMenuGates" style="display:none">
	<br>
	<!-- 	<div class="PainelPropriedadeTitle"><font size="2"><b>Portais (Gates)</b></font></div> -->
	<script src="forms/frmMenuGates.js"></script>
	<p align="center">
		<select id="optGates" onchange="showGate()" style="width:100%;" disabled></select>
		<button onclick="addGate()">+</button>
		<button onclick="delGate()">-</button>
	</p>
	<div id="frmGatesObject" style="display:none">
		<label>
			<font size="2"><b>Nome do Portal:</b></font><br>
			<input id="txtGateNome" 
				type="text" value="this" style="width:100%;"
				onchange="
					gates[optGates.selectedIndex].nome=this.value;
					optGates.options[optGates.selectedIndex].text=this.value;
					drawGates();
				"
			/>
		</label>
		<div>
			<fieldset>
				<legend>Area de Gatilho:</legend>
				<p align="center">
					<label>
						<font size="2"><b>Longitude (<spam id="frmGatesAreaLeft">10</spam>)</b></font> 
						<input id="rngGateAreaLeft"
							type="range" min="0" max="19" value="10" step="1" style="width:100%;" onchange="
							gates[optGates.selectedIndex].areaLeft=this.value;
							frmGatesAreaLeft.innerHTML=this.value;
							drawGates();
						"/>
					</label><br>
					<label>
						<font size="2"><b>Latitude (<spam id="frmGatesAreaTop">7</spam>)</b></font> 
						<input id="rngGateAreaTop"
							type="range" min="0" max="14" value="7" step="1" style="width:100%;" onchange="
							gates[optGates.selectedIndex].areaTop=this.value;
							frmGatesAreaTop.innerHTML=this.value;
							drawGates();
						"/>
					</label>
				</p>
				<p align="center">
					<nobr>
						<font size="2"><b>Width:</b></font> <input id="nbrGateAreaWidth" 
							type="number" min="1" max="20" value="1" step="1" style="width:40px;" onchange="
							gates[optGates.selectedIndex].areaWidth=this.value;
							//alert(gates[optGates.selectedIndex].areaWidth);
							drawGates();
						"/>&nbsp;&nbsp;
						<font size="2"><b>Height:</b></font> <input id="nbrGateAreaHeight" 
							type="number" min="1" max="15" value="1" step="1" style="width:40px;" onchange="
							gates[optGates.selectedIndex].areaHeight=this.value;
							drawGates();
						"/>
					</nobr>
				</p>
			</fieldset>
		</div>
		<div>
			<fieldset>
				<legend>Destino:</legend>
				<label>
					<font size="2"><b>Mapa:</b></font> 
					<input id="txtGateDestMap" 
						type="text" value="this" onchange="
						gates[optGates.selectedIndex].destMap=this.value;
						drawGates();
					"/>
				</label><br>
				<p align="center">
					<label>
						<font size="2"><b>Longitude (<spam id="frmGatesDestLeft">10</spam>)</b></font> 
						<input id="rngGateDestLeft"
							type="range" min="0" max="19" value="10" step="1" style="width:100%;" onchange="
							gates[optGates.selectedIndex].destLeft=this.value;
							frmGatesDestLeft.innerHTML=this.value;
							drawGates();
						"/>
					</label><br>
					<label>
						<font size="2"><b>Latitude (<spam id="frmGatesDestTop">7</spam>)</b></font> 
						<input id="rngGateDestTop" 
							type="range" min="0" max="14" value="7" step="1" style="width:100%;" onchange="
							gates[optGates.selectedIndex].destTop=this.value;
							frmGatesDestTop.innerHTML=this.value;
							drawGates();
						"/>
					</label>
				</p>
				<nobr>
					<font size="2"><b>Direção:</b></font> 
					<select id="optGateDestDirection" onchange="
						gates[optGates.selectedIndex].destDirection=this.value;
						drawGates();
					">
						<option value="south">Sul</option>
						<option value="west">Leste</option>
						<option value="north">Norte</option>
						<option value="lest">Oeste</option>
						<option value="same">Mesma</option>
					</select>
				</nobr>
			</fieldset>
		</div>
	</div>
</div>
<!-- ############################################################################################################# -->
