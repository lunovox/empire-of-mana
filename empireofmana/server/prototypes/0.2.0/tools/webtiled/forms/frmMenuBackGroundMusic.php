<!-- ############# frmMenuBackGroundMusic.php ################################################################# -->
<div id="frmMenuBackGroundMusic" style="display:none">
	<script src="forms/frmMenuBackGroundMusic.js"></script>
	<!-- <div class="PainelPropriedadeTitle"><font size="3"><b>Background Music</b></font></div>-->
	<select id="frmMenuBackGroundMusicArquivo" style="width:250px" sort="true" onchange="onPlay()">
		<option value="">Nenhum</option>
		<?php
			//echo "<!-- ".$_SERVER['PHP_SELF']." -->\n";
			$PastaReal="../../musics/";
			$Capsula=@opendir($PastaReal.$Local);
			unset($lista);
			while ($Conteudo=@readdir($Capsula)) {
				//echo "<!-- ".$PastaReal.$Conteudo."-->\n";
				if($Conteudo!="." and $Conteudo!=".." and $Conteudo!="db"){
					$URL=$PastaReal.$Conteudo;
					if(is_file($URL)){
						$Partes=explode(".",$Conteudo);
						$Tipo=strtolower($Partes[count($Partes)-1]);
						if($Tipo=="ogg" or $Tipo=="ogv" or $Tipo=="mp3"){
							$lista[]=$Conteudo;
						}
					}
				}
			}
			sort($lista);
			for($i=0;$i<sizeof($lista);$i++){
				echo "\n\t<option value=\"".$lista[$i]."\">".$lista[$i]."</option>";
			}/**/
			echo "\n";
		?> 
	</select><br>
	<div id="frmMenuBackGroundMusicProp" align="center" style="display:none">
		<input type="button" id="btnPlayPause" disabled="true" value="PAUSE" onclick="onPause(this)"/>
		<label><input id="frmMenuBackGroundMusicLoop" type="checkbox" checked="true" onchange="Tocador.loop=this.checked;"/>Repetir ao finalizar</label>
	</div>
	<!-- 	<audio id="frmMenuBackGroundMusicTocador" autoplay="true"> -->
</div>
<!-- ############################################################################################################# -->
