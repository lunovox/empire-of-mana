function delCamada(){
	if(optLayers.selectedIndex>=13){
		var $resp = confirm(
			'Deseja apagar a camada \''+optLayers.options[optLayers.selectedIndex].text+'\' definitivamente?'/*+
			'\n('+optLayers.selectedIndex+')'/**/
		);
		if($resp) optionRemove(optLayers, optLayers.selectedIndex);
	}
}
function showFrmMenu($Janela){
	//alert("$Janela="+$Janela);
	frmMenuGates.style.display='none';
	frmMenuMobs.style.display='none';
	frmMenuColisions.style.display='none';
	frmMenuTitle.style.display='none';
	frmMenuBackGroundLayer.style.display='none';
	frmMenuOverGroundLayer.style.display='none';
	frmMenuBackGroundMusic.style.display='none';
	
	frmMenuWebTiled.style.display='none';
	frmMenuInConstruction.style.display='none';
	
	if($Janela=='webtiled'){ frmMenuWebTiled.style.display='block';}
	else if($Janela=='gates'){ frmMenuGates.style.display='block'; drawGates();}
	else if($Janela=='mobs'){ frmMenuMobs.style.display='block';}
	else if($Janela=='colisions'){ frmMenuColisions.style.display='block';}
	
	else if($Janela=='title'){ frmMenuTitle.style.display='block';}
	else if($Janela=='backgroundlayer'){ frmMenuBackGroundLayer.style.display='block';}
	else if($Janela=='overgroundlayer'){ frmMenuOverGroundLayer.style.display='block';}
	else if($Janela=='backgroundmusic'){ frmMenuBackGroundMusic.style.display='block';}
	
	
	else{ frmMenuInConstruction.style.display='block';}
}