<!-- ############# frmMenuBackGroundLayer.php ################################################################# -->
<div id="frmMenuBackGroundLayer" style="display:none">
	<!-- 	<div class="PainelPropriedadeTitle"><font size="3"><b>Background Layer</b></font></div> -->
	<script src="forms/frmMenuBackGroundLayer.js"></script>
	<select style="width:150px" onchange="selBackGroundImage(this)">
		<option value="">Nenhum</option>
		<?php
			//echo "<!-- ".$_SERVER['PHP_SELF']." -->\n";
			$PastaReal="../../backgrounds/";
			$Capsula=@opendir($PastaReal.$Local);
			unset($lista);
			while ($Conteudo=@readdir($Capsula)) {
				//echo "<!-- ".$PastaReal.$Conteudo."-->\n";
				if($Conteudo!="." and $Conteudo!=".." and $Conteudo!="db"){
					$URL=$PastaReal.$Conteudo;
					if(is_file($URL)){
						$Partes=explode(".",$Conteudo);
						$Tipo=strtolower($Partes[count($Partes)-1]);
						if($Tipo=="png" or $Tipo=="jpg"){
							$lista[]=$Conteudo;
						}
					}
				}
			}
			sort($lista);
			for($i=0;$i<sizeof($lista);$i++){
				echo "\n\t<option value=\"../../backgrounds/".$lista[$i]."\">".$lista[$i]."</option>";
			}/**/
			echo "\n";
		?> 
	</select>
	<div id="divBackGround" style="display:none">
		<p align="center"><img id="imgViewBackground" style="width:180px;"></p>
		<p>
			<b>Left (<spam id="divMenuBackGroundLayerLeft">0</spam>px):</b><br>
			<input id="rngBackGroundLayerLeft"
				type="range" min="0" max="0" value="0" step="1" style="width:100%;"
				onchange="divMenuBackGroundLayerLeft.innerHTML=this.value;"
			/><br>
			<b>Top (<spam id="divMenuBackGroundLayerTop">0</spam>px):</b><br>
			<input id="rngBackGroundLayerTop"
				type="range" min="0" max="0" value="0" step="1" style="width:100%;"
				onchange="divMenuBackGroundLayerTop.innerHTML=this.value;"
			/>
		</p>
		<p>
			<b>Left Speed (<spam id="divMenuBackGroundLayerLeftSpeed">0</spam>px/frame):</b><br>
			<input 
				type="range" min="-40" max="40" value="0" step="1" style="width:100%;"
				onchange="divMenuBackGroundLayerLeftSpeed.innerHTML=this.value;"
			/><br>
			<b>Top Speed (<spam id="divMenuBackGroundLayerTopSpeed">0</spam>px/frame):</b><br>
			<input 
				type="range" min="-40" max="40" value="0" step="1" style="width:100%;"
				onchange="divMenuBackGroundLayerTopSpeed.innerHTML=this.value;"
			/>
		</p>
	</div>
</div>
<!-- ############################################################################################################# -->
