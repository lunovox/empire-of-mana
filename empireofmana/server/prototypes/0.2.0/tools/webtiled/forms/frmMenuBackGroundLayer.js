function selBackGroundImage($obj){
	if($obj.selectedIndex>=1){
		divBackGround.style.display='none';
		document.getElementById('imgViewBackground').src=$obj.value;
		var myImg=new Image();
		myImg.src=$obj.value;
		myImg.onload = function() {
			if(rngBackGroundLayerLeft.value>myImg.width)rngBackGroundLayerLeft.value=myImg.width; 
			rngBackGroundLayerLeft.max=myImg.width;
			divMenuBackGroundLayerLeft.innerHTML=rngBackGroundLayerLeft.value;
			if(rngBackGroundLayerTop.value>myImg.height)rngBackGroundLayerTop.value=myImg.height; 
			rngBackGroundLayerTop.max=myImg.height;
			divMenuBackGroundLayerTop.innerHTML=rngBackGroundLayerTop.value;
			divBackGround.style.display='block';
		};
	}else{
		divBackGround.style.display='none';
	}
}