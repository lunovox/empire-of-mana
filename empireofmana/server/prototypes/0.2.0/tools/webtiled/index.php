<html>
	<head> 
		<title>Web Tiled EOM</title> 
		<link rel="shortcut icon" href="src/icon-WebTiled.png">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
		<meta name="distribution" content="Global">
		<meta name="description" content="Esse é um editor de mapas de RPG que funciona diretamente no browser para o Empire of Mana(MMORPG-P2W).">
		<meta name="keywords" content="Empire of Mana, EOM, Tiled, Map Editor, Free, P2W">
		<meta name="owner" content="empireofmana@tuatec.com.br">
		<meta rev="made" href="empireofmana@tuatec.com.br">
		<meta name="copyright" content="TUATECnologia.COM.BR COOP. LTDA">
		<meta name="author" content="Equipe NOOBWARE">
		<meta name="content-language" content="pt-br">
		<meta name="rating" content="General">
		<meta HTTP-EQUIV="generator" content="Quanta Plus, SSH, JavaScript, HTML5, DOM, MySQL" />
		<link href="libs/style.css" rel="stylesheet" type="text/css">
		<script src="libs/ClassControlSelect.js"></script>
		<script src="libs/ClassControlMouse.js"></script>
		<script>
			$Tilesets=new Array();
			layerObject = [];
			layerColision = [];
			layerTop = new Array();
			layerGround = new Array();
			gates = new Array();
			mobs = new Array();
			//alert(gates.length);
			<?=(isset($_GET['tester']) && $_GET['tester']=="Lunovox")?"":"document.oncontextmenu=function(){return false;};";?>
		</script>
	</head>
	<body leftmargin="0" marginheight="0" marginwidth="0">
		<table class="TableExtructure">
			<tr>
				<td colspan="2">
					<div class="PainelPrincipal" class_="Tools" >
						<button disabled>Novo</button>
						<button disabled>Abrir</button>
						<button disabled>Salvar</button>
						<button disabled>Exportar</button>
					</div>
				</td>
			</tr>
			<tr>
				<td width="200" valign="top">
					<?php require_once("forms/frmPropriedade.php"); ?>
				</td>
				<td valign="top">
					<?php require_once("forms/frmMontador.php"); ?>
				</td>
			</tr>
		</table>
	</body>
</html>