# Empire of Mana

RPG game **[UNCONTINUED]** in Javascript based on The Mana World to work in web browsers.

### Screenshots

![](https://gitlab.com/lunovox/empire-of-mana/raw/master/empireofmana/drafts/layouts/screen-login.png)

![](https://gitlab.com/lunovox/empire-of-mana/raw/master/empireofmana/drafts/layouts/screen-register-real.png)

![](https://gitlab.com/lunovox/empire-of-mana/raw/master/empireofmana/drafts/layouts/screen-register-real-2.png)

