=Criando um cabeçalho inicial=
Para criar um bom cabeçalho coloque tudo que um programador precisa saber para entender do que se trata o programa.<BR>
Como:<Br>
-Nome do programa.<Br>
-Descrição breve do propósito<BR>
-Site do programa<Br>
-Nome do autor<Br>
-E-mail do autor<Br>
-Nome e e-mail do responsável pela manutenção<Br>
-Descrição detalhada do funcionamento<br>
-Data de criação<br>
-Histórico de mudanças (data,nome,descrição)<br>
-Licença<br>

=Veja um exemplo:=
{{{
/*
	nomedoprograma - Busca um nome em uma lista

	Site:  http://meuprograma.com.br
	Autor:  João da silva <joão@email.com>
	Manutenção: Maria <maria@email.com>

	---------------------------------------------------
	
	Este programa rece um nome como parâmetro e procura
	em uma base de nomes, retornando mais informações--
	relacionadas com o nome.
	
	Exemplo:
		./nomedoprograma joão
			joão autor
		./nomedoprograma maria
			maria manutenção
	
	---------------------------------------------------
	
	Histórico:
		v1.0 1999-05-18, joão
			-Versão inicial
		v1.1 1999-08-02, joão
			-Corrigido bug com nomes acentuados
		v2.0 2000-04-28, Pedro
			-Corrigido 2.485 bugs (João não sabe programar!)

	---------------------------------------------------

      	Copyright 2011 
      
      	This program is free software; you can redistribute it and/or modify
      	it under the terms of the GNU General Public License as published by
      	the Free Software Foundation; either version 2 of the License, or
      	(at your option) any later version.
      
      	This program is distributed in the hope that it will be useful,
      	but WITHOUT ANY WARRANTY; without even the implied warranty of
      	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
      	GNU General Public License for more details.
      
      	You should have received a copy of the GNU General Public License
      	along with this program; if not, write to the Free Software
      	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
      	MA 02110-1301, USA.
		
		
*/
}}}
Ficou extenso? Sim. <Br>Ruim? Não.<BR>
Pode parecer feio. De fato, às vezes o cabeçalho pode ficar até maior que o programa. Mais isso não é um problema,
pois, no interpretador os comentários são ignorados, já para o programador, que diferença! E cuitado, este programador
pode ser você mesmo...