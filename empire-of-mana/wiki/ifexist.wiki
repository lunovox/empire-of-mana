<p align="center"><img src="https://lh6.googleusercontent.com/-c0djxT6DFSQ/Tn3kb5bV17I/AAAAAAAAAkQ/zIkMLkaITdA/s640/Captura_de_tela-2.png">

===IFEXIST==
Veja outros [protocolos Protocolos]


----

 * *Atualização do servidor:* ← {{{ifexist?users&3&30}}}

<p align="justify">Significa que software-servidor está dizendo ao software-cliente que atualmente existem *3 usuário logando*, e *30 usuário logados*. O software-cliente imprimirá esta informação na tela de login. É uma informação importante para o usuário, pois todo servidor tem limite de usuários logados para evitar [lag LAGs]. Quando um usuário não-vip morrer é deslogados, dando oportunidade de outro usuário reconectar. Obviamente usuários vips ao morrerem não deslogam, mas apenas voltam para o save-point.

Veja outros [protocolos Protocolos]

----

===COMANDOS OBSOLETOS===

O cliente pergunta ao servidor se o *email* foi registrado:
 * *Enviar pergunta:* → {{{ifexist?email&rui.gravata@hotmail.com}}}
 * Respostas possíveis:
  * *caso registrado:* ← {{{ifexist?email&rui.gravata@hotmail.com&1}}}
  * *caso não registrado:* ← {{{ifexist?email&rui.gravata@hotmail.com&0}}}

O cliente pergunta ao servidor se o *char* foi registrado:
 * *Enviar pergunta:* → {{{ifexist?char&Lunovox}}}
 * Respostas possíveis:
  * *caso registrado:* ← {{{ifexist?char&Lunovox&1}}}
  * *caso não registrado:* ← {{{ifexist?char&Lunovox&0}}}

O cliente pergunta ao servidor quantos chars estão *online*:
 * *Enviar pergunta:* → {{{ifexist?online}}}
 * Respostas possíveis:
  * *Caso ninguem online:* ← {{{ifexist?online&0}}}
  * *Caso 78 usuários online:* ← {{{ifexist?online&78}}}

*Observação:* O valor retornável são os usuário que estão identificado com senha. Os que estão conectado mas ainda se identificaram no login não são inclusos na contagem de usuários online.

----

Veja outros [protocolos Protocolos]