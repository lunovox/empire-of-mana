/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes.Control;

import Classes.Model.Protocolo;
import org.webbitserver.WebSocketConnection;
import org.webbitserver.WebSocketHandler;

/*
 *  Rui tente separa toda lógica da classe websocket, para que sempre que preci-
 * sarmos trocar de lib, seja mais fácil. Quanto mais MVC melhor :)
 */

public class WebSocket extends Protocolo implements WebSocketHandler{

    public void onOpen(WebSocketConnection ws) throws Exception {
        log(filtrarPorta()+" → entrou no servidor!");
        numberOfPlayersOnline++;
        setProtocolWebSocket(ws);
        sendMessage("ifexist?online&" +  numberOfPlayersOnline);
    }

    public void onClose(WebSocketConnection ws) throws Exception {
      numberOfPlayersOnline--;
      log(filtrarPorta() + " → saiu no servidor!");
      try {
	 broadCast("ifexist?online&"+numberOfPlayersOnline);
	 log("ALL ← ifexist?online&" + numberOfPlayersOnline);
      } catch (Exception e) {
	 log("Falha ao enviar mensagem para '" + filtrarPorta() + "'!");
      }
    }

    public void onMessage(WebSocketConnection ws, String msg) throws Throwable {
        treatProtocolo(msg);
    }

    public void onMessage(WebSocketConnection ws, byte[] bytes) throws Throwable {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void onPong(WebSocketConnection ws, String string) throws Throwable {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
}
