package Classes;

import java.util.ArrayList;

public class bdPlayers {

	private ArrayList<Player> Players = new ArrayList<Player>();
	private ArrayList<String> Nick = new ArrayList<String>();
	private ArrayList<String> WebSocket = new ArrayList<String>();

	public void AddPlayer(String $WebSocket, String $Nick, Integer $Sex, String $WebIPs, Integer $PowerGM, Integer $Race, Integer $Classe, String $PositionNow,
	   String $PositionSave, Integer $Level, Integer $Experience, Integer $HPnow, Integer $HPmax, Integer $MPnow, Integer $MPmax, Integer $AvailPoints,
	   Integer $Strength, Integer $Agility, Integer $Vitality, Integer $Intelligence, Integer $Dexterity, Integer $Lucky,
	   String $Inventory, String $Equipped
	){
		Players.add(new Player($WebSocket, $Nick, $Sex, $WebIPs, $PowerGM, $Race, $Classe, $PositionNow,
			$PositionSave, $Level, $Experience, $HPnow, $HPmax, $MPnow, $MPmax, $AvailPoints,
			$Strength, $Agility, $Vitality, $Intelligence, $Dexterity, $Lucky,
			$Inventory, $Equipped)
		);
		Nick.add($Nick);
		WebSocket.add($WebSocket);
	}
	public Player getPlayerByNick(String $Nick) {
		int $l = Nick.indexOf($Nick);
		if ($l >= 0) {
			return Players.get($l);
		}
		return null;
	}
	public Player getPlayerByWebSocket(String $WebSocket) {
		int $l = WebSocket.indexOf($WebSocket);
		if ($l >= 0) {
			return Players.get($l);
		}
		return null;
	}
	public Player getPlayer(int Index) {
		return Players.get(Index);
	}
	public Boolean delPlayerByNick(String $Nick) {
		int $l = Nick.indexOf($Nick);
		if ($l >= 0) {
			Players.remove($l);
			return true;
		}
		return false;
	}
	public Boolean delPlayerByWebSocket(String $WebSocket) {
		int $l = WebSocket.indexOf($WebSocket);
		if ($l >= 0) {
			Players.remove($l);
			return true;
		}
		return false;
	}
	public Boolean delPlayer(int Index) {
		if (Index >= 0 && Players.size() > Index) {
			Players.remove(Index);
			return true;
		}
		return false;
	}
	public int getTamanho() {
		return Players.size();
	}

	public class Player {
		public Player(String $WebSocket, String $Nick, Integer $Sex, String $WebIPs, Integer $PowerGM, Integer $Race, Integer $Classe, String $PositionNow,
			String $PositionSave, Integer $Level, Integer $Experience, Integer $HPnow, Integer $HPmax, Integer $MPnow, Integer $MPmax, Integer $AvailPoints,
			Integer $Strength, Integer $Agility, Integer $Vitality, Integer $Intelligence, Integer $Dexterity, Integer $Lucky,
			String $Inventory, String $Equipped
		) {
			webSocket = $WebSocket;
			nick = $Nick;
			sex = $Sex;
			webIPs = $WebIPs;
			powerGM = $PowerGM;
			race = $Race;
			classe = $Classe;
			positionNow = $PositionNow;
			positionSave = $PositionSave;
			level = $Level;
			experience = $Experience;
			hpNow = $HPnow;
			hpMax = $HPmax;
			mpNow = $MPnow;
			mpMax = $MPmax;
			availPoints = $AvailPoints;
			strength = $Strength;
			agility = $Agility;
			vitality = $Vitality;
			intelligence = $Intelligence;
			dexterity = $Dexterity;
			lucky = $Lucky;
			inventory = $Inventory;
			equipped = $Equipped;
		}
		String webSocket;
		String nick;
		Integer sex;
		String webIPs;
		Integer powerGM;
		Integer race;
		Integer classe;
		String positionNow;
		String positionSave;
		Integer level;
		Integer experience;
		Integer hpNow;
		Integer hpMax;
		Integer mpNow;
		Integer mpMax;
		Integer availPoints;
		Integer strength;
		Integer agility;
		Integer vitality;
		Integer intelligence;
		Integer dexterity;
		Integer lucky;
		String inventory;
		String equipped;

		public String getWebSocket() {
			return webSocket;
		}
		public String getNick() {
			return nick;
		}
		public Integer getSex() {
			return sex;
		}
		public String getWebIPs() {
			return webIPs;
		}
		public Integer getPowerGM() {
			return powerGM;
		}
		public Integer getRace() {
			return race;
		}
		public Integer getClasse() {
			return classe;
		}
		public String getPositionNow() {
			return positionNow;
		}
		public String getPositionSave() {
			return positionSave;
		}
		public Integer getLevel() {
			return level;
		}
		public Integer getExperience() {
			return experience;
		}
		public Integer getHpNow() {
			return hpNow;
		}
		public Integer getHpMax() {
			return hpMax;
		}
		public Integer getMpNow() {
			return mpNow;
		}
		public Integer getMpMax() {
			return mpMax;
		}
		public Integer getAvailPoints() {
			return availPoints;
		}
		public Integer getStrength() {
			return strength;
		}
		public Integer getAgility() {
			return agility;
		}
		public Integer getVitality() {
			return vitality;
		}
		public Integer getIntelligence() {
			return intelligence;
		}
		public Integer getDexterity() {
			return dexterity;
		}
		public Integer getLucky() {
			return lucky;
		}
		public String getInventory() {
			return inventory;
		}
		public String getEquipped() {
			return equipped;
		}

		public void setSex(int $Sex) {
			sex = $Sex;
		}
		public void setWebIPs(String $WebIPs) {
			webIPs = $WebIPs;
		}
		public void setPowerGM(int $PowerGM) {
			powerGM = $PowerGM;
		}
		public void setRace(int $Race) {
			race = $Race;
		}
		public void setClasse(int $Classe) {
			classe = $Classe;
		}
		public void setPositionNow(String $PositionNow) {
			positionNow = $PositionNow;
		}
		public void setPositionSave(String $PositionSave) {
			positionSave = $PositionSave;
		}
		public void setLevel(int $Level) {
			level = $Level;
		}
		public void setExperience(int $Experience) {
			experience = $Experience;
		}
		public void setHpNow(int $HpNow) {
			hpNow = $HpNow;
		}
		public void setHpMax(int $HpMax) {
			hpMax = $HpMax;
		}
		public void setMpNow(int $MpNow) {
			mpNow = $MpNow;
		}
		public void setMpMax(int $MpMax) {
			mpMax = $MpMax;
		}
		public void setAvailPoints(int $AvailPoints) {
			availPoints = $AvailPoints;
		}
		public void setStrength(int $Strength) {
			strength = $Strength;
		}
		public void setAgility(int $Agility) {
			agility = $Agility;
		}
		public void setVitality(int $Vitality) {
			vitality = $Vitality;
		}
		public void setIntelligence(int $Intelligence) {
			intelligence = $Intelligence;
		}
		public void setDexterity(int $Dexterity) {
			dexterity = $Dexterity;
		}
		public void setLucky(int $Lucky) {
			lucky = $Lucky;
		}
		public void setInventory(String $Inventory) {
			inventory = $Inventory;
		}
		public void setEquipped(String $Equipped) {
			equipped = $Equipped;
		}
	}
}
