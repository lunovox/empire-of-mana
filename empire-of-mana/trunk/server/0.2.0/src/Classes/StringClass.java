
package Classes;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class StringClass {
    public StringClass(String Testo) {
        testoEmTratamento = Testo;
    }
    public StringClass() {
        testoEmTratamento = "";
    }
    String testoEmTratamento="";

    public void setTesto(String Testo) {
        testoEmTratamento=Testo;
    }
    public String getTesto() {
        return testoEmTratamento;
    }
    public int getContLinhas() {
        String Partes[] = testoEmTratamento.split("\n");
        return Partes.length;
    }
    public String getLinha(int Linha) {
        String Partes[] = testoEmTratamento.split("\n");
        return Partes[Linha];
    }
    public static boolean isSingler(String $Testo, boolean isNumeric, boolean isLowerCases, boolean ifUpperCase){
	return isSingler($Testo, isNumeric, isLowerCases, ifUpperCase, $Testo.length());
    }
    public static boolean isSingler(String $Testo, boolean isNumeric, boolean isLowerCases, boolean ifUpperCase, int MinLength){
	if($Testo.length()>=MinLength){
	    int ContValityChars=0;
	    for (int $i=0; $i<$Testo.length(); $i++){
		if(
		    ($Testo.charAt($i)>=97 && $Testo.charAt($i)<=122 && isLowerCases) ||
		    ($Testo.charAt($i)>=65 && $Testo.charAt($i)<=90 && ifUpperCase) ||
		    ($Testo.charAt($i)>=48 && $Testo.charAt($i)<=57 && isNumeric)
		)ContValityChars++;
	    }
	    if($Testo.length()==ContValityChars) return true;
	}
	return false;
    }
    public void setKeyCode(String codigoDeCaracteres){
        if(codigoDeCaracteres.equals("UTF-8")){//Transforma ISO-8859-1 ? UTF-8
            testoEmTratamento=ISO88591toUTF8(testoEmTratamento);
        }else if(codigoDeCaracteres.equals("ISO-8859-1")){//Transforma UTF-8 ? ISO-8859-1
            testoEmTratamento=UTF8toISO88591(testoEmTratamento);
        }
    }
    public static String UTF8toISO88591(String UTF8){ // S� funciona se for executado fora do netbeans!
        try{
            return URLDecoder.decode(UTF8, "ISO-8859-1");
        } catch(UnsupportedEncodingException E){
            return UTF8;
        }
    }
    public static String ISO88591toUTF8(String ISO88591){ // S� funciona se for executado fora do netbeans!
        try{
            return new String(ISO88591.getBytes("UTF-8"), "ISO-8859-1");
        } catch(UnsupportedEncodingException E){
            return ISO88591;
        }
    }
    public static String getEscape(String $Testo) {
	String[] hex = {
	    "%00", "%01", "%02", "%03", "%04", "%05", "%06", "%07",
	    "%08", "%09", "%0a", "%0b", "%0c", "%0d", "%0e", "%0f",
	    "%10", "%11", "%12", "%13", "%14", "%15", "%16", "%17",
	    "%18", "%19", "%1a", "%1b", "%1c", "%1d", "%1e", "%1f",
	    "%20", "%21", "%22", "%23", "%24", "%25", "%26", "%27",
	    "%28", "%29", "%2a", "%2b", "%2c", "%2d", "%2e", "%2f",
	    "%30", "%31", "%32", "%33", "%34", "%35", "%36", "%37",
	    "%38", "%39", "%3a", "%3b", "%3c", "%3d", "%3e", "%3f",
	    "%40", "%41", "%42", "%43", "%44", "%45", "%46", "%47",
	    "%48", "%49", "%4a", "%4b", "%4c", "%4d", "%4e", "%4f",
	    "%50", "%51", "%52", "%53", "%54", "%55", "%56", "%57",
	    "%58", "%59", "%5a", "%5b", "%5c", "%5d", "%5e", "%5f",
	    "%60", "%61", "%62", "%63", "%64", "%65", "%66", "%67",
	    "%68", "%69", "%6a", "%6b", "%6c", "%6d", "%6e", "%6f",
	    "%70", "%71", "%72", "%73", "%74", "%75", "%76", "%77",
	    "%78", "%79", "%7a", "%7b", "%7c", "%7d", "%7e", "%7f",
	    "%80", "%81", "%82", "%83", "%84", "%85", "%86", "%87",
	    "%88", "%89", "%8a", "%8b", "%8c", "%8d", "%8e", "%8f",
	    "%90", "%91", "%92", "%93", "%94", "%95", "%96", "%97",
	    "%98", "%99", "%9a", "%9b", "%9c", "%9d", "%9e", "%9f",
	    "%a0", "%a1", "%a2", "%a3", "%a4", "%a5", "%a6", "%a7",
	    "%a8", "%a9", "%aa", "%ab", "%ac", "%ad", "%ae", "%af",
	    "%b0", "%b1", "%b2", "%b3", "%b4", "%b5", "%b6", "%b7",
	    "%b8", "%b9", "%ba", "%bb", "%bc", "%bd", "%be", "%bf",
	    "%c0", "%c1", "%c2", "%c3", "%c4", "%c5", "%c6", "%c7",
	    "%c8", "%c9", "%ca", "%cb", "%cc", "%cd", "%ce", "%cf",
	    "%d0", "%d1", "%d2", "%d3", "%d4", "%d5", "%d6", "%d7",
	    "%d8", "%d9", "%da", "%db", "%dc", "%dd", "%de", "%df",
	    "%e0", "%e1", "%e2", "%e3", "%e4", "%e5", "%e6", "%e7",
	    "%e8", "%e9", "%ea", "%eb", "%ec", "%ed", "%ee", "%ef",
	    "%f0", "%f1", "%f2", "%f3", "%f4", "%f5", "%f6", "%f7",
	    "%f8", "%f9", "%fa", "%fb", "%fc", "%fd", "%fe", "%ff"
	};
	StringBuilder sbuf = new StringBuilder();
	int len = $Testo.length();
	for (int i = 0; i < len; i++) {
	    int ch = $Testo.charAt(i);
	    if ('A' <= ch && ch <= 'Z') {		// 'A'..'Z'
		sbuf.append((char) ch);
	    } else if ('a' <= ch && ch <= 'z') {	// 'a'..'z'
		sbuf.append((char) ch);
	    } else if ('0' <= ch && ch <= '9') {	// '0'..'9'
		sbuf.append((char) ch);
	    } else if (ch == '-' || ch == '_' // unreserved
		    || ch == '.' || ch == '!'
		    || ch == '~' || ch == '*'
		    || ch == '\'' || ch == '('
		    || ch == ')') {
		sbuf.append((char) ch);
	    } else {					// 0x7FF < ch <= 0xFFFF
		sbuf.append(hex[(char) ch]);
	    }
	}
	//return sbuf.toString().replace("+", "%20");
	return sbuf.toString();
    }
    public static String getUnescape(String $Testo) {
	return UTF8toISO88591($Testo);
    }
    public static String md5(String $Senha){
        String sen = "";
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        BigInteger hash = new BigInteger(1, md.digest($Senha.getBytes()));
        sen = hash.toString(16);
        return sen;
    }
    public static String getAgora(){
	return getAgora("yyyy-MM-dd HH:mm:ss");
    }
    public static String getAgora(String $Formato){
	return (new SimpleDateFormat($Formato)).format(new java.util.Date());
    }
    public static Timestamp getAgoraTimestamp(){
	return new Timestamp(System.currentTimeMillis());
    }
    public static String implode(String[] $Array, String $Divisor) {
	String out = "";
	for(int i=0; i<$Array.length; i++) {
	    if(i!=0) { out += $Divisor; }
	    out += $Array[i];
	}
	return out;
    }
    public void setSubstituicao(String De, String Por){
        testoEmTratamento=getSubstituicao(testoEmTratamento, De, Por);
    }
    public String getSubstituicao(String De, String Por){
        return getSubstituicao(testoEmTratamento, De, Por);
    }
    public String getSubstituicao(String Entrada, String De, String Por){
        int Loc1=-1;
        do{
            Loc1=-1;
            Loc1=Entrada.indexOf(De);
            if(Loc1>=0){
                String Parte1="", Parte2="";
                Parte1=Entrada.substring(0, Loc1);
                Parte2=Entrada.substring(Loc1+De.length(), Entrada.length());
                Entrada = Parte1+Por+Parte2;
            }
        }while(Loc1>=0);
        return Entrada;/**/
    }
    public String extrairEntre(String Parte1, String Parte2){
        return extrairEntre(testoEmTratamento, Parte1, Parte2);
    }
    public String extrairEntre(String Parte1, String Parte2, int Apartir){
        return extrairEntre(testoEmTratamento, Parte1, Parte2, Apartir);
    }
    public String extrairEntre(String Entrada, String Parte1, String Parte2){
        return extrairEntre(Entrada, Parte1, Parte2, 0);
    }
    public String extrairEntre(String Entrada, String Parte1, String Parte2, int Apartir){
        int Loc1=-1, Loc2=-1;
        Loc1=Entrada.indexOf(Parte1,Apartir);
        if(Loc1>=0){
            Loc2=Entrada.indexOf(Parte2,Loc1+Parte1.length());
            if(Loc2>=0 && Loc2>=Loc1){
                return Entrada.substring(Loc1+Parte1.length(), Loc2);
            }else{
                return "";
            }
        }else{
            return "";
        }

    }
}
