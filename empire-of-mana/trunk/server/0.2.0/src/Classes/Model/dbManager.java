/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes.Model;

import Classes.Model.Logs;
import Formularios.mySQL;
import java.sql.SQLException;

/**
 *
 * @author vanderson
 */
public class dbManager extends Logs{

    int contPlayersOnline = 0;
    boolean seprintTesto = true;
    boolean modePing = false;
    String MySqlServer = "localhost";
    String MySqlUser = "root";
    String MySqlPass = "";
    String MySqlDataBase = "eomserver";
    String MySqlTableTeste = "users";
    mySQL mySQL = null;

    public dbManager(int port, String server, String user, String password, String database, String tabletest) {
        MySqlServer = server;
        MySqlUser = user;
        MySqlPass = password;
        MySqlDataBase = database;
        MySqlTableTeste = tabletest;
        try {
            mySQL = new mySQL(server, user, password, database);
        } catch (ClassNotFoundException ex) {

            log("EOM-ERRO: " + ex.getMessage());
        } catch (SQLException ex) {

            log("SQLException: " + ex.getMessage());
            log("SQLState: " + ex.getSQLState());
            log("VendorError: " + ex.getErrorCode());
        } catch (Exception ex) {
            log("Problemas ao tentar conectar com o banco de dados: " + ex);
        }
        msgInicial(port, server, database);
    }
    
}
