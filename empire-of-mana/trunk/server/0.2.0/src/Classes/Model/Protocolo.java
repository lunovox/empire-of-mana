/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes.Model;

import Classes.Model.Logs;
import Formularios.EncodeURL;
import com.sun.xml.internal.ws.addressing.W3CAddressingConstants;
import org.webbitserver.WebSocketConnection;

/**
 *
 * @author vanderson
 */
public class Protocolo extends Logs {
    public int numberOfPlayersOnline=0;
    
    private WebSocketConnection ProtocolWebSocket;

    public WebSocketConnection getProtocolWebSocket() {
        return ProtocolWebSocket;
    }

    public void setProtocolWebSocket(WebSocketConnection ProtocolWebSocket) {
        this.ProtocolWebSocket = ProtocolWebSocket;
    }
    
    private boolean pingEnable = false;

    public boolean isPingEnable() {
        return pingEnable;
    }

    public void setPingEnable(boolean pingEnable) {
        this.pingEnable = pingEnable;
    }
     
     public void sendMessage(String msg) {
      try {
	 if (ProtocolWebSocket != null) {
	    if (isLogEnable()) {
	       log(filtrarPorta() + " ← " + msg);
	    }
	    ProtocolWebSocket.send(msg);
	 } else {
	    if (isLogEnable()) {
	       log("ALL ← " + msg);
	    }
	    broadCast(msg);
	 }
      } catch (Exception ex) {
	 log("SERVER → Falha ao enviar o protocolo para '" + filtrarPorta() + "'!");
	 log("SERVER → protocolo: '" + msg + "'");
      }
   }
   public void broadCast(String msg){
       
   }
   public String filtrarPorta() {
      return "Jogador id";
   }
   
   public void treatProtocolo(String msg){
      String comando = null;
      String atributos[] = null;
      try {
	 if (!isLogEnable()) {
	    comando = msg.split("\\?")[0];
	    atributos = msg.split("\\?")[1].split("&");
	    log(filtrarPorta() + " → " + msg);
	    if (comando.startsWith("register")) {
	       sendMessage("register?0&" + EncodeURL.escape("Erro ao registrar o usuário no banco de dados!"));
	    } else if (comando.startsWith("login")) {
	       sendMessage("login?0&" + EncodeURL.escape("Erro ao acessar o usuário no banco de dados!"));
	    } else if (comando.startsWith("modePing")) {
	       if (atributos[0].equals("on")) {
		  setPingEnable(true);
		  sendMessage("modePing?on");
	       }
	    } else if (msg.equals("ifexist?online")) {
	       sendMessage("ifexist?online&" + numberOfPlayersOnline);
	    } else if (msg.equals("ifexist?logged")) {
	       sendMessage("ifexist?logged&0");
	    }
	 } else if (msg.equals("modePing?off")) {
	    setPingEnable(false);
	    log(filtrarPorta() + " → modePing?off");
	    log("ALL ← modePing?off");
	    broadCast("modePing?off!");
	 } else {
	    log(filtrarPorta() + " → " + msg);
	    log("ALL ← " + msg);
	    broadCast(msg);
	 }
      } catch (Exception ex) {
	 try {
	    sendMessage("Por favor siga o protocolo de comunicação!");
	 } catch (Exception ex1) {
	    log(ex1.toString());
	 }
      }
   }
}
