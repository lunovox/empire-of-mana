/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes.Model;

import java.text.SimpleDateFormat;

/**
 *
 * @author vanderson
 */
public class Logs {
    private  boolean logEnable = true;

    public boolean isLogEnable() {
        return logEnable;
    }

    public void setLogEnable(boolean logEnable) {
        this.logEnable = logEnable;
    }


    public void msgInicial(int port){
      java.util.Date Agora = new java.util.Date();
      SimpleDateFormat Formatador = new SimpleDateFormat("dd/MM/yyyy h:mm a");
      log("");
      log("#############################################################");
      log("###   SERVIDOR EMPIRE OF MANA (by Lunovox & Vanderson)    ###");
      log("#############################################################");
      log("###   Porta do WebSocket: "+port);
      log("###   Conexão MySQL.....: off");
      log("###   Ativação..........: "+Formatador.format(Agora));
      log("#############################################################");
      log("###   PARÂMENTROS → [-h][-p][-s][-u][-w][-d][-t]          ###");
      log("#############################################################");
      log("");

   }
   public void msgInicial(int port, String server, String database){
      java.util.Date Agora = new java.util.Date();
      SimpleDateFormat Formatador = new SimpleDateFormat("dd/MM/yyyy h:mm a");

      log("");
      log("#############################################################");
      log("###   SERVIDOR EMPIRE OF MANA (by Lunovox & Vanderson)    ###");
      log("#############################################################");
      log("###   Porta do WebSocket: " + port);
      log("###   Servidor MySQL....: " + server);
      log("###   Banco de Dados....: " + database);
      log("###   Ativação..........: " + Formatador.format(Agora));
      log("#############################################################");
      log("###   PARÂMENTROS → [-h][-p][-s][-u][-w][-d][-t]          ###");
      log("#############################################################");
      log("");
      //throw new UnsupportedOperationException("Not yet implemented");
      //printTesto("Servidor ativado!");
   }
   public void log(String log) {
      if (logEnable) {
	 System.out.println(log);
      }
   }
}
