/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Geodata.AI;

import Geodata.Beans.Map;
import Geodata.PathFinder;

/**
 *
 * @author vanderson
 */
public class vision extends PathFinder {

    public final static char baixo = 'D';
    public final static char cima = 'U';
    public final static char esquerda = 'L';
    public final static char direita = 'R';
    Map map;
    int distance;
    int x, y;
    char lookingFor; // L = esquerda, R = direita, U = para cima, D = para baixo 

    public vision(int distance, Map map) {
        this.distance = distance;
        this.map = map;
    }

    public void setLokingFor(char lookingFor) {
        this.lookingFor = lookingFor;
    }

    public boolean existAnyoneIn(int x, int y) {
        return !map.mapThings[x][y].players.isEmpty();
    }
    
    public int distance(int x0,int y0,int x1,int y1){
        return (int)Math.sqrt(Math.pow((x0-x1),2)+Math.pow((y0-y1),2));
    }
    
    public boolean canSee(int x0, int y0, int x1, int y1) {
        if(distance(x0,y0,x1,y1)>distance){
            return false;
        }
        
        int dx = Math.abs(x1 - x0);
        int dy = Math.abs(y1 - y0);
        int sx, sy;

        if (x0 < x1) {
            sx = 1;
        } else {
            sx = -1;
        }
        if (y0 < y1) {
            sy = 1;
        } else {
            sy = -1;
        }

        int err = dx - dy;
        
        int position[]= new int[2];
        
        while (true) {
            position[0]=x0;
            position[1]=y0;
            
            if(!isPassable(position, map.mapColisions)){
                return false;
            }
            
            if (x0==x1) {
                if(y0==y1){
                    return true;
                }
            }

            int e2 = 2 * err;
            if (e2 > -dy) {
                err = err - dy;
                x0 = x0 + sx;
            }
            if (e2 < dx) {
                err = err + dx;
                y0 = y0 + sy;
            }

        }
    }
    

}
