/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Geodata.AI;

import Geodata.Beans.Player;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author vanderson
 */
public class MonsterAI extends vision implements Runnable {
    //////// Types /////////////////

    public static final int typeAggressive = 10;
    public static final int typeNormal = 5;
    public static final int typeCoward = 1;
    //////////////////////////////////
    //////// states ///////////////
    private static final int patrol = 0;
    private static final int fleeing = 1;
    private static final int chasing = 2;
    ////////////////////////////////
    private Player enemy;
    private Player monster;
    private int state;
    private int fear = 0;
    private int fury = 0;
    private int type;
    private int speed;
 
    public MonsterAI(Player monster, int type, int speed) {
        this.monster = monster;
        this.type = type;
        this.speed = speed;
    }

    public boolean isSeeingSomeone() {
        return false;
    }

    public void randomWalk() {
        int[] origin={monster.getX(),monster.getY()};
        int[] destination={-1,-1};
        int map[][]=monster.getMap().mapColisions;
                
        while((!isOnMap(destination,map[0].length,map.length) || !isPassable(destination, map))){
                    
 
           
           destination[0]=origin[0]+(int)(Math.random()*speed)*(int)Math.pow((-1),(int)((Math.random()+1)*2));
           destination[1]=origin[1]+(int)(Math.random()*speed)*(int)Math.pow((-1),(int)((Math.random()+1)*2));
        
        }
   
    
         String path = getPath(origin, destination, map);
         walkOnPath(path);
    }
    
    public void walkOnPath(String way){
        String[] path = way.split("@");
        int position[];
        
        for(int i=1;i<path.length;i++){
            position = dotStringToDotInt(path[i].replaceAll(" ", ""));
            monster.moveMonster(position[0], position[1]);
            waitFor(5000/speed);
        }
        
    }
            
    public void waitFor(int x) {
        synchronized (this) {
            try {
                wait(x);
            } catch (InterruptedException ex) {
                Logger.getLogger(MonsterAI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void aggressiveBehavior() {
        state = patrol;
        while (true) {
            if (state == patrol) {
                if (isSeeingSomeone()) {
                    state = chasing;
                } else {
                    randomWalk();
                    waitFor(12000);
                }
            } else if (state == chasing) {
            } else if (state == fleeing) {
            }
        }
    }

    public void cowardBehavior() {
        state = patrol;
        while (true) {
        }
    }

    public void normalBehavior() {
        state = patrol;
        while (true) {
        }
    }

    public void run() {
        if (type == typeAggressive) {
            aggressiveBehavior();
        } else if (type == typeCoward) {
            cowardBehavior();
        } else if (type == typeNormal) {
            normalBehavior();
        }
    }
}
