/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Geodata;

import Geodata.AI.MonsterAI;
import Geodata.Beans.Map;
import Geodata.Beans.Player;
import java.util.ArrayList;


/**
 *
 * @author vanderson
 */
public class GeodataMain implements Runnable{
    Player[] monster = new Player[1000];
    
    public static void main(String[] args){
        new Thread(new GeodataMain()).start();
    }

    
    public void run() {
        
        int $mapa[][] = { //Mapa PadrÃ£o: 15 linhas + 20 colunas = 640x480px
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
            {1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1},
            {1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1},
            {1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1},
            {1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1},
            {1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1},
            {1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1},
            {1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1},
            {1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1},
            {1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1},
            {1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1},
            {1, 1, 0, 1, 0, 1, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1},
            {1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1},
            {1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1},
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}
        };
                       
        ArrayList<Map> maps = new ArrayList<Map>();
        
        for(int i=1;i<1000;i++){
            Map m =new Map();
            m.mapColisions=$mapa;
            monster[i]=new Player("Monstro"+i, 1, 10, 5, 8, 6, 7);
            
            monster[i].setMap(m);
            monster[i].moveMonster(1, 1);
            m.setId(i);
            maps.add(m);
            
            Thread t = new Thread(new MonsterAI(monster[i], MonsterAI.typeAggressive, 5));
            
            t.setPriority(Thread.MIN_PRIORITY);
            t.start();
        }
        
           
        }
        
   // }
}
