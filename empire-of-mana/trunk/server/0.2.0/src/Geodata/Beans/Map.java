
package Geodata.Beans;


public class Map {
    private int id;
    public int mapColisions[][] = new int[15][20];
    public Square mapThings[][] = new Square[15][20];
    
    public Map(){
        for(int i=0;i<15;i++){
            for(int j=0;j<20;j++){
               mapThings[i][j]=new Square(); 
            }
        }
        
    }
    
    public void moveMonster(Player monster,int toX,int toY){
        mapThings[monster.x][monster.y].monsters.remove(monster);
        mapThings[toX][toY].monsters.add(monster);
        monster.setX(toX);
        monster.setY(toY);
    }
    public void movePlayer(Player player,int toX,int toY){
        mapThings[player.x][player.y].players.remove(player);
        mapThings[toX][toY].players.add(player);
        player.setX(toX);
        player.setY(toY);
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int[][] getMapColisions() {
        return mapColisions;
    }

    public void setMapColisions(int[][] mapColisions) {
        this.mapColisions = mapColisions;
    }

    public Square[][] getMapThings() {
        return mapThings;
    }

    public void setMapThings(Square[][] mapThings) {
        this.mapThings = mapThings;
    }
    
}
