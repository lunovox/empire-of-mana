/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Geodata.Beans;

/**
 *
 * @author vanderson
 */
public class Player {
    private Map map;
    
    boolean isAlive;
    private String name;
    private int level;
    private int hp;
    private int mp;
    
    private int str;
    private int dex;
    private int car;
    private int vit;
    private int wis;
    
    private int atkWeapons;
    private int defArmors;
    
    int x,y;
         
    char lookingFor; // L = esquerda, R = direita, U = para cima, D = para baixo
    
    public Player(String name,int level,int str,int dex,int car,int vit,int wis){
        isAlive=true;
        this.name=name;
        this.level=level;
        this.str=str;
        this.dex=dex;
        this.car=car;
        this.vit=vit;
        this.wis=wis;
        setHp();
        setMp();
        this.map=map;
    }
    
    public void setHp(){
        hp=((vit/2)+(str/4))+(2*level)+10;
    }
    
    public void setMp(){
        mp=((vit/4)+(wis/2))+(2*level)+10;
    }
    
    public void updateHp(int value){
        hp=hp+value;
        
        if(hp<0){
            isAlive=false;
        }
    }
    
    public void updateMp(int value){
        mp=mp+value;
    }
    
    public void levelUp(){
        level=level+1;
        setMp();
        setHp();
    }
    
    public void moveMonster(int x,int y){
         map.mapThings[this.x][this.y].monsters.remove(this);
         map.mapThings[x][y].monsters.add(this);
         this.x=x;
         this.y=y;
    }
    
    public void movePlayer(int x,int y){
         map.mapThings[this.x][this.y].players.remove(this);
         map.mapThings[x][y].players.add(this);
         this.x=x;
         this.y=y;
    }
    
    public int hit(){
        return (str+level)/4 + (int)(Math.random()*str);
    }
    
    public int damage(){
        return (int)(Math.random()*atkWeapons) + (str+level)/4;
    }
    
    public int getAtkWeapons() {
        return atkWeapons;
    }

    public void setAtkWeapons(int atkWeapons) {
        this.atkWeapons = atkWeapons;
    }

    public int getDefArmors() {
        return defArmors;
    }

    public void setDefArmors(int defArmors) {
        this.defArmors = defArmors;
    }
    
    public void setPosition(int x,int y){
        this.x=x;
        this.y=y;
    }
    
    public int getCar() {
        return car;
    }

    public void setCar(int car) {
        this.car = car;
    }

    public int getDex() {
        return dex;
    }

    public void setDex(int dex) {
        this.dex = dex;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getMp() {
        return mp;
    }

    public void setMp(int mp) {
        this.mp = mp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStr() {
        return str;
    }

    public void setStr(int str) {
        this.str = str;
    }

    public int getVit() {
        return vit;
    }

    public void setVit(int vit) {
        this.vit = vit;
    }

    public int getWis() {
        return wis;
    }

    public void setWis(int wis) {
        this.wis = wis;
    }

    public boolean isIsAlive() {
        return isAlive;
    }

    public void setIsAlive(boolean isAlive) {
        this.isAlive = isAlive;
    }

    public char getLookingFor() {
        return lookingFor;
    }

    public void setLookingFor(char lookingFor) {
        this.lookingFor = lookingFor;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Map getMap() {
        return map;
    }

    public void setMap(Map map) {
        this.map = map;
    }
    
    
}
