/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import Classes.Control.WebSocket;
import Classes.Model.dbManager;
import java.io.IOException;
import org.webbitserver.WebServer;
import org.webbitserver.WebServers;
import org.webbitserver.handler.StaticFileHandler;

/**
 *
 * @author vanderson
 */
public class Main {

    public static void main(String[] args) {
        boolean modeGrafic = false;
        int port = 8888;
        String server = "localhost";
        String user = "root";
        String password = "";
        String database = "eomserver";
        String tabletest = "eomserver";
        for (int a = 0; a < args.length; a++) {
            if ((args[a].toLowerCase().equals("-p") || args[a].toLowerCase().equals("--port")) && !args[a + 1].isEmpty() && Integer.parseInt(args[a + 1]) > 0) {
                try {
                    port = Integer.parseInt(args[a + 1].toString());
                } catch (Exception ex) { /**/ }
            } else if ((args[a].toLowerCase().equals("-s") || args[a].toLowerCase().equals("--server")) && !args[a + 1].isEmpty() && !args[a + 1].equals("")) {
                server = args[a + 1].toString();
            } else if ((args[a].toLowerCase().equals("-u") || args[a].toLowerCase().equals("--user")) && !args[a + 1].isEmpty() && !args[a + 1].equals("")) {
                user = args[a + 1].toString();
            } else if ((args[a].toLowerCase().equals("-w") || args[a].toLowerCase().equals("--pass") || args[a].toLowerCase().equals("--password")) && !args[a + 1].isEmpty() && !args[a + 1].equals("")) {
                password = args[a + 1].toString();
            } else if ((args[a].toLowerCase().equals("-d") || args[a].toLowerCase().equals("--db") || args[a].toLowerCase().equals("--database")) && !args[a + 1].isEmpty() && !args[a + 1].equals("")) {
                database = args[a + 1].toString();
            } else if ((args[a].toLowerCase().equals("-t") || args[a].toLowerCase().equals("--tabletest")) && !args[a + 1].isEmpty() && !args[a + 1].equals("")) {
                tabletest = args[a + 1].toString();
            } else if (args[a].toLowerCase().equals("-h") || args[a].toLowerCase().equals("--help")) {
                System.out.println("");
                System.out.println("####################################################################################################");
                System.out.println("###   SERVIDOR EMPIRE OF MANA                                                                    ###");
                System.out.println("####################################################################################################");
                System.out.println("##### Created by ###################################################################################");
                System.out.println("###     * Lunovox → <rui.gravata@hotmail.com> / <rui.gravata@hotmail.com>                        ###");
                System.out.println("###     * Vanderson → <vandersonmr2@gmail.com>                                                   ###");
                System.out.println("####################################################################################################");
                System.out.println("##### COMANDO ######################################################################################");
                System.out.println("###      → java -jar <endereço>/EmpireOfMana.jar <parâmetros> &                                  ###");
                System.out.println("###                                                                                              ###");
                System.out.println("###   Observação: O simbolo '&' acima servirá para que o terminal continue esperando comandos    ###");
                System.out.println("###               mesmo após executar este servidor.                                             ###");
                System.out.println("####################################################################################################");
                System.out.println("##### PARÂMETROS ###################################################################################");
                System.out.println("###      [--help|-h]                       → Exibe ajudar sobre este pregrama.                   ###");
                System.out.println("###      [--port|-p] <nº>                  → Escolhe qual será a porta ocupada pelo servidor.    ###");
                System.out.println("###      [--server|-s] <url>               → Informa ao servidor o endereço onde está armazenado ###");
                System.out.println("###                                          o banco de dados mySQL.                             ###");
                System.out.println("###      [--user|-u] <UserName>            → Informa ao servidor o nome do usuário que acessará  ###");
                System.out.println("###                                          o banco de dados mySQL.                             ###");
                System.out.println("###      [--password|--pass|-w] <Senha>    → Informa ao servidor a senha do usuário que acessará ###");
                System.out.println("###                                          o banco de dados mySQL.                             ###");
                System.out.println("###      [--database|--dbase|-d] <Banco>   → Informa ao servidor o nome do banco de dados mySQL  ###");
                System.out.println("###                                          que contém as tabelas do EOM.                       ###");
                System.out.println("###      [--tableteste|-t] <Tabela>        → Informa ao servidor o nome da tabela que será       ###");
                System.out.println("###                                          no banco de dados mySQL.                            ###");
                System.out.println("####################################################################################################");
                System.out.println("");
                System.exit(0); // ← 0 = (Sair sem erro)  1 = (Sair com Erro)
            }
        }
        WebServer ws = null;
        if (!server.equals("") && !user.equals("") && !password.equals("") && !database.equals("")) {
            dbManager db = new dbManager(port, server, user, password, database, tabletest);
        }
        try{
            
             WebServer webServer = WebServers.createWebServer(port)
                .add("/", new WebSocket())
                .add(new StaticFileHandler("/web"))
                .start();
              System.out.println("Servidor funcionando no endereço "
                      + webServer.getUri());
        }catch(IOException e){
            System.err.println("Falha ao iniciar o servidor: "+e);
        }
    }
}
