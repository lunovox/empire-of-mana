package Formularios;

import com.mysql.jdbc.Connection;
import java.sql.*;

public final class mySQL {
    Statement stmt = null;
    Connection conn = null;
    ResultSet result = null;
    String $Servidor="";
    String $Usuario="";
    String $Senha="";
    String $Banco="";
    public mySQL(String $Servidor, String $Usuario, String $Senha, String $Banco) throws ClassNotFoundException, SQLException, Exception {
	openConnection($Servidor, $Usuario, $Senha, $Banco);
    }
    public void openConnection(String $Servidor, String $Usuario, String $Senha, String $Banco) throws ClassNotFoundException, SQLException, Exception {
	/* OBSERVAÇÃO:
	 *	É preciso especificar os AutoReconect e os Timeouts por parâmetro de URL e por proriedades de conexão, como foi mostrado abaixo.
	 *	Os parâmetro de URL servem para que o provedor mysql não inicie uma desconexão com o java.
	 *	As proriedades de conexão servem para que o java não inicie uma desconexão com o provedor mysql.
	 * O Java estava(ou ainda está. não sei!) dando bug quando uma pessoa tenta se conectar ao servidor após 6 horas(+/-) que o ...
	 * ... java passou sem consultar o mysql remoto.
	 */
	Class.forName("org.gjt.mm.mysql.Driver");
	DriverManager.setLoginTimeout(0);
	conn = (Connection) DriverManager.getConnection("jdbc:mysql://" + $Servidor + "/" + $Banco+"?initialTimeout=0&connectTimeout=0&socketTimeout=0&autoReconnect=true", $Usuario, $Senha);/**/
	conn.setAutoReconnect(true); // ← Reconect automaticamente se passar muito tempo sem consultas.
	conn.setAutoReconnectForPools(true); // ← Nã sei exatamente para que serv, mas parecer ser igualmente importante. =S
	conn.setAutoReconnectForConnectionPools(true); // ← Nã sei exatamente para que serv, mas parecer ser igualmente importante. =S
	conn.setInitialTimeout(0);
	conn.setSocketTimeout(0);
	conn.setConnectTimeout(0);
	stmt = conn.createStatement();
    }
    public void closeConnection() throws SQLException {
	conn.close();
    }
    public Connection getConnection() {
	return conn;
    }
    public Statement getStatement() {
	return stmt;
    }
    public void setResult(String $Comando) {
	result = getResult($Comando);
    }
    public ResultSet getResult() {
	return result;
    }
    public ResultSet getResult(String $Comando) {
	try {
	    return stmt.executeQuery($Comando);
	} catch (SQLException sqlex) {
	    //System.out.println("Falha de Consulta() → "+sqlex.getMessage());
	    sqlex.printStackTrace();
	}
	return null;
    }
    public void setUpdate(String $Comando) {
	getUpdate($Comando);
    }
    public String getUpdate(String $Comando) {
	try {
	    stmt.executeUpdate($Comando);
	    return "";
	} catch (SQLException sqlex) {
	    System.out.println("Falha de Consulta() → "+sqlex.getMessage());
	    //sqlex.printStackTrace();
	    return sqlex.getMessage();
	}
	//return "Houve um erro desconhecido!";
    }
    public int getCountRows() {
	return getCountRows(result);
    }
    public int getCountRows(ResultSet rs) {
	ResultSet newRS = rs;
	try {
	    newRS.first();
	    newRS.last();
	    return newRS.getRow();
	} catch (SQLException ex) {
	    //Logger.getLogger(mySQL.class.getName()).log(Level.SEVERE, null, ex);
	    System.out.println("Falha de contRows() → " + ex.getMessage());
	}
	return -1; // ← Se não encontrou!
    }
    public int getCountColumn() {
	return getCountColumn(result);
    }
    public int getCountColumn(ResultSet rs) {
	try {
	    ResultSetMetaData mData = rs.getMetaData();
	    return mData.getColumnCount();
	} catch (SQLException sqlex) {
	    System.out.println("Falha de contColumn() → " + sqlex.getMessage());
	}
	return 0;
    }
    public String getString(int $row, int $col) {
	return getString(result, $row, $col);
    }
    public String getString(ResultSet $rs, int $row, int $col) {
	try {
	    Object myObj = null;
	    //$rs.next();
	    $rs.absolute($row+1);
	    myObj = $rs.getObject($col + 1);
	    if(myObj!=null) return myObj.toString();
	    return "";
	    //return $rs.getString($col + 1);
	} catch (SQLException sqlex) {
	    sqlex.printStackTrace();
	}
	return null;
    }
    public Integer getInteger(int $row, int $col) {
	return getInteger(result, $row, $col);
    }
    public Integer getInteger(ResultSet $rs, int $row, int $col) {
	try {
	    Object myObj = null;
	    //$rs.next();
	    $rs.absolute($row+1);
	    myObj = $rs.getObject($col + 1);
	    if(myObj!=null) return Integer.parseInt(myObj.toString());
	    return 0;
	} catch (SQLException sqlex) {
	    sqlex.printStackTrace();
	}
	return null;
    }
    public Timestamp getTimestamp(int $row, int $col) {
	return getTimestamp(result, $row, $col);
    }
    public Timestamp getTimestamp(ResultSet $rs, int $row, int $col) {
	try {
	    Object myObj = null;
	    $rs.absolute($row+1);
	    return $rs.getTimestamp($col + 1);
	} catch (SQLException sqlex) {
	    sqlex.printStackTrace();
	}
	return null;
    }
}