package Formularios;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.RandomAccessFile;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class FileClass {

   public static void apagar(String PastaOuArquivo) {
      File Objeto = new File(PastaOuArquivo);
      Objeto.delete();
   }
   public static void arquivoCopiar(String De, String Para) {
      File CapsulaOrigem = new File(De);
      arquivoCopiar(CapsulaOrigem, Para);
   }
   public static void arquivoCopiar(File CapsulaOrigem, String Para) {
      File CapsulaDestino = new File(Para);
      CapsulaDestino.setExecutable(CapsulaOrigem.canExecute(), true);
      CapsulaDestino.setReadable(CapsulaOrigem.canRead());
      CapsulaDestino.setWritable(CapsulaOrigem.canWrite());
      try {
	 FileChannel Origem = new FileInputStream(CapsulaOrigem).getChannel();
	 FileChannel Destino = new FileOutputStream(CapsulaDestino).getChannel();
	 Origem.transferTo(0, Origem.size(), Destino);
	 if (Origem != null && Origem.isOpen()) {
	    Origem.close();
	 }
	 if (Destino != null && Destino.isOpen()) {
	    Destino.close();
	 }
      } catch (IOException Exc) {
	 //
      }/**/
   }
   public static void arquivoMover(String De, String Para) {
      File Origem = new File(De);
      File Destino = new File(Para);
      Origem.renameTo(Destino);
   }
   public static void pastaCriar(String EnderecoDaNovaPasta) {
      File dir = new File(EnderecoDaNovaPasta);
      dir.mkdirs();
   }
   public static String[] listarPasta(String Endereco) {
      ///home/indigovox/localhost/eathena-data/npc
      File Capsula = new File(Endereco);
      File[] Conteudo = Capsula.listFiles();
      int ContPastas = 0;
      for (int c = 0; c < Conteudo.length; c++) {
	 if (Conteudo[c].isDirectory()) {
	    ContPastas++;
	 }
      }
      String Pasta[] = new String[ContPastas];
      ContPastas = 0;
      for (int p = 0; p < Conteudo.length; p++) {
	 if (Conteudo[p].isDirectory()) {
	    Pasta[ContPastas] = Conteudo[p].getName();
	    //TxtScript.setText(TxtScript.getText()+Conteudo[c].getName()+"\n");
	    ContPastas++;
	 }
      }
      Arrays.sort(Pasta);
      return Pasta;
   }
   public static String[] listarArquivos(String Endereco) {
      ///home/indigovox/localhost/eathena-data/npc
      File Capsula = new File(Endereco);
      File[] Conteudo = Capsula.listFiles();
      int ContArquivos = 0;
      for (int c = 0; c < Conteudo.length; c++) {
	 if (Conteudo[c].isFile()) {
	    ContArquivos++;
	 }
      }
      String Arquivos[] = new String[ContArquivos];
      ContArquivos = 0;
      for (int a = 0; a < Conteudo.length; a++) {
	 if (Conteudo[a].isFile()) {
	    Arquivos[ContArquivos] = Conteudo[a].getName();
	    ContArquivos++;
	 }
      }
      Arrays.sort(Arquivos);
      return Arquivos;
   }
   public static boolean arquivoSeSimpleNome(File PastaPai, String NomDeArquivo) {
      /**
       * Fun��o Copiada de Site "http://forums.sun.com/thread.jspa?threadID=629458"
       * Eu n�o entendo essa Fun��o, mas tentei implementa-la para ver se serve.
       */
      if (PastaPai == null || NomDeArquivo == null) {
	 return false;
      }
      File Arquivo = new File(PastaPai, NomDeArquivo);
      if (!Arquivo.exists()) { //se o arquivo j� existe, pode ser o nome do arquivo est� correto
	 try {
	    boolean SeNomeValido = Arquivo.createNewFile();
	    //apagar o arquivo porque � criado para a verifica��o de valida��o
	    if (SeNomeValido) {
	       Arquivo.delete();
	    }
	    //Se o pai do novo arquivo e o pai dado � diferente, ent�o o nome do arquivo est� errado
	    if (!Arquivo.getParent().equals(PastaPai.getPath())) {
	       return false;
	    }
	    return SeNomeValido;
	 } catch (IOException ioe) {
	    return false;
	 }
      }
      //Se o pai do novo arquivo e o pai dado � diferente, ent�o o nome do arquivo est� errado
      return Arquivo.getParent().equals(PastaPai.getPath());
   }
   public static boolean seExiste(String PastaOuArquivo) {
      File Capsula;
      Capsula = new File(PastaOuArquivo);
      if (Capsula.exists()) {
	 return true;
      } else {
	 return false;
      }
   }
   public static boolean seInternet() {
      return seInternet("http://www.google.com");
   }
   public static boolean seInternet(String $SiteComparador) {
      try {
	 java.net.URL mandarMail = new java.net.URL($SiteComparador);
	 java.net.URLConnection conn = mandarMail.openConnection();

	 java.net.HttpURLConnection httpConn = (java.net.HttpURLConnection) conn;
	 httpConn.connect();
	 int x = httpConn.getResponseCode();
	 if (x == 200) {
	    return true;
	 }
      } catch (java.net.MalformedURLException urlmal) {
      } catch (java.io.IOException ioexcp) {
      }
      return false;
   }
   public static boolean arquivoSalvar(String Endereco, String Conteudo) {
      try {
	 BufferedWriter Capsula = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(Endereco), "UTF-8"));
	 String Cabecalho = "";
	 Capsula.write(Conteudo);
	 Capsula.flush();
	 Capsula.close();
	 return true;
      } catch (java.io.IOException exc) {
	 return false;
      }
   }
   public static String arquivoAbrir_deprecado(String Endereco) {
      try {
	 String Conteudo = "", Linha = "";
	 BufferedReader Capsula = new BufferedReader(new InputStreamReader(new FileInputStream(Endereco), "UTF-8"));
	 while ((Linha = Capsula.readLine()) != null) {
	    if (!Conteudo.equals("")) {
	       Conteudo = Conteudo + "\n" + Linha;
	    } else {
	       Conteudo = Linha;
	    }
	 }
	 Capsula.close();
	 return Conteudo;
      } catch (java.io.IOException exc) {
	 return null;
      }
   }
   public static String arquivoAbrir(String Endereco) {
      String Conteudo = "";
      int Tamanho = 1024 * 1024 * 32;
      ByteBuffer buf = ByteBuffer.allocate(Tamanho); //create buffer with capacity of 48 bytes
      try {
	 RandomAccessFile aFile = new RandomAccessFile(Endereco, "rw");
	 FileChannel inChannel = aFile.getChannel();
	 int bytesRead = inChannel.read(buf);
	 char Letra = 0;
	 byte[] bytearray = null;
	 while (bytesRead != -1) {
	    buf.flip();
	    bytearray = new byte[buf.remaining()];
	    while (buf.hasRemaining()) {
	       buf.get(bytearray);
	    }
	    Conteudo += new String(bytearray);
	    buf.clear();
	    bytesRead = inChannel.read(buf);
	 }
	 return Conteudo;
      } catch (IOException e) {
	 return Conteudo;
      }
   }
   public static Element arquivoAbrirXML(String Endereco) {
      if(Endereco!=null && !Endereco.equals("")){
	 try {
	    /*File Arq = null;
	    if(Endereco.toLowerCase().indexOf("http://")==0 || Endereco.toLowerCase().indexOf("https://")==0){
	       URI $uri = new URI(Endereco);
	       Arq = new File($uri);
	    }/**/
	    //DocumentBuilder builder = factory.newDocumentBuilder();
	    //Document document = builder.parse(new File("teste2.xml"));

	    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	    DocumentBuilder dBuilder = dbf.newDocumentBuilder();
	    Document Documento = null;
	    if(Endereco.toLowerCase().indexOf("http://")==0 || Endereco.toLowerCase().indexOf("https://")==0){
	       /*URL arquivo = new URL(Endereco);
	       InputStreamReader $ISR = new InputStreamReader(arquivo.openStream());
	       InputStream $IS = new InputStream();

	       Documento = dBuilder.parse( new InputStream() {

		  @Override
		  public int read() throws IOException {
		     throw new UnsupportedOperationException("Not supported yet.");
		  }
	       });/**/
	       URL $arq = new URL(Endereco);
	       Documento = dBuilder.parse($arq.toURI().toString());
	    }else{
	       Documento = dBuilder.parse(new File(Endereco));
	    }
	    //Document Documento =db.parse(Endereco);
	    return Documento.getDocumentElement();
	 } catch (URISyntaxException ex) {
	    //Logger.getLogger(FileClass.class.getName()).log(Level.SEVERE, null, ex);
	 } catch (ParserConfigurationException ex) {
	    //Logger.getLogger(FileClass.class.getName()).log(Level.SEVERE, null, ex);
	 } catch (IOException ex) {
	    //Logger.getLogger(FileClass.class.getName()).log(Level.SEVERE, null, ex);
	 } catch (SAXException ex) {
	    //Logger.getLogger(FileClass.class.getName()).log(Level.SEVERE, null, ex);
	 }
      }
      return null;
   }
   public static boolean arquivoSalvarXML(String Endereco, Document Doc) {
      return arquivoSalvarXML(Endereco, new DOMSource(Doc), "utf-8");
   }
   public static boolean arquivoSalvarXML(String Endereco, DOMSource Source) {
      return arquivoSalvarXML(Endereco, Source, "utf-8");
   }
   public static boolean arquivoSalvarXML(String Endereco, Document Doc, String Encoding) {
      return arquivoSalvarXML(Endereco, new DOMSource(Doc), Encoding);
   }
   public static boolean arquivoSalvarXML(String Endereco, DOMSource Source, String Encoding) {
      try {
	 Transformer trans = TransformerFactory.newInstance().newTransformer();
	 trans.setOutputProperty(OutputKeys.INDENT, "yes");//Saber se o XML ser� identado(ter� espa�os entre tags).
	 trans.setOutputProperty(OutputKeys.STANDALONE, "yes");
	 trans.setOutputProperty(OutputKeys.ENCODING, Encoding);
	 trans.transform(
		 Source,
		 new StreamResult(new FileOutputStream(Endereco)));
	 ////////////////////////////////////////////////////////////////////////////////////////////////
	 // Salva compactado.
	 //trans.transform( new GZipStreamSource(new File("input.xml")), new StreamResult(System.out) );
	 /////////////////////////////////////////////////////////////////////////////////////////////////
	 return true;
      } catch (FileNotFoundException ex) {
	 Logger.getLogger(FileClass.class.getName()).log(Level.SEVERE, null, ex);
	 return false;
      } catch (TransformerException ex) {
	 Logger.getLogger(FileClass.class.getName()).log(Level.SEVERE, null, ex);
	 return false;
      }
   }
   public static int getAtributo(Element elem, String atributo, int padrao) {
      try {
	 return Integer.parseInt(elem.getAttribute(atributo));
      } catch (Exception ex) {
	 //Logger.getLogger(FileClass.class.getName()).log(Level.SEVERE, null, ex);
      }
      return padrao;
   }
   public static boolean getAtributo(Element elem, String atributo, boolean padrao) {
      try {
	 return Boolean.parseBoolean(elem.getAttribute(atributo));
      } catch (Exception ex) {
	 //Logger.getLogger(FileClass.class.getName()).log(Level.SEVERE, null, ex);
      }
      return padrao;
   }
   public static String getAtributo(Element elem, String atributo, String padrao) {
      String str = elem.getAttribute(atributo);
      if (str == null) {
	 str = padrao;
      }
      return str;
   }/**/
   public static int getConteudo(Element elemento, String tag, int padrao) {
      try {
	 NodeList children = elemento.getElementsByTagName(tag);
	 if (children == null) {
	    return padrao;
	 }
	 Element child = (Element) children.item(0);
	 if (child == null) {
	    return padrao;
	 }
	 return Integer.parseInt(child.getFirstChild().getNodeValue());
      } catch (Exception ex) {
	 //Logger.getLogger(FileClass.class.getName()).log(Level.SEVERE, null, ex);
      }
      return padrao;
   }
   public static String getConteudo(Element elemento, String tag, String padrao) {
      try {
	 NodeList children = elemento.getElementsByTagName(tag);
	 if (children == null) {
	    return padrao;
	 }
	 Element child = (Element) children.item(0);
	 if (child == null) {
	    return padrao;
	 }
	 return child.getFirstChild().getNodeValue();
      } catch (Exception ex) {
	 //Logger.getLogger(FileClass.class.getName()).log(Level.SEVERE, null, ex);
      }
      return padrao;
   }/**/
   public static String urlAbrir(String Endereco) {
      try {
	 URL arquivo = new URL(Endereco);
	 BufferedReader in = new BufferedReader(new InputStreamReader(arquivo.openStream()));
	 String str, Conteudo = "";
	 int contador = 0;
	 while ((str = in.readLine()) != null) {
	    contador += 1;
	    Conteudo += (Conteudo.equals("") ? str : "\n" + str);
	 }
	 in.close();
	 return Conteudo;
      } catch (IOException e) {
	 return null;
      }
   }
}
