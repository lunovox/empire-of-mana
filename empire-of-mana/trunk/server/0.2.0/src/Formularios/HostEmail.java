/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Formularios;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Properties;
import javax.mail.AuthenticationFailedException;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class HostEmail {
   private String fromSMTP = ""; // antes era "smtp2.locaweb.com.br"
   private String fromPort = "";
   private String fromName = "";
   private String fromMail = "";
   private String fromPassword = "";
   private Properties myProperties = new Properties();
   private Session mySession = null;
   private MimeMessage myMessage = null;
   private Authenticator Identificacao = null;
   private Boolean EnableTTLs = false;
   
   public HostEmail(String SMTP, String FromPort, String FromName, String FromEmail, String FromPassWord, boolean EnableTTLs){
      //Exemplo: setHost("smtp.gmail.com", "465", "Empire of Mana - MMORPG(Free-P2W)", "empireofmana@tuatec.com.br", "******", true);
      setHost(SMTP, FromPort, FromName, FromEmail, FromPassWord, EnableTTLs);
   }
   public final void setHost(String SMTP, String FromPort, String FromName, String FromEmail, String FromPassWord, boolean EnableTTLs){
      if(SMTP!=null && !SMTP.equals(""))           this.fromSMTP = SMTP;
      if(FromPort!=null && !FromPort.equals(""))   this.fromPort = FromPort;
      if(FromName!=null && !FromName.equals(""))   this.fromName = FromName;
      if(FromEmail!=null && !FromEmail.equals("")) this.fromMail = FromEmail;
      if(FromPassWord!=null && !FromPassWord.equals("")) this.fromPassword = FromPassWord;
      this.EnableTTLs=EnableTTLs;

      myProperties.put("mail.host", this.fromSMTP);
      //myProperties.put("mail.debug", "true");
      myProperties.put("mail.smtp.debug", "true");
      myProperties.put("mail.mime.charset", "ISO-8859-1");
      myProperties.put("mail.smtp.socketFactory.fallback", "false");
      myProperties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");/**/
      if(FromPort!=null && !FromPort.equals("")){
         myProperties.put("mail.smtp.port", this.fromPort); //myProperties.put("mail.smtp.port","465");
         myProperties.put("mail.smtp.socketFactory.port", this.fromPort);
      }
      if(this.EnableTTLs){
	 myProperties.put("mail.smtp.starttls.enable","true");
	 //System.out.println("TTL ativado!");
      }
      
      if(FromPassWord!=null && !FromPassWord.equals("")){
	 //System.out.println("Verificando identificação!");
	 myProperties.put("mail.smtp.auth", "true");
	 //System.out.println("Usuário:"+this.fromMail+"    Senha:"+this.fromPassword);
	 mySession = Session.getInstance(myProperties,new Authenticator() {
	    @Override
	    protected PasswordAuthentication getPasswordAuthentication() {
	       return new PasswordAuthentication(fromMail, fromPassword);
	    }
	 });
      }else{
	 mySession = Session.getInstance(myProperties, null);
	 //System.out.println("Acessando sem identificação!");
      }
      myMessage = new MimeMessage(mySession);
   }
   public String getHostSMTP(){
      return this.fromSMTP;
   }
   public Session getSession(){
      return this.mySession;
   }
   public MimeMessage getMimeMessage(){
      return this.myMessage;
   }
   public boolean isEnableTTLs(){
      return this.EnableTTLs;
   }
   public String sendEmail(String $ParaNome, String $ParaEmail, String $Assunto, String $Mensagem) {
      if(mySession!=null){
	 myMessage = new MimeMessage(mySession);
	 try {
	    // "de" e "para"!!
	    myMessage.setFrom(new InternetAddress(fromMail,fromName));
	    myMessage.setRecipient(
	       Message.RecipientType.TO,
	       new InternetAddress($ParaEmail,$ParaNome)
	    );
	    myMessage.setSentDate(new Date()); // ← não esqueca da data ou irá 31/12/1969 !!!
	    myMessage.setSubject($Assunto);
	    myMessage.setText($Mensagem);
	    Transport.send(myMessage); // ← enviando mensagem (tentando)
	    return "";
	 } catch (UnsupportedEncodingException ex) {
	    return " → Falha de codificação!";
	    //Logger.getLogger(HostEmail.class.getName()).log(Level.SEVERE, null, ex);/**/
	 } catch (AddressException ex) {
	    return " → Falha no formato de endereço de email!";
	    //Logger.getLogger(HostEmail.class.getName()).log(Level.SEVERE, null, ex);
	 } catch (AuthenticationFailedException ex) {
	    return " → Falha na identificação com senha do SMTP email!";
	    //Logger.getLogger(HostEmail.class.getName()).log(Level.SEVERE, null, ex);
	 } catch (MessagingException ex) {
	    return " → Falha no corpo de mensagem!";
	    //Logger.getLogger(HostEmail.class.getName()).log(Level.SEVERE, null, ex);
	 } catch (Exception ex) {
	    return " → Falha de envio de email!";
	    //Logger.getLogger(HostEmail.class.getName()).log(Level.SEVERE, null, ex);
	 }
      }
      return " → Falha de início de sessão!";
   }
   /*public final class UsuarioSenha extends Authenticator {
      public UsuarioSenha(String Username, String PassWord) {
	 getPasswordAuthentication(Username, PassWord);
      }
      public PasswordAuthentication getPasswordAuthentication(String Username, String PassWord) {
	 //Username = "xxxxx@googlemail.com";
	 //PassWord = "xxxxx";
	 return new PasswordAuthentication(Username, PassWord);
      }
   }/**/
}
