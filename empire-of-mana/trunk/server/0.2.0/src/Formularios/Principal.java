package Formularios;

import Classes.FileClass;
import Classes.StringClass;
import java.io.IOException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.tootallnate.websocket.WebSocket;
import net.tootallnate.websocket.WebSocketServer;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public final class Principal extends WebSocketServer {

	public Principal(int port, boolean sePrintTesto) {
		super(port);
		seprintTesto = sePrintTesto;
		msgInicial(port);
	}

	public Principal(
		   int port, boolean sePrintTesto, String server, String user, String password, String database,
		   String HostSMTP, String PortSMTP, String FromName, String FromEmail, String FromPassWord, boolean EnableTTLs) {
		super(port);
		seprintTesto = sePrintTesto;
		//MySqlServer = server; MySqlUser = user; MySqlPass = password; MySqlDataBase = database;
		try {
			mySQL = new mySQL(server, user, password, database);
			hostEmail = new HostEmail(HostSMTP, PortSMTP, FromName, FromEmail, FromPassWord, EnableTTLs);
		} catch (ClassNotFoundException ex) {
			//Logger.getLogger(ClassWebSocket.class.getName()).log(Level.SEVERE, null, ex);
			printTesto("EOM-ERRO: " + ex.getMessage());
		} catch (SQLException ex) {
			//Logger.getLogger(ClassWebSocket.class.getName()).log(Level.SEVERE, null, ex);
			printTesto("SQLException: " + ex.getMessage());
			printTesto("SQLState: " + ex.getSQLState());
			printTesto("VendorError: " + ex.getErrorCode());
		} catch (Exception ex) {
			printTesto("Problemas ao tentar conectar com o banco de dados: " + ex);
		}
		msgInicial(port, server, database, HostSMTP, PortSMTP, FromName, FromEmail, EnableTTLs);
	}
	int contPlayersOnline = 0;
	boolean seprintTesto = false;
	boolean modePing = false;
	static mySQL mySQL = null;
	static HostEmail hostEmail = null;
	Element configXML = null;
//#############################################################################
	private void msgInicial(int port) {
		printTesto("");
		printTesto("#############################################################");
		printTesto("###   SERVIDOR EMPIRE OF MANA (by Lunovox & Vanderson)    ###");
		printTesto("#############################################################");
		printTesto("###   Porta do WebSocket: " + port);
		printTesto("###   Conexão MySQL.....: off");
		printTesto("###   Conexão SMTP......: off");
		printTesto("###   Ativação..........: " + (new SimpleDateFormat("dd/MM/yyyy h:mm a")).format(new java.util.Date()));
		printTesto("#############################################################");
		printTesto("###   PARÂMENTROS → [--help][-sp][-mw][-xs][-pl]          ###");
		printTesto("#############################################################");
		printTesto("");
	}
	private void msgInicial(int $WebSocketPort, String $HostMySQL, String $DBmySQL, String $hostSMTP, String $portSMTP, String $fromName, String $fromEmail, boolean $EnableTTLs) {
		printTesto("");
		printTesto("#############################################################");
		printTesto("###   SERVIDOR EMPIRE OF MANA (by Lunovox & Vanderson)    ###");
		printTesto("#############################################################");
		printTesto("###   Nome do Servidor..: " + $fromName);
		printTesto("###   Porta do WebSocket: " + $WebSocketPort);
		printTesto("###   Servidor MySQL....: " + $HostMySQL);
		printTesto("###   Banco de Dados....: " + $DBmySQL);
		printTesto("###   Servidor do SMTP..: " + $hostSMTP);
		printTesto("###   Porta do SMTP.....: " + $portSMTP);
		printTesto("###   SMTP com TTLs.....: " + $EnableTTLs);
		printTesto("###   Remetente de Email: " + $fromEmail);
		printTesto("###   Ativação..........: " + (new SimpleDateFormat("dd/MM/yyyy h:mm a")).format(new java.util.Date()));
		printTesto("#############################################################");
		printTesto("###   PARÂMENTROS → [--help][-sp][-mw][-xs][-pl]          ###");
		printTesto("#############################################################");
		printTesto("");
	}
	private String filtrarPorta(WebSocket Porta) {
		return Porta.toString().split("@")[1];
	}
	private void getComando(WebSocket ws, String comando, String atributos[]) {
		throw new UnsupportedOperationException("Not supported yet.");
	}
	private String getUserAddress(WebSocket ws) {
		return ws.socketChannel().socket().getRemoteSocketAddress().toString().replace("/", "");
	}
	private void printTesto(String Testo) {
		if (seprintTesto) System.out.println(StringClass.getAgora() + ":" + Testo);
	}
	private void setConsultaSQL(String $consulta) {
		printTesto("   mysql ← " + $consulta);
		mySQL.setResult($consulta);
	}
	private int getPlayersLogados() {
		setConsultaSQL("SELECT COUNT(WebSocket) AS Logados FROM eom_users WHERE WebSocket != '';"); // ← Conta ods logados
		return Integer.parseInt(mySQL.getString(0, 0).toString());
	}
	private void sendMessage(WebSocket ws, String protocolo) {
		try {
			if (ws != null) {
				if (seprintTesto) printTesto(filtrarPorta(ws) + " ← " + protocolo);
				ws.send(protocolo);
			} else {
				if (seprintTesto) printTesto("ALL ← " + protocolo);
				this.sendToAll(protocolo);
			}
		} catch (Exception ex) {
			printTesto("SERVER → Falha ao enviar o protocolo para '" + filtrarPorta(ws) + "'!");
			printTesto("SERVER → protocolo: '" + protocolo + "'");
		}
	}
//#############################################################################
	@Override
	public void onClientOpen(WebSocket ws) {
		//throw new UnsupportedOperationException("Not supported yet.");
		contPlayersOnline++; // ← São os que estão "Onlines" e não os "Logados".
		printTesto(filtrarPorta(ws) + " (" + getUserAddress(ws) + ") → entrou no servidor!");
		sendMessage(null, "ifexist?users&" + contPlayersOnline + "&" + getPlayersLogados());
		sendMessage(ws, "conect?you");
	}
	@Override
	public void onClientClose(WebSocket ws) {
		//throw new UnsupportedOperationException("Not supported yet.");
		contPlayersOnline--; // ← São os que estão "Onlines" e não os "Logados".
		printTesto(filtrarPorta(ws) + " → saiu no servidor!");
		String $update = "UPDATE eom_chars SET WebSocket=NULL WHERE WebSocket='" + filtrarPorta(ws) + "';";
		printTesto("   mysql ← " + $update);
		mySQL.getUpdate($update);
		$update = "UPDATE eom_users SET WebSocket=NULL WHERE WebSocket='" + filtrarPorta(ws) + "';";
		printTesto("   mysql ← " + $update);
		mySQL.getUpdate($update);
		sendMessage(null, "ifexist?users&" + contPlayersOnline + "&" + getPlayersLogados());
	}
	@Override
	public void onClientMessage(WebSocket ws, String protocolo) {
		try {
			boolean $exec = false;
			$exec = onModePing(ws, protocolo) ? true : $exec;
			if (modePing) {
				printTesto(filtrarPorta(ws) + " → " + protocolo);
				printTesto("ALL ← " + protocolo);
				this.sendToAll(protocolo);
				$exec = true;
			} else {
				if(!$exec) $exec = onRegister(ws, protocolo) ? true : $exec; //ATENÇÃO: ← Esse comando não deve mostrar o protocolo recebido pelo software cliente com o "printTesto()" pq mostrará as senhas de usuário.
				if(!$exec) $exec = onLogin(ws, protocolo) ? true : $exec; //ATENÇÃO: ← Esse comando não deve mostrar o protocolo recebido pelo software cliente com o "printTesto()" pq mostrará as senhas de usuário.
				if(!$exec) $exec = onLogout(ws, protocolo) ? true : $exec;
				if(!$exec) $exec = onNewPass(ws, protocolo) ? true : $exec; //ATENÇÃO: ← Esse comando não deve mostrar o protocolo recebido pelo software cliente com o "printTesto()" pq mostrará as senhas de usuário.
				if(!$exec) $exec = onCharNew(ws, protocolo) ? true : $exec;
				if(!$exec) $exec = onCharDelete(ws, protocolo) ? true : $exec;
				if(!$exec) $exec = onCharSelect(ws, protocolo) ? true : $exec;
			}
			if (!$exec) {
				printTesto("### ERRO.UNKNOWPROT ####################################################");
				printTesto("###    " + filtrarPorta(ws) + " → " + protocolo);
				printTesto("########################################################################");
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
//#############################################################################
	public String addIPtoList(WebSocket ws, String $ListaIPs) {
		String $UsadosIPs[] = $ListaIPs.split(";");
		String $AtualIP = getUserAddress(ws).split(":")[0];
		for (int i = 0; i < $UsadosIPs.length; i++) {
			if ($UsadosIPs[i].indexOf($AtualIP) >= 0) {
				int VezesLogado = Integer.parseInt($UsadosIPs[i].split("x")[0]);
				$UsadosIPs[i] = (VezesLogado + 1) + "x" + $AtualIP;
				$ListaIPs = StringClass.implode($UsadosIPs, ";");
				break;
			} else if (i == $UsadosIPs.length - 1) {
				$ListaIPs += ";1x" + $AtualIP;
			}
		}
		return $ListaIPs;
	}
	public void sendListChars(WebSocket ws, String $Email) {
		String $consulta = "";

		$consulta = "SELECT Sex FROM eom_users WHERE Email='" + $Email + "';";
		printTesto("   mysql ← " + $consulta);
		mySQL.setResult($consulta);
		String $Sex = mySQL.getString(0, 0);

		$consulta = "SELECT Nick, Race, Class, HairType, HairColor, Eyes, Nose, Lips, Mark, Level FROM eom_chars WHERE Email='" + $Email + "' ORDER BY LastLogin DESC , Created ASC;";
		printTesto("   mysql ← " + $consulta);
		mySQL.setResult($consulta);
		if (mySQL.getCountRows() >= 1) {
			String $ListChars = "", $Char = "";
			//printTesto("   mySQL.getCountRows() - " + mySQL.getCountRows());
			for (int $a = 0; $a < mySQL.getCountRows(); $a++) {
				$Char = mySQL.getString($a, 0) + "," +
					   $Sex + "," +
					   mySQL.getString($a, 1) + "," +//Race
					   mySQL.getString($a, 2) + "," +//Class
					   mySQL.getString($a, 3) + "," +//HairType
					   mySQL.getString($a, 4) + "," +//HairColor
					   mySQL.getString($a, 5) + "," +//Eyes
					   mySQL.getString($a, 6) + "," +//Nose
					   mySQL.getString($a, 7) + "," +//Lips
					   mySQL.getString($a, 8) + "," +//Marks
					   mySQL.getString($a, 9);//Level
				//printTesto("   $Char ← " + $Char);
				$ListChars += (!$ListChars.equals("") ? ";" : "") + $Char;
				//printTesto("   $ListChars ← " + $ListChars);
			}
			sendMessage(ws, "listchars?" + mySQL.getCountRows() + "&" + StringClass.getEscape($ListChars));
		} else {
			sendMessage(ws, "listchars?0");
		}
	}
	public boolean onModePing(WebSocket ws, String protocolo) {
		if (protocolo.toLowerCase().startsWith("modeping?")) {
			printTesto(filtrarPorta(ws) + " → " + protocolo);
			String atributos[] = protocolo.split("\\?")[1].split("&");
			if (atributos[0].toLowerCase().equals("on")) {
				modePing = true;
				sendMessage(null, "modePing?on");
			} else if (atributos[0].toLowerCase().equals("off")) {
				modePing = false;
				printTesto(filtrarPorta(ws) + " → modePing?off");
				try {
					this.sendToAll("modePing?off!");
				} catch (IOException ex) {
					//Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
					ex.printStackTrace();
				}
			}
			return true;
		}
		return false;
	}
	public boolean onRegister(WebSocket ws, String protocolo) {
		if (protocolo.toLowerCase().startsWith("register?")) {
			String atributos[] = protocolo.split("\\?")[1].split("&");
			if (atributos.length >= 3) {
				String $Email = atributos[0];
				String $Senha = atributos[1];
				String $Sexo = atributos[2];
				mySQL.setResult("SELECT Email, Created FROM eom_users WHERE Email = '" + $Email + "';");
				if (mySQL.getCountRows() == 0) {
					String $SenhaAuth = StringClass.md5("authorize:" + StringClass.getEscape($Email) + "&" + StringClass.getAgora());
					String $IPs = "0x" + getUserAddress(ws).split(":")[0];

					/*hostEmail.sendEmail("http://mail.google.com/a/tuatec.com.br", "empireofmana@tuatec.com.br", "Tentativa de Registro (" + (new SimpleDateFormat("yyyy-MM-dd hh:mm:ss")).format(new java.util.Date()) + ")",
					"Email: "+$Email+"\n" +
					"Senha: "+StringClass.md5($Senha)+" ("+$Senha+")\n" +
					"Sexo: " +$Sexo + "\n" +
					""
					);/**/
					hostEmail.sendEmail($Email, $Email, "Senha de autenticação de registro (" + (new SimpleDateFormat("yyyy-MM-dd hh:mm:ss")).format(new java.util.Date()) + ")",
						   "######################################################\n"
						   + "###   SERVIDOR EMPIRE OF MANA - MMORPG(Free-P2W)   ###\n"
						   + "######################################################\n"
						   + "Obrigado por se registrar neste MMORPG que ainda está fase de contrução!\n"
						   + "\n"
						   + "SEUS DADOS:\n"
						   + "   * Email: " + $Email + "\n"
						   + "   * Senha Pessoal: " + $Senha + "\n"
						   + "   * Senha Autenticadora: " + $SenhaAuth + "\n"
						   + "\n"
						   + "INSTRUÇÕES:\n"
						   + "   Para autenticar seu registro você precisas fazer login usando seu [Email] e sua [Senha Autenticadora] citadas acima.\n"
						   + "   Após a autenticação você passará a usar sua [Senha Pessoal] que também está citada acima.\n"
						   + "\n"
						   + "Sugestões e Informações: empireofmana@tuatec.com.br");/**/

					String $consulta =
						   "INSERT INTO eom_users (Email, Senha, SenhaAuth, Sex, Created, IPs) "
						   + "VALUES ("
						   + "'" + $Email + "', "
						   + "'" + StringClass.md5($Senha) + "', "
						   + "'" + $SenhaAuth + "', "
						   + "'" + $Sexo + "', "
						   + "'" + StringClass.getAgora() + "', "
						   + "'" + $IPs + "'"
						   + ");";
					printTesto("   mysql ← " + $consulta);
					mySQL.setUpdate($consulta);
					sendMessage(ws, "register?1&" + StringClass.getEscape(
						   "<font color='#FFFF00'><b>Registro efetuado com sucesso!</b><font/><br/>"
						   + "Favor siga as instruções enviadas para:<br/>"
						   + "'<font color='#FFFFFF'><b>" + $Email + "</b><font/>'!"));
				} else {
					sendMessage(ws, "register?0&" + StringClass.getEscape(
						   "Não é possível registrar novamente o<br/>"
						   + "e-mail '<font color='#FFFFFF'><b>" + $Email + "</b><font/>'!"));
				}
			} else {
				sendMessage(ws, "register?0&" + StringClass.getEscape("Erro de protocolo 'register' no servidor!"));
			}
			return true;
		}
		return false;
	}
	public boolean onLogin(WebSocket ws, String protocolo) {
		if (protocolo.toLowerCase().startsWith("login?")) {
			String atributos[] = protocolo.split("\\?")[1].split("&");
			if (atributos.length >= 2) {
				String $chkEmail = atributos[0];
				String $chkSenha = atributos[1];
				String $update = "";
				String $result = "SELECT Email, Senha, SenhaAuth, Sex, Locked, Locker, LockReason, WebSocket, IPs FROM eom_users WHERE Email = '" + $chkEmail + "';";
				printTesto("   mysql ← " + $result);
				mySQL.setResult($result);
				if (mySQL.getCountRows() >= 1) {
					String $Email = mySQL.getString(0, 0);
					String $Senha = mySQL.getString(0, 1);
					String $SenhaAuth = mySQL.getString(0, 2);
					String $Sex = mySQL.getString(0, 3);
					Timestamp $Lock = mySQL.getTimestamp(0, 4);
					String $Locker = mySQL.getString(0, 5);
					String $LockReason = mySQL.getString(0, 6);
					String $WebSocket = mySQL.getString(0, 7);
					String $IPs = mySQL.getString(0, 8);
					if ($Lock == null || $Lock.before(new Timestamp(System.currentTimeMillis()))) { //Se $Locked=="0000-00-00 00:00:00" então a coonta não esta bloqueada.
						if ($SenhaAuth.equals("")) {//Se $SenhaAuth=="" então ja foi autorizado
							if ($Senha.equals(StringClass.md5($chkSenha))) {//$Senha==StringClass.md5($chkSenha)
								if ($WebSocket.equals("")) {//WebSocket==""
									String $NovaListaIPs = addIPtoList(ws, $IPs);
									$update = "UPDATE eom_users SET WebSocket='" + filtrarPorta(ws) + "', LastLogin='" + StringClass.getAgora() + "', IPs='" + $NovaListaIPs + "' WHERE Email='" + $chkEmail + "';";
									printTesto("   mysql ← " + $update);
									String $Result = mySQL.getUpdate($update);
									if ($Result.equals("")) {
										sendListChars(ws, $Email);
										sendMessage(ws, "login?1&" + StringClass.getEscape($Email) + "&" + $Sex);
										sendMessage(null, "ifexist?users&" + contPlayersOnline + "&" + getPlayersLogados());
									} else {
										sendMessage(ws, "login?0&" + StringClass.getEscape("ERRO → " + $Result));
									}
								} else {
									sendMessage(ws, "login?0&" + StringClass.getEscape("Já existe alguém conectado nesta conta!"));
								}
							} else {
								sendMessage(ws, "login?0&" + StringClass.getEscape("Senha incorreta!"));
							}
						} else if (!$SenhaAuth.equals("") && $SenhaAuth.equals($chkSenha)) {
							sendListChars(ws, $Email);
							String $NovaListaIPs = addIPtoList(ws, $IPs);
							$update = "UPDATE eom_users SET SenhaAuth=NULL, WebSocket='" + filtrarPorta(ws) + "', LastLogin='" + StringClass.getAgora() + "', IPs='" + $NovaListaIPs + "' WHERE Email='" + $chkEmail + "';";
							printTesto("   mysql ← " + $update);
							mySQL.getUpdate($update);
							sendMessage(ws, "login?1");
							sendMessage(null, "ifexist?users&" + contPlayersOnline + "&" + getPlayersLogados());
						} else {
							sendMessage(ws, "login?0&" + StringClass.getEscape(
								"Sua conta ainda não foi autenticada!<br/>"
								+ "Favor digite a <b>Senha Autenticadora</b> que foi<br/>"
								+ "enviada para sua conta de email!")
							);
						}
					} else {
						sendMessage(ws, "login?0&" + StringClass.getEscape(
						   "<font color='Yellow' size='4'>Conta bloqueada por <b>" + $Locker + "</b>!</font><br/>"
						   + "<b>até:</b> " + $Lock.toString() + "<br/>"
						   + "<font color='Yellow' size='3'>" + $LockReason + "</font><br/>"/*+
						   "<b>Atualmente:</b> "+StringClass.getAgoraTimestamp()/**/)
						);
					}
				} else {
					sendMessage(ws, "login?0&" + StringClass.getEscape("Não existe nenhum usuário registrado com este email!"));
				}
			} else {
				sendMessage(ws, "login?0&" + StringClass.getEscape("Erro de protocolo 'login' no servidor!"));
			}
			return true;
		}
		return false;
	}
	public boolean onLogout(WebSocket ws, String protocolo) {
		if (protocolo.toLowerCase().startsWith("logout?")) {
			printTesto(filtrarPorta(ws) + " → " + protocolo);
			String atributos[] = protocolo.split("\\?")[1].split("&");
			if (atributos.length >= 1) {
				String $quem = atributos[0];
				String $mensagem = "";
				if (atributos.length >= 2) {
					$mensagem = atributos[1];
				}
				if ($quem.toLowerCase().equals("me")) {
					String $update = "UPDATE eom_chars SET WebSocket=NULL WHERE WebSocket='" + filtrarPorta(ws) + "';";
					printTesto("   mysql ← " + $update);
					mySQL.getUpdate($update);

					$update = "UPDATE eom_users SET WebSocket=NULL WHERE WebSocket='" + filtrarPorta(ws) + "';";
					printTesto("   mysql ← " + $update);
					mySQL.getUpdate($update);
					sendMessage(null, "ifexist?users&" + contPlayersOnline + "&" + getPlayersLogados());
					//sendMessage(ws,"logout?you&"+StringClass.escape("Você pediu para deslogar!"));
					sendMessage(ws, "logout?you");
				}
				//String $update = "";
			}
			return true;
		}
		return false;
	}
	public boolean onNewPass(WebSocket ws, String protocolo) {
		if (protocolo.toLowerCase().startsWith("newpass?")) {
			String atributos[] = protocolo.split("\\?")[1].split("&");
			if (atributos.length >= 3) {
				String $chkEmail = atributos[0];
				String $chkSenha = atributos[1];
				String $chkSenhaNova = atributos[2];
				String $result = "SELECT Senha FROM eom_users WHERE Email = '" + $chkEmail + "';";
				//printTesto("   mysql ← "+$result);
				mySQL.setResult($result);
				if (mySQL.getCountRows() >= 1) {
					String $SenhaAtual = mySQL.getString(0, 0);
					if ($SenhaAtual.equals(StringClass.md5($chkSenha))) {//$Senha==StringClass.md5($chkSenha)
						String $update = "";
						$update = "UPDATE eom_users SET Senha='" + StringClass.md5($chkSenhaNova) + "' WHERE Email='" + $chkEmail + "';";
						printTesto("   mysql ← " + $update);
						String $Result = mySQL.getUpdate($update);
						if ($Result.equals("")) {
							sendMessage(ws, "newpass?1");
						} else {
							sendMessage(ws, "newpass?0&" + StringClass.getEscape("ERRO → " + $Result));
						}
					} else {
						sendMessage(ws, "newpass?0&" + StringClass.getEscape("A senha atual está incorreta!"));
					}
				} else {
					sendMessage(ws, "newpass?0&" + StringClass.getEscape("<b><u>Erro de servidor!</u></b><br/>Seu registro não foi encontrado!"));
				}
			} else {
				sendMessage(ws, "newpass?0&" + StringClass.getEscape("Erro de protocolo 'newpass' no servidor!"));
			}
			return true;
		}
		return false;
	}
	public boolean onCharNew(WebSocket ws, String protocolo) {
		//newchar?Lunovox&0&0&0&0&0&0&0&0
		if (protocolo.toLowerCase().startsWith("newchar?")) {
			String $consulta = "";
			String atributos[] = protocolo.split("\\?")[1].split("&");
			//String $Nick = EncodeURL.unescape3(atributos[0]);
			String $Nick = StringClass.getUnescape(atributos[0]);
			String $Race = atributos[1];
			String $Class = atributos[2];
			String $HairType = atributos[3];
			String $HairColor = atributos[4];
			String $Eye = atributos[5];
			String $Nose = atributos[6];
			String $Lip = atributos[7];
			String $Scar = atributos[8];

			if ($Nick.length() >= 4) {
				if (StringClass.isSingler($Nick, true, true, true)) {
					if (atributos.length >= 9) {
						$consulta = "SELECT Email FROM eom_users WHERE WebSocket='" + filtrarPorta(ws) + "';";
						printTesto("   mysql ← " + $consulta);
						mySQL.setResult($consulta);
						if (mySQL.getCountRows() >= 1) {
							String $Email = mySQL.getString(0, 0);
							//$consulta="SELECT Email, Nick, COUNT(Nick) AS ifExist FROM eom_chars WHERE Nick='"+$Nick+"';";
							$consulta = "SELECT Email FROM eom_chars WHERE Nick='" + $Nick + "';";
							printTesto("   mysql ← " + $consulta);
							mySQL.setResult($consulta);
							if (mySQL.getCountRows() == 0) {
								//String $SenhaAuth = StringClass.md5("authorize:"+StringClass.escape($Email)+"&"+StringClass.getAgora());
								String $IPs = "0x" + getUserAddress(ws).split(":")[0];
								String $update =
									   "INSERT INTO eom_chars ("
									   + "Email, Nick, Locked, Locker, LockReason, Created, LastLogin, WebSocket, IPs, PowerGM, "
									   + "Race, Class, HairType, HairColor, Eyes, Nose, Lips, Mark, "
									   + "PositionNow, PositionSave, Level, Experience, HP, MP, availpoints, "
									   + "strength, agility, vitality, intelligence, dexterity, lucky, Inventory, Equipped"
									   + ") VALUES ("
									   + "'" + $Email + "', '" + $Nick + "', NULL, NULL, NULL, '" + StringClass.getAgora() + "', NULL, NULL, '" + $IPs + "', '0', "
									   + "'" + $Race + "', '" + $Class + "', '" + $HairType + "', '" + $HairColor + "', '" + $Eye + "', '" + $Nose + "', '" + $Lip + "', '" + $Scar + "', "
									   + "'0:0,0:S:S', '0:0,0:S:S', '1', '0', '30/30', '30/30', '0', "
									   + "'1', '1', '1', '0', '1', '0', '3:1,12:1,13:1', '12,13'"
									   + ");";
								printTesto("   mysql ← " + $update);
								mySQL.setUpdate($update);
								sendListChars(ws, $Email);
								sendMessage(ws, "newchar?1&" + StringClass.getEscape("<font color='#FFFF00'><b>Avatar criando com sucesso!</b><font/>!"));
							} else {
								sendMessage(ws, "newchar?0&" + StringClass.getEscape("Não é possível usar o apelido de outro avatar!"));
							}
						} else {
							sendMessage(ws, "newchar?0&" + StringClass.getEscape("Somente usuários logados podem adicionar novos personagens!"));
						}
					} else {
						sendMessage(ws, "newchar?0&" + StringClass.getEscape("Erro de protocolo 'register' no servidor!"));
					}
				} else {
					sendMessage(ws, "newchar?0&" + StringClass.getEscape("Este apelido possui caracteres inválidos!"));
				}
			} else {
				sendMessage(ws, "newchar?0&" + StringClass.getEscape("O apelido mínimo de 4 caracteres!"));
			}
			return true;
		}
		return false;
	}
	public boolean onCharDelete(WebSocket ws, String protocolo) {
		if (protocolo.toLowerCase().startsWith("delchar?")) {
			String $consulta = "";
			String $Nick = StringClass.getUnescape(protocolo.split("\\?")[1]);
			if ($Nick.length() >= 0) {
				$consulta = "SELECT Email FROM eom_users WHERE WebSocket='" + filtrarPorta(ws) + "';";
				printTesto("   mysql ← " + $consulta);
				mySQL.setResult($consulta);
				if (mySQL.getCountRows() >= 1) {//Verifica se está logado quem enviou a requisição!
					String $Email = mySQL.getString(0, 0);
					String $update = "DELETE FROM eom_chars WHERE Email='" + $Email + "' AND Nick='" + $Nick + "';";
					printTesto("   mysql ← " + $update);
					mySQL.setUpdate($update);
					sendListChars(ws, $Email);
					sendMessage(ws, "delchar?1&" + StringClass.getEscape("<font color='#FFFF00'><b>Avatar apagado com sucesso!</b><font/>!"));
				} else {
					sendMessage(ws, "delchar?0&" + StringClass.getEscape("Somente usuários logados podem apagar avatares!"));
				}
			} else {
				sendMessage(ws, "delchar?0&" + StringClass.getEscape("CLIENT.ERRO: Não foi informado o nome do avatar que será apagado!"));
			}
			return true;
		}
		return false;
	}
	public boolean onCharSelect(WebSocket ws, String protocolo) {
		if (protocolo.toLowerCase().startsWith("selchar?")) {
			String $consulta = "";
			String $Nick = StringClass.getUnescape(protocolo.split("\\?")[1]);
			if ($Nick.length() >= 0) {
				$consulta = "SELECT Email FROM eom_users WHERE WebSocket='" + filtrarPorta(ws) + "';";
				printTesto("   mysql ← " + $consulta);
				mySQL.setResult($consulta);
				if (mySQL.getCountRows() >= 1) {//Verifica se está logado quem enviou a requisição!
					String $Email = mySQL.getString(0, 0);
					String $update = "UPDATE eom_chars SET WebSocket=NULL WHERE Email='" + $Email + "';";
					printTesto("   mysql ← " + $update);
					mySQL.getUpdate($update);
					String $result = "SELECT "
						   + "IPs, PowerGM, PositionNow, Level, Experience, HP, MP, Inventory, Equipped "
						   + "FROM eom_chars WHERE Email='" + $Email + "' AND Nick='" + $Nick + "';";
					printTesto("   mysql ← " + $result);
					mySQL.setResult($result);
					if (mySQL.getCountRows() >= 1) {
						String $IPs = mySQL.getString(0, 0);
						Integer $PowerGM = mySQL.getInteger(0, 1);
						String $PositionNow = mySQL.getString(0, 2);
						Integer $Level = mySQL.getInteger(0, 3);
						Integer $Experience = mySQL.getInteger(0, 4);
						String $HP = mySQL.getString(0, 5);
						String $MP = mySQL.getString(0, 6);
						String $Inventory = mySQL.getString(0, 7);
						String $Equipped = mySQL.getString(0, 8);


						String $NovaListaIPs = addIPtoList(ws, $IPs);
						$update = "UPDATE eom_chars SET WebSocket='" + filtrarPorta(ws) + "', LastLogin='" + StringClass.getAgora() + "', IPs='" + $NovaListaIPs + "' WHERE Email='" + $Email + "' AND Nick='" + $Nick + "';";
						printTesto("   mysql ← " + $update);
						mySQL.setUpdate($update);
						sendListChars(ws, $Email);
						sendMessage(ws,
							   "selchar?1&"
							   + StringClass.getEscape($Nick) + "&"
							   + $PowerGM + "&"
							   + $PositionNow + "&"
							   + $Level + "&"
							   + $Experience + "&"
							   + $HP + "&"
							   + $MP + "&"
							   + $Inventory + "&"
							   + $Equipped);
					} else {
						sendMessage(ws, "selchar?0&" + StringClass.getEscape("Avatar \"" + $Nick + "\" não encontrado no banco de dados!"));
					}
				} else {
					sendMessage(ws, "selchar?0&" + StringClass.getEscape("Somente usuários logados podem apagar avatares!"));
				}
			} else {
				sendMessage(ws, "selchar?0&" + StringClass.getEscape("CLIENT.ERRO: Não foi informado o nome do avatar que será apagado!"));
			}
			return true;
		}
		return false;
	}
//#############################################################################
	public static void main(String[] args) throws Exception {
		boolean modeGrafic = false;
		String $xmlAddress = "";
		int $portWebSocket = 8888;
		boolean $sePrintTesto = false;
		String $sqlServer = "localhost";
		String $sqlUser = "root";
		String $sqlPass = "";
		String $sqlDB = "eomserver";
		String $hostSMTP = "";
		String $portSMTP = "";
		String $fromName = "";
		String $fromEmail = "";
		String $fromPassWord = "";
		boolean $enableTTLs = false;
		for (int a = 0; a < args.length; a++) {
			if (args[a].toLowerCase().equals("-pl") || args[a].toLowerCase().equals("--plogs") || args[a].toLowerCase().equals("--printlogs")) {
				$sePrintTesto = true;
			} else if ((args[a].toLowerCase().equals("-xs") || args[a].toLowerCase().equals("--xserver") || args[a].toLowerCase().equals("--xmlserver")) && !args[a + 1].isEmpty() && !args[a + 1].equals("")) {
				$xmlAddress = args[a + 1].toString();
			} else if ((args[a].toLowerCase().equals("-wp") || args[a].toLowerCase().equals("--wport") || args[a].toLowerCase().equals("--websocketport")) && !args[a + 1].isEmpty() && Integer.parseInt(args[a + 1]) > 0) {
				try {
					$portWebSocket = Integer.parseInt(args[a + 1].toString());
				} catch (Exception ex) { /**/ }
			} else if ((args[a].toLowerCase().equals("-ss") || args[a].toLowerCase().equals("--sqlserver")) && !args[a + 1].isEmpty() && !args[a + 1].equals("")) {
				$sqlServer = args[a + 1].toString();
			} else if ((args[a].toLowerCase().equals("-su") || args[a].toLowerCase().equals("--sqluser")) && !args[a + 1].isEmpty() && !args[a + 1].equals("")) {
				$sqlUser = args[a + 1].toString();
			} else if ((args[a].toLowerCase().equals("-sp") || args[a].toLowerCase().equals("--sqlpass") || args[a].toLowerCase().equals("--sqlpassword")) && !args[a + 1].isEmpty() && !args[a + 1].equals("")) {
				$sqlPass = args[a + 1].toString();
			} else if ((args[a].toLowerCase().equals("-sd") || args[a].toLowerCase().equals("--sqldb") || args[a].toLowerCase().equals("--sqldatabase")) && !args[a + 1].isEmpty() && !args[a + 1].equals("")) {
				$sqlDB = args[a + 1].toString();
			} else if (args[a].toLowerCase().equals("-sm") || args[a].toLowerCase().equals("--sqlmt") || args[a].toLowerCase().equals("--sqlmakertables")) {
				if (!$sqlServer.equals("")) {
					if (!$sqlUser.equals("")) {
						if (!$sqlPass.equals("")) {
							if (!$sqlDB.equals("")) {
								mySQL mySQL = null;
								try {
									mySQL = new mySQL($sqlServer, $sqlUser, $sqlPass, $sqlDB);
									System.out.println("");
									System.out.println("SERVER: Criando tabelas do banco de dados '" + $sqlDB + "'...");
									String $consulta[] = new String[4];
									$consulta[0] = "DROP TABLE IF EXISTS eom_users;";
									$consulta[1] = "CREATE TABLE IF NOT EXISTS eom_users (  Email varchar(50) NOT NULL,  Senha varchar(50) NOT NULL,  SenhaAuth varchar(50) DEFAULT NULL,  Sex enum('M','F') NOT NULL DEFAULT 'M',  Created datetime NOT NULL DEFAULT '0000-00-00 00:00:00',  LastLogin datetime DEFAULT NULL,  WebSocket varchar(7) DEFAULT NULL,  Locked datetime DEFAULT NULL COMMENT 'Até quando estará bloqueado.',  Locker varchar(50) DEFAULT NULL COMMENT 'GM responsável pelo bloqueio.',  LockReason longtext COMMENT 'Especifica o motivo da conta bloqueada.',  IPs longtext NOT NULL,  PRIMARY KEY (Email)) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
									$consulta[2] = "DROP TABLE IF EXISTS eom_chars;";
									$consulta[3] = "CREATE TABLE IF NOT EXISTS eom_chars (  Email varchar(50) NOT NULL,  Nick varchar(50) NOT NULL,  Locked datetime DEFAULT NULL COMMENT 'Até quando estará bloqueado.',  Locker varchar(50) DEFAULT NULL COMMENT 'GM responsável pelo bloqueio.',  LockReason longtext COMMENT 'Especifica o motivo do Char bloqueado.',  Created datetime NOT NULL DEFAULT '0000-00-00 00:00:00',  LastLogin datetime DEFAULT NULL,  WebSocket varchar(7) DEFAULT NULL,  IPs longtext NOT NULL COMMENT 'Serve para saber qual o char mais usado pelo jogador.',  PowerGM tinyint(2) NOT NULL DEFAULT '0' COMMENT 'Permite que o char possua comandos extras de comando ao servidor. (0~99)',  Race tinyint(2) NOT NULL DEFAULT '0',  Class tinyint(2) NOT NULL DEFAULT '0',  HairType tinyint(2) NOT NULL DEFAULT '0',  HairColor tinyint(2) NOT NULL DEFAULT '0',  Eyes tinyint(2) NOT NULL DEFAULT '0',  Nose tinyint(2) NOT NULL DEFAULT '0',  Lips tinyint(2) NOT NULL DEFAULT '0',  Mark tinyint(2) NOT NULL DEFAULT '0' COMMENT 'Pode ser um cicatriz ou uma tatuagem.',  PositionNow varchar(15) NOT NULL DEFAULT '0:0,0:S:S' COMMENT 'Posição atual do Char. (<mapa>:<TileX>,<TileY>:<Norte/Sul/Leste/Oeste>:<Parado,Atacando,Sentado,Morto>)',  PositionSave varchar(15) NOT NULL DEFAULT '0:0,0:S:S' COMMENT 'Posição de Ressurreição do Char. (<mapa>:<TileX>,<TileY>:<Norte/Sul/Leste/Oeste>:<Parado,Atacando,Sentado,Morto>)',  `Level` varchar(3) NOT NULL DEFAULT '1',  Experience bigint(9) NOT NULL DEFAULT '0',  HP varchar(15) NOT NULL DEFAULT '60/60' COMMENT 'Sintaxe: <AtualHP>,<MaxHP>',  MP varchar(15) NOT NULL DEFAULT '0/0' COMMENT 'Sintaxe: <AtualMP>,<MaxMP>',  availpoints int(5) NOT NULL DEFAULT '0' COMMENT 'Pontos disponíveis para distribuir nos atributos',  strength tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Dado de ataque',  agility tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Defesa de Esquiva',  vitality tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Tamanho do HP',  intelligence tinyint(2) NOT NULL DEFAULT '0' COMMENT 'Tamanho do MP',  dexterity tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Probabilidade de Acerto de Golpe',  lucky tinyint(2) NOT NULL DEFAULT '0' COMMENT 'Probabilidade de Drop Item',  Inventory longtext COMMENT 'São os itens que o jogador possui. Sintaxe → <ID1>:<Quantia1>,<ID2>:<Quantia2>,...,<IDX>:<QuantiaX>',  Equipped longtext COMMENT 'São os itens que o jogador esta equipado. Sintaxe → <ID1>,<ID2>,...,<IDX>',  PRIMARY KEY (Email,Nick)) ENGINE=MyISAM DEFAULT CHARSET=utf8;";
									$consulta[4] = "DROP TABLE IF EXISTS eom_badwords;";
									$consulta[5] = "CREATE TABLE IF NOT EXISTS eom_badwords (  BadWord varchar(30) NOT NULL COMMENT 'Qual o palavrão que será bloqueado.',  TimePenalty datetime NOT NULL DEFAULT '0000-00-00 00:30:00' COMMENT 'Qual a penalidade de tempo que o char ou conta sofrerá ao proferir tais palavras.',  TypePenalty enum('P','C') NOT NULL DEFAULT 'C' COMMENT 'Define o tipo de bloqueio que a palavra proferia dará. Sintaxe: P=Player/Conta, C=Char/Personagem',  PenalyReason longtext NOT NULL COMMENT 'É a mensagem que aparecerá na tela do jogador bloquedo e globalmente.',  PRIMARY KEY (BadWord)) ENGINE=MyISAM DEFAULT CHARSET=utf8;";

									for (int $c = 0; $c < $consulta.length; $c++) {
										System.out.println("   → " + $consulta[$c]);
										mySQL.setUpdate($consulta[$c]);
									}
									System.out.println("SERVER: Tabelas criadas com sucesso no mysql!");
									System.out.println("");
									System.exit(0); // ← 0 = (Sair sem erro)  1 = (Sair com Erro)
								} catch (ClassNotFoundException ex) {
									Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
								} catch (SQLException ex) {
									Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
								} catch (Exception ex) {
									Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
								}
							} else {
								System.out.println("Favor digite o banco de dados mysql! (Exemplo: --sqldatabase <eomserver>)");
							}
						} else {
							System.out.println("Favor digite a senha mysql! (Exemplo: --sqlpassword <Senha>)");
						}
					} else {
						System.out.println("Favor digite o usuário mysql! (Exemplo: --sqluser <Usuário>)");
					}
				} else {
					System.out.println("Favor digite o servidor mysql! (Exemplo: --sqlserver 127.0.0.1)");
				}
				//System.exit(0); // ← 0 = (Sair sem erro)  1 = (Sair com Erro)
			} else if ((args[a].toLowerCase().equals("-ms") || args[a].toLowerCase().equals("--msmtp") || args[a].toLowerCase().equals("--mailsmtp")) && !args[a + 1].isEmpty() && !args[a + 1].equals("")) {
				$hostSMTP = args[a + 1].toString();
			} else if ((args[a].toLowerCase().equals("-mp") || args[a].toLowerCase().equals("--mport") || args[a].toLowerCase().equals("--mailport")) && !args[a + 1].isEmpty() && !args[a + 1].equals("")) {
				$portSMTP = args[a + 1].toString();
			} else if ((args[a].toLowerCase().equals("-mn") || args[a].toLowerCase().equals("--mname") || args[a].toLowerCase().equals("--mailname")) && !args[a + 1].isEmpty() && !args[a + 1].equals("")) {
				$fromName = args[a + 1].toString();
			} else if ((args[a].toLowerCase().equals("-ma") || args[a].toLowerCase().equals("--maddress") || args[a].toLowerCase().equals("--mailaddress")) && !args[a + 1].isEmpty() && !args[a + 1].equals("")) {
				$fromEmail = args[a + 1].toString();
			} else if ((args[a].toLowerCase().equals("-mw") || args[a].toLowerCase().equals("--mpass") || args[a].toLowerCase().equals("--mailpass") || args[a].toLowerCase().equals("--mailpassword")) && !args[a + 1].isEmpty() && !args[a + 1].equals("")) {
				$fromPassWord = args[a + 1].toString();
			} else if (args[a].toLowerCase().equals("-mt") || args[a].toLowerCase().equals("--mttl") || args[a].toLowerCase().equals("--mailttl")) {
				$enableTTLs = true;
			} else if (args[a].toLowerCase().equals("-h") || args[a].toLowerCase().equals("--help")) {
				System.out.println("");
				System.out.println("####################################################################################################");
				System.out.println("###   SERVIDOR EMPIRE OF MANA - MMORPG(Free-P2W)                                                 ###");
				System.out.println("####################################################################################################");
				System.out.println("##### COMANDO ######################################################################################");
				System.out.println("###      → java -jar <endereço>/EmpireOfMana.jar <parâmetros> &                                  ###");
				System.out.println("###                                                                                              ###");
				System.out.println("###   Observação: O simbolo '&' demostrado acima servirá para que o terminal continue esperando  ###");
				System.out.println("###               comandos mesmo após executar este servidor.                                    ###");
				System.out.println("####################################################################################################");
				System.out.println("##### PARÂMETROS ###################################################################################");
				System.out.println("###      [--help/-h]                       → Exibe ajudar sobre este pregrama.                   ###");
				System.out.println("###      [--printlogs/-pl]                 → Imprime as comunicações mySQL e SMTP em tempo real. ###");
				System.out.println("##### Com XML ######################################################################################");
				System.out.println("###      [--xmlserver|xs] <endereço>       → Informa ao servidor o endereço do XML com dados de  ###");
				System.out.println("###                                          configuração do servidor.                           ###");
				System.out.println("###      [--sqlpass|-sp] <Senha>           → Informa ao servidor a senha do usuário que acessará ###");
				System.out.println("###                                          o banco de dados mySQL.                             ###");
				System.out.println("###      [--mailpassword|-mw] <Senha>      → Informa ao servidor a senha de email no provedor    ###");
				System.out.println("###                                          de email escolhido.                                 ###");
				System.out.println("##### Sem XML ######################################################################################");
				System.out.println("###      [--websocketport/-wp] <nº>        → Escolhe qual será a porta ocupada pelo servidor.    ###");
				System.out.println("###      [--mailsmtp|-ms] <url>            → Informa ao servidor o endereço de SMTP do provedor  ###");
				System.out.println("###                                          de email escolhido.                                 ###");
				System.out.println("###      [--mailport|-mp] <porta>          → Informa ao servidor a porta de SMTP do provedor de  ###");
				System.out.println("###                                          email escolhido.                                    ###");
				System.out.println("###      [--mailname|-mn] <Nome>           → Informa ao servidor o nome do dono do email no      ###");
				System.out.println("###                                          provedor de email escolhido.                        ###");
				System.out.println("###      [--mailaddress|-ma] <Email>       → Informa ao servidor o endereço de email no provedor ###");
				System.out.println("###                                          de email escolhido.                                 ###");
				System.out.println("###      [--mailpassword|-mw] <Senha>      → Informa ao servidor a senha de email no provedor    ###");
				System.out.println("###                                          de email escolhido.                                 ###");
				System.out.println("###      [--mailtest] <Email>              → Envia um email de teste deste programa para um      ###");
				System.out.println("###                                          Destinatário escolhido.                             ###");
				System.out.println("###----------------------------------------------------------------------------------------------###");
				System.out.println("###      [--sqlserver|-ss] <url>           → Informa ao servidor o endereço onde está armazenado ###");
				System.out.println("###                                          o banco de dados mySQL.                             ###");
				System.out.println("###      [--sqluser|-su] <UserName>        → Informa ao servidor o nome do usuário que acessará  ###");
				System.out.println("###                                          o banco de dados mySQL.                             ###");
				System.out.println("###      [--sqlpass|-sp] <Senha>           → Informa ao servidor a senha do usuário que acessará ###");
				System.out.println("###                                          o banco de dados mySQL.                             ###");
				System.out.println("###      [--sqldb|-sd] <Banco>             → Informa ao servidor o nome do banco de dados mySQL  ###");
				System.out.println("###                                          que contém as tabelas do EOM.                       ###");
				System.out.println("###      [--sqlmakertables|-sm]            → Cria todas as tabelas necessárias no BD do mySQL.   ###");
				System.out.println("###                                          (CUIDADO: Isso esvazia tablas de mesmo nome)        ###");
				System.out.println("####################################################################################################");
				System.out.println("##### Created by ###################################################################################");
				System.out.println("###     * Vanderson → <vandersonmr2@gmail.com>                                                   ###");
				System.out.println("###     * Lunovox → <rui.gravata@hotmail.com> / <rui.gravata@hotmail.com>                        ###");
				System.out.println("####################################################################################################");
				System.out.println("");
				System.exit(0); // ← 0 = (Sair sem erro)  1 = (Sair com Erro)
			}
		}
		Principal ws = null;

		Element $ArqXML = FileClass.arquivoAbrirXML($xmlAddress);
		if ($ArqXML != null) {
			NodeList $noWebSocket = $ArqXML.getElementsByTagName("websocket");
			if ($noWebSocket.getLength() >= 1) {
				Element $tagWebSocket = (Element) $noWebSocket.item(0);
				if ($tagWebSocket.getNodeType() == Node.ELEMENT_NODE) {
					$portWebSocket = FileClass.getAtributo($tagWebSocket, "port", $portWebSocket);
					$sePrintTesto = !FileClass.getAtributo($tagWebSocket, "silentmode", $sePrintTesto);
					if (!$sePrintTesto) {
						System.out.println("Servidor '" + $xmlAddress + "' rodando no modo silencioso!");
					}
					//System.out.println("WebSocketPort: "+$portWebSocket);
				}
			}

			NodeList $noMySQL = $ArqXML.getElementsByTagName("mysql");
			if ($noMySQL.getLength() >= 1) {
				Element $tagMySQL = (Element) $noMySQL.item(0);
				if ($tagMySQL.getNodeType() == Node.ELEMENT_NODE) {
					$sqlServer = FileClass.getAtributo($tagMySQL, "host", $sqlServer);
					$sqlUser = FileClass.getAtributo($tagMySQL, "user", $sqlUser);
					$sqlDB = FileClass.getAtributo($tagMySQL, "database", $sqlDB);
					if ($sqlPass.equals("")) {
						Endecrypt endecrypt = new Endecrypt($sqlUser + "@" + $sqlServer + "/" + $sqlDB);
						$sqlPass = endecrypt.decriptar(FileClass.getAtributo($tagMySQL, "password", $sqlPass));
						//System.out.println("$sqlPass: "+$sqlPass);
						//System.out.println("$sqlPass.decriptar: " + endecrypt.decriptar($sqlPass));
					}
				}
			}

			NodeList $noMail = $ArqXML.getElementsByTagName("mail");
			if ($noMail.getLength() >= 1) {
				Element $tagMail = (Element) $noMail.item(0);
				if ($tagMail.getNodeType() == Node.ELEMENT_NODE) {
					$hostSMTP = FileClass.getAtributo($tagMail, "smtp", $hostSMTP);
					$portSMTP = FileClass.getAtributo($tagMail, "port", $portSMTP);
					$fromName = FileClass.getAtributo($tagMail, "name", $fromName);
					$fromEmail = FileClass.getAtributo($tagMail, "address", $fromEmail);
					$enableTTLs = FileClass.getAtributo($tagMail, "ttl", $enableTTLs);
					if ($fromPassWord.equals("")) {
						Endecrypt endecrypt = new Endecrypt($fromEmail + ":" + $portSMTP + "/" + $hostSMTP);
						$fromPassWord = endecrypt.decriptar(FileClass.getAtributo($tagMail, "password", $fromPassWord));
						//System.out.println("$fromPassWord: "+$fromPassWord);
						//System.out.println("$fromPassWord.decriptar: "+endecrypt.decriptar($fromPassWord));
					}
				}
			}
			if ($portWebSocket > 0) {
				if (!$sqlServer.equals("")) {
					if (!$sqlUser.equals("")) {
						if (!$sqlPass.equals("")) {
							if (!$sqlDB.equals("")) {
								if (!$hostSMTP.equals("")) {
									if (!$portSMTP.equals("")) {
										if (!$fromName.equals("")) {
											if (!$fromEmail.equals("")) {
												if (!$fromPassWord.equals("")) {
													if ($enableTTLs == true) {
														ws = new Principal(
															   $portWebSocket, $sePrintTesto,
															   $sqlServer, $sqlUser, $sqlPass, $sqlDB,
															   $hostSMTP, $portSMTP, $fromName, $fromEmail, $fromPassWord, $enableTTLs);
														ws.start();
													} else {
														System.out.println("Digite se seu servidor de email escolhido utilizar TTLs! [Exemplo: -mt <true/false>]");
													}
												} else {
													System.out.println("Digite a senha de email do seu servidor escolhido! [Exemplo: -mw <senha>]");
												}
											} else {
												System.out.println("Digite o seu endereço de email do seu servidor escolhido! [Exemplo: -ma <email>]");
											}
										} else {
											System.out.println("Digite o nome do seu servidor! [Exemplo: -mn \"Empire of Mana - <MeuNome>\"]");
										}
									} else {
										System.out.println("Digite a sua servidort de SMTP! [Exemplo para o Google: -mp 465]");
									}
								} else {
									System.out.println("Digite o seu servidor de SMTP! [Exemplo para o Google: -ms smtp.gmail.com]");
								}
							} else {
								System.out.println("Digite o Banco de Dados de usuário MySQL! [Exemplo: -sd eomserver]");
							}
						} else {
							System.out.println("Digite a senha de usuário MySQL! [Exemplo: -sp <SuaSenha>]");
						}
					} else {
						System.out.println("Digite o usuário de MySQL! [Exemplo: -su root]");
					}
				} else {
					System.out.println("Digite o servidor de MySQL! [Exemplo: -ss 127.0.0.1]");
				}
			} else {
				System.out.println("Digite a porta do websocket! [Exemplo: -wp 8787]");
			}
		} else {
			System.out.println("Não foi possivel abrir: " + $xmlAddress);
		}

		if (ws == null) {
			System.exit(1); // ← 0 = (Sair sem erro)  1 = (Sair com Erro)
		}
	}/**/
}
