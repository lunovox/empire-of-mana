function toTrim(str){ //trim completo
	return str.replace(/^\s+|\s+$/g,"");
}
function toLTrim(str){ //left trim
	return str.replace(/^\s+/,"");
}
function toRTrim(str) { //right trim
	return str.replace(/\s+$/,"");
}
function ifFormatMail($testo){
	var result=false;
	var filter=/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
	if(filter.test($testo)){result = true;}
	return result;
}