function onBtnCharListSelectChar($index){
	var divCharListTesto = document.getElementById("divCharListTesto");
	var btnCharListChangePassWord = document.getElementById("btnCharListChangePassWord");
	var btnCharListCancel = document.getElementById("btnCharListCancel");
	var btnCharListSelectChar = document.getElementsByName("btnCharListSelectChar");
	var btnCharListDeleteChar = document.getElementsByName("btnCharListDeleteChar");
	var btnCharListMakerChar = document.getElementsByName("btnCharListMakerChar");
	btnCharListChangePassWord.disabled=true;
	btnCharListCancel.disabled=true;
	for($i=0;$i<btnCharListMakerChar.length;$i++){
		btnCharListSelectChar[$i].disabled=true;
		btnCharListDeleteChar[$i].disabled=true;
		btnCharListMakerChar[$i].disabled=true;
	}
	showAviso(divCharListTesto,"Carregando Mapa! Espere um momento...");
	if(document.body.style!=undefined) document.body.style.cursor='wait';
	wSocket.send("selchar?"+escape(usersXML.getCharByIndex($index).nick));/**/
}
function onBtnCharListMakerChar(){
	drawCharMakerSelects();
	showForm('frmCharMaker');
}
function onBtnCharListChangePassWord(){
	showForm('frmChangePassWord');
}
function btnCharListCancel(){
	if(document.body.style!=undefined) document.body.style.cursor='wait';
	document.getElementById("btnCharListCancel").disabled=true;/**/
	wSocket.send("logout?me");
	//document.body.style.cursor='default';
	//showForm('frmLogin');
	//#### TESTE POR PING ########################################################
	/*wSocket.send("modoPing?on");
	wSocket.send("ifexist?user&0&0");
	wSocket.send("logout?you");
	wSocket.send("modoPing?off");
	//############################################################################/**/

}
function showChars($Avatars){
	var cnvCharListAvatar = document.getElementsByName("cnvCharListAvatar");
	var divCharListSelDel = document.getElementsByName("divCharListSelDel");
	var divCharListDescrition = document.getElementsByName("divCharListDescrition");
	var btnCharListMakerChar = document.getElementsByName("btnCharListMakerChar");
	var btnCharListSelectChar = document.getElementsByName("btnCharListSelectChar");
	var btnCharListDeleteChar = document.getElementsByName("btnCharListDeleteChar");
	//alert(cnvCharListAvatar.length);
	usersXML.delAllChars();
	for($c=0;$c<cnvCharListAvatar.length;$c++){
		$cnvAvatar = cnvCharListAvatar[$c];
		$ctxAvatar = $cnvAvatar.getContext('2d');
		$divSelDel = divCharListSelDel[$c];
		$divDesc = divCharListDescrition[$c];
		$btnMakerChar = btnCharListMakerChar[$c];
		$btnSelectChar = btnCharListSelectChar[$c];
		$btnDeleteChar = btnCharListDeleteChar[$c];
		//alert($c+"/"+$myCont);
		//alert($c+":"+$ctxAvatar);
		//$myAvatars=$Avatars.split(";");
		if($Avatars!="" && $c<$Avatars.length){
			$Avatar=$Avatars[$c];
			//alert($Avatar);
			$atrib = $Avatar.split(",");
			usersXML.addChar(
				$atrib[0],
				$atrib[1],
				$atrib[1],
				$atrib[2],
				$atrib[3],
				$atrib[4],
				$atrib[5],
				$atrib[6],
				$atrib[7],
				$atrib[8],
				$atrib[9],
				$atrib[10]
			);
			drawAvatar(
				$cnvAvatar,
				$ctxAvatar,
				$atrib[1],
				$atrib[2],
				$atrib[3],
				$atrib[4],
				$atrib[5],
				$atrib[6],
				$atrib[7],
				$atrib[8],
				$atrib[9]
			);/**/
			showAviso($divDesc,
				"<center><font size='2'><b>"+$atrib[0]+"</b></font></center>"+
				"<left><font size='1'>"+
				"<b>Raça:</b> "+usersXML.getAvatars().races.race[$atrib[2]].name+"<br/>"+
				"<b>Classe:</b> "+usersXML.getClasses().class[$atrib[3]].name+"<br/>"+
				"<b>Nível:</b> "+$atrib[10]+
				"</left>"
			);/**/
			$divSelDel.style.display="block";
			$btnMakerChar.style.display="none";
			//$btnMakerChar.disabled=false;
			//$btnSelectChar.disabled=true;
			//$btnDeleteChar.disabled=true;
		}else{
			$ctxAvatar.save();
			$ctxAvatar.clearRect(0, 0, $cnvAvatar.width, $cnvAvatar.height);
			$ctxAvatar.restore();
			showAviso($divDesc,"");
			$divSelDel.style.display="none";
			$btnMakerChar.style.display="block";
			//$btnSelectChar.disabled=true;
			//$btnDeleteChar.disabled=true;
		}
	}
}
function onBtnCharListDeleteChar($index){
	if(confirm("Deseja realmente apagar '"+usersXML.getCharByIndex($index).nick+"'?")){
		var divCharListTesto = document.getElementById("divCharListTesto");
		var btnCharListChangePassWord = document.getElementById("btnCharListChangePassWord");
		var btnCharListCancel = document.getElementById("btnCharListCancel");
		var btnCharListSelectChar = document.getElementsByName("btnCharListSelectChar");
		var btnCharListDeleteChar = document.getElementsByName("btnCharListDeleteChar");
		var btnCharListMakerChar = document.getElementsByName("btnCharListMakerChar");
		btnCharListChangePassWord.disabled=true;
		btnCharListCancel.disabled=true;
		for($i=0;$i<btnCharListMakerChar.length;$i++){
			btnCharListSelectChar[$i].disabled=true;
			btnCharListDeleteChar[$i].disabled=true;
			btnCharListMakerChar[$i].disabled=true;
		}
		showAviso(divCharListTesto,"Apagando avatar! Espere um momento...");
		if(document.body.style!=undefined) document.body.style.cursor='wait';
		wSocket.send("delchar?"+escape(usersXML.getCharByIndex($index).nick));/**/
	}
}