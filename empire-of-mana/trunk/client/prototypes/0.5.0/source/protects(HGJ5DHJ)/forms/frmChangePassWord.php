<table id="frmChangePassWord" class="painel" style="left:80px; top:160px;">
	<tr>
		<td colspan="2" class="titleForms"><b>TROCANDO SENHA</b></td>
	</tr>
	<tr>
		<td align="center" colspan="2">
			<div id="divChangePassWord" class="aviso"></div>
		</td>
	</tr>
	<tr>
		<td align="right"><b>Senha Atual:</b></td>
		<td><input id="txtChangePassWordSenhaAntiga" type="password" maxlength="20" style="width:130px" /></td>
	</tr>
	<tr>
		<td align="right"><b>Senha Nova:</b></td>
		<td><input id="txtChangePassWordSenha" type="password" maxlength="20" style="width:130px" /></td>
	</tr>
	<tr>
		<td align="right"><b>Repetição:</b></td>
		<td><input id="txtChangePassWordSenhaConfirmada" type="password" maxlength="20" style="width:130px" /></td>
	</tr>
	<tr>
		<td colspan="2" align="center">
			<button id="btnChangePassWordDone"   onclick="onBtnChangePassWordDone();">Trocar</button>
			<button id="btnChangePassWordCancel" onclick="onBtnChangePassWordCancel();">Cancelar</button><br>
		</td>
	</tr>
</table>