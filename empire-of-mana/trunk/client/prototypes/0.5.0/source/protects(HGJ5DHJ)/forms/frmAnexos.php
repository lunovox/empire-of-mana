<table id="frmAnexos" class="painel" style="left:60px; top:100px;">
	<tr>
		<td class="titleForms"><b>ANEXAR O EMPIRE OF MANA</b></td>
	</tr>
	<tr>
		<td align="center">
			<div id="divAnexosTesto" class="aviso"></div>
		</td>
	</tr>
	<tr>
		<td align="left">
			<fieldset>
				<legend><b>Por Iframe ao seu Site</b></legend>
				<div id="dibAnexoSiteEndereco">
					Digite o endereço de seu site:<br>
					<input id="txtAnexosSiteEndereco" type="text" class="codigo" style="width:380px; height:23px;">
					<button style="width:70;" onclick="onBtnAnexosGerarIframe();"><img src="images/icons/icon-code-16x16.png" align="top"> Gerar</button>
				</div>
				<div id="dibAnexoSiteCodigo" style="display:none;">
					Copie o código abaixo e cole em seu site:<br>
					<textarea id="txtAnexosSiteCodigo" 
						readonly="true" rows="6" 
						class="codigo" style="width:450"
						onclick="SelectAllByObject(this)"
					></textarea>
				</div>
			</fieldset>
		</td>
	</tr>
	<tr>
		<td align="center">
			<button id="btnAnexosBack" style="border-radius:3; opacity:1.0;" onclick="onBtnAnexosBack();">Voltar</button><br>
		</td>
	</tr>
</table>
