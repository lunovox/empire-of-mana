/**
 * @autor Lunovox
 * Created on 03/11/2011, 20:33:38
 */

package Formularios;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.tootallnate.websocket.WebSocket;

public class Main extends javax.swing.JFrame {
	public Main() {initComponents();}
	WebSocketTester ws=null;

	@SuppressWarnings("unchecked")
     // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
     private void initComponents() {

          jScrollPane1 = new javax.swing.JScrollPane();
          txtStatus = new javax.swing.JTextArea();
          jLabel1 = new javax.swing.JLabel();
          txtPorta = new javax.swing.JTextField();
          tbnConexao = new javax.swing.JButton();

          setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
          setTitle("WebSocket Tester");

          txtStatus.setBackground(new java.awt.Color(1, 1, 1));
          txtStatus.setColumns(20);
          txtStatus.setFont(new java.awt.Font("Monospaced", 0, 15)); // NOI18N
          txtStatus.setForeground(new java.awt.Color(254, 254, 254));
          txtStatus.setRows(5);
          jScrollPane1.setViewportView(txtStatus);

          jLabel1.setText("Porta:");

          txtPorta.setText("8787");

          tbnConexao.setText("Conectar");
          tbnConexao.addActionListener(new java.awt.event.ActionListener() {
               public void actionPerformed(java.awt.event.ActionEvent evt) {
                    tbnConexaoActionPerformed(evt);
               }
          });

          javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
          getContentPane().setLayout(layout);
          layout.setHorizontalGroup(
               layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                         .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 662, Short.MAX_VALUE)
                         .addGroup(layout.createSequentialGroup()
                              .addComponent(jLabel1)
                              .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                              .addComponent(txtPorta, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                              .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                              .addComponent(tbnConexao)))
                    .addContainerGap())
          );
          layout.setVerticalGroup(
               layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
               .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                         .addComponent(jLabel1)
                         .addComponent(txtPorta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                         .addComponent(tbnConexao))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 204, Short.MAX_VALUE)
                    .addContainerGap())
          );

          pack();
     }// </editor-fold>//GEN-END:initComponents

	 private void tbnConexaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tbnConexaoActionPerformed
		 if(tbnConexao.getText().endsWith("Conectar")){
			try{
				int $Port =Integer.parseInt(txtPorta.getText());
				txtPorta.setText(String.valueOf($Port));
				txtPorta.setEnabled(false);
				ws = new WebSocketTester(Integer.parseInt(txtPorta.getText()),txtStatus);
				ws.printTest("Esperando Conexão de cliente pelo 'ws://localhost:"+txtPorta.getText()+"'!");
				ws.start();
				tbnConexao.setText("Desconectar");
			}catch(NumberFormatException ex){

			}
		}else{
			try {
				for(int $c=0;$c<ws.connections().length-1;$c++){ws.connections()[$c].close();}
				ws.stop();
				ws.printTest("Conexão com cliente fechada!");
				ws=null;
			} catch (IOException ex) {
				ws.printTest("ERRO: "+ex.getMessage());
				Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
			}
			txtPorta.setEnabled(true);
			tbnConexao.setText("Conectar");
		}
	 }//GEN-LAST:event_tbnConexaoActionPerformed
	public static void main(String args[]) {
		 java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				new Main().setVisible(true);
			}
		});
	}

     // Variables declaration - do not modify//GEN-BEGIN:variables
     private javax.swing.JLabel jLabel1;
     private javax.swing.JScrollPane jScrollPane1;
     private javax.swing.JButton tbnConexao;
     private javax.swing.JTextField txtPorta;
     private javax.swing.JTextArea txtStatus;
     // End of variables declaration//GEN-END:variables

}
