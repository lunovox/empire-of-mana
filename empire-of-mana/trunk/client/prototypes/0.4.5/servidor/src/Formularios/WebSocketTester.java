/**
 * @author lunovox
 * @description Serve para testarse a conexão WebSocket Está funcionando corretamente
 */

package Formularios;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextArea;
import net.tootallnate.websocket.WebSocket;
import net.tootallnate.websocket.WebSocketServer;

public class WebSocketTester extends WebSocketServer {
	public WebSocketTester(int port, JTextArea obj) {
		super(port);
		txtStatus=obj;
   }
	public WebSocketTester(int port) {
		super(port);
   }
	JTextArea txtStatus=null;
	public void printTest(String $Mensagem) {
		if(txtStatus!=null){
			if(txtStatus.getText().isEmpty()){
				txtStatus.setText($Mensagem);
			}else{
				txtStatus.setText(txtStatus.getText()+"\n"+$Mensagem);
			}
		}else{
			System.out.println($Mensagem);
		}
	}
	@Override public void onClientOpen(WebSocket ws) {
		printTest("onClientOpen(WebSocket ws)");
		try {
			ws.send("Correctado corretamente!");
		} catch (IOException ex) {
			Logger.getLogger(WebSocketTester.class.getName()).log(Level.SEVERE, null, ex);
		}
		//System.exit(0); // ← 0 = (Sair sem erro)  1 = (Sair com Erro)
	}
	@Override public void onClientClose(WebSocket ws) {
		printTest("onClientClose(WebSocket ws)");
		//System.exit(0); // ← 0 = (Sair sem erro)  1 = (Sair com Erro)
	}
	@Override public void onClientMessage(WebSocket ws, String $Mensagem) {
		printTest("onClientMessage(WebSocket ws, String string='"+$Mensagem+"')");
		try {
			ws.send("Mensagem recebida corretamente!");
			//System.exit(0); // ← 0 = (Sair sem erro)  1 = (Sair com Erro)
		} catch (IOException ex) {
			Logger.getLogger(WebSocketTester.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	public static void main(String args[]) {
		int $port=8787;
		WebSocketTester ws = new WebSocketTester($port);
		ws.printTest("Esperando Conexão!");
		ws.start();
	}

	@Override
	public void onIOError(IOException ioe) {
		throw new UnsupportedOperationException("Not supported yet.");
	}
}
