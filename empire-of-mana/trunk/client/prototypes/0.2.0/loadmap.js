function loadXML(url){
  //by Micox: micoxjcg@yahoo.com.br.
    if(window.XMLHttpRequest){
        var Loader = new XMLHttpRequest();
        Loader.open("GET", url ,false);
        Loader.send(null);
        return Loader.responseXML;
    }else if(window.ActiveXObject){
        var Loader = new ActiveXObject("Msxml2.DOMDocument.3.0");
        Loader.async = false;
        Loader.load(url);
        return Loader;
    }
}
function loadMap(url){
    xmlNode = loadXML(url);
    for(var n=0;n<xmlNode.childNodes.length;n++){//percorrendo os filhos do nó
        novoMap=[];
        if(xmlNode.childNodes[n].nodeType == 1){//ignorar espaços em branco
            nodeMap = xmlNode.childNodes[n];
            if(nodeMap.nodeName=="map"){
                novoMap.url=url;
                for(var a=0;a<nodeMap.attributes.length;a++){
                    atrib = nodeMap.attributes[a];
                    if(atrib.nodeName=="version"){
                        novoMap.version=atrib.nodeValue;
                    }else if(atrib.nodeName=="orientation"){
                        novoMap.orientation=atrib.nodeValue;
                    }else if(atrib.nodeName=="width"){
                        novoMap.width=atrib.nodeValue;
                    }else if(atrib.nodeName=="height"){
                        novoMap.height=atrib.nodeValue;
                    }else if(atrib.nodeName=="tilewidth"){
                        novoMap.tilewidth=atrib.nodeValue;
                    }else if(atrib.nodeName=="tileheight"){
                        novoMap.tileheight=atrib.nodeValue;
                    }else if(atrib.nodeName=="tileset"){

                    }
                }

                novoMap.tileset=[];
                novoMap.layer=[];
                
                for(var t=0;t<nodeMap.childNodes.length;t++){
                    nodeProp = nodeMap.childNodes[t];
                    if(nodeProp.nodeType == 1){
                        if(nodeProp.nodeName=="tileset"){
                            contTile=novoMap.tileset.length;
                            novoMap.tileset[contTile]=[];
                            for(var a=0;a<nodeProp.attributes.length;a++){
                                var atrib = nodeProp.attributes[a];
                                
                                if(atrib.nodeName=="firstgid"){
                                    novoMap.tileset[contTile].firstgid=atrib.nodeValue;
                                }else if(atrib.nodeName=="name"){
                                    novoMap.tileset[contTile].name=atrib.nodeValue;
                                }else if(atrib.nodeName=="tilewidth"){
                                    novoMap.tileset[contTile].tilewidth=atrib.nodeValue;
                                }else if(atrib.nodeName=="tilewidth"){
                                    novoMap.tileset[contTile].name=atrib.nodeValue;
                                }else if(nodeProp.childNodes.length>=1){
                                    for(var contImage=0;contImage<nodeProp.childNodes.length;contImage++){
                                        nodeImage = nodeProp.childNodes[contImage];
                                        if(nodeImage.nodeType == 1 && nodeImage.nodeName=="image"){
                                             for(var ai=0;ai<nodeImage.attributes.length;ai++){
                                                atribImage = nodeImage.attributes[ai];
                                                if(atribImage.nodeName=="source"){
                                                    novoMap.tileset[contTile].image=atribImage.nodeValue;
                                                }
                                            }

                                        }
                                    }
                                }
                            }
                        }else if(nodeProp.nodeName=="layer"){
                            contLayer=novoMap.layer.length;
                            novoMap.layer[contLayer]=[];
                            novoMap.layer[contLayer].opacity=1.0;
                            for(var al=0;al<nodeProp.attributes.length;al++){
                                atribLayer = nodeProp.attributes[al];
                                if(atribLayer.nodeName=="name"){
                                    novoMap.layer[contLayer].name=atribLayer.nodeValue;
                                    if(atribLayer.nodeValue="collision") novoMap.layercollision=contLayer;
                                }else if(atribLayer.nodeName=="opacity"){
                                    novoMap.layer[contLayer].opacity=atribLayer.nodeValue;
                                }else if(atribLayer.nodeName=="width"){
                                    novoMap.layer[contLayer].width=atribLayer.nodeValue;
                                }else if(atribLayer.nodeName=="height"){
                                    novoMap.layer[contLayer].height=atribLayer.nodeValue;
                                }
                            }
                            //document.write("novoMap.layer["+(contLayer)+"].name='"+novoMap.layer[contLayer].name+"'<br/>");

                            for(var np=0;np<nodeProp.childNodes.length;np++){
                                nodeData = nodeProp.childNodes[np];
                                if(nodeData.nodeType == 1){
                                    if(nodeData.nodeName=="data"){
                                        //document.write("nodeData.nodeName='"+nodeData.nodeName+"'<br/>");
                                        novoMap.layer[contLayer].data=[];
                                        for(var e=0;e<nodeData.childNodes.length;e++){
                                            nodeTile = nodeData.childNodes[e];
                                            if(nodeTile.nodeType == 1){
                                                if(nodeTile.nodeName=="tile"){
                                                    contData=novoMap.layer[contLayer].data.length;
                                                    for(var nd=0;nd<nodeTile.attributes.length;nd++){
                                                        atribTile=nodeTile.attributes[nd];
                                                        if(atribTile.nodeName=="gid"){
                                                            novoMap.layer[contLayer].data[contData]=atribTile.nodeValue;
                                                        }
                                                    }
                                                    //document.write("novoMap.layer["+(contLayer)+"].data["+contData+"]='"+novoMap.layer[contLayer].data[contData]+"'<br/>");
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    return novoMap;
}

mapa =loadMap("maps/z10.tmx");
document.write(mapa.layer[2].data[0]);

//document.write(10);
