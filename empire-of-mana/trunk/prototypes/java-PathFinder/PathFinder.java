/*
*
* Path Finder - Algoritimo A*
* Vanderson M. do Rosario <vandersonmr2@gmail.com>
* 
* Próximo passo é organizar e comentar o algoritimo.
*
*/
import java.util.HashMap;

public class PathFinder {

	HashMap<Integer, String> ListaFechada = new HashMap<Integer, String>();
	HashMap<String, Integer> ListaAbertaCusto = new HashMap<String, Integer>();
	HashMap<String, String> ListaAbertaPai = new HashMap<String, String>();
	int VezesReiniciado;
	int PMenor[];
	int ValorMenor;
	HashMap<String, String> ListaAbertaPai_UsoTemporario = null;

	public String ProcurarPathFinder(int A[], int[] B, int[][] mapa) {
		int[] uA = A;
		ListaFechada.put(0, IntToString(uA));
		int custo;
		while (true) {
			PMenor = A;
			ValorMenor = 1000000000;

			for (int i = -1; i < 2; i++) {
				for (int j = -1; j < 2; j++) {

					int[] posicao = { uA[0] + i, uA[1] + j };

					if (estaNoMapa(posicao,mapa.length)) {

						if (terrenoPassavel(posicao, mapa)
								&& !ListaFechada
										.containsValue(IntToString(posicao))) {

							if (!(posicao[0] == uA[0] && posicao[1] == uA[1])) {

								custo = CalcCusto(posicao, B, A);
								if (ListaAbertaCusto
										.containsKey(IntToString(posicao))) {

									if (Integer
											.valueOf((ListaAbertaCusto
													.get(IntToString(posicao)))) > custo) {
										AdicionarListaAberta(posicao, custo, uA);
									}
								} else {
									AdicionarListaAberta(posicao, custo, uA);
								}

								if ((custo < ValorMenor)) {
									ValorMenor = custo;
									PMenor = posicao;
								}

							}
						}
					}
				}
			}

			if (!ListaFechada.containsValue(IntToString(PMenor))) {
				ListaFechada.put(ListaFechada.size() + 1, IntToString(PMenor));
			}
			uA = PMenor;

			if (PMenor[0] == B[0] && PMenor[1] == B[1])
				break;

			if (IntToString(uA).equals(IntToString(A))) {
				VezesReiniciado += 1;
				if (VezesReiniciado > 9) {

					if (ChecarPosicoesRestantes() == null) {
						break;
					} else {
						try {
							uA = StringToInt((String) ListaAbertaPai_UsoTemporario
									.values().toArray()[0]);
						} catch (Exception e) {
							break;
						}
					}
					VezesReiniciado = 0;

				}
			}
		}
		return PathToCordenadas(ListaAbertaPai,ListaFechada,B,A);
	}
	public boolean terrenoPassavel(int[] posicao,int[][] mapa){
		return mapa[posicao[0]][posicao[1]]==0;
	}
	public boolean estaNoMapa(int[] posicao,int length){
		return (posicao[0] >= 0 & posicao[1] >= 0
				& posicao[0] < length
				& posicao[1] < length);
		
	}
	public String ChecarPosicoesRestantes() {

		@SuppressWarnings("unchecked")
		HashMap<String, String> clone = (HashMap<String, String>) ListaAbertaPai
				.clone();

		ListaAbertaPai_UsoTemporario = clone;
		for (int l = 0; l < ListaFechada.size() + 1; l++) {

			ListaAbertaPai_UsoTemporario.remove(ListaFechada.get(l));

			if (ListaAbertaPai_UsoTemporario.isEmpty()) {
				return null;
			}
		}
		
		return (String) ListaAbertaPai_UsoTemporario.values().toArray()[0];

	}

	public void AdicionarListaAberta(int[] posicao, int custo, int[] uA) {
		ListaAbertaCusto.put(IntToString(posicao), custo);
		ListaAbertaPai.put(IntToString(posicao), IntToString(uA));
	}

	public String IntToString(int x[]) {
		return x[0] + ";" + x[1];
	}

	public int[] StringToInt(String s) {
		int[] x = new int[2];
		x[0] = Integer.parseInt(s.split(";")[0]);
		x[1] = Integer.parseInt(s.split(";")[1]);
		return x;
	}

	public int CalcCusto(int[] posicao, int[] B, int A[]) {
		int g = 0;
		if ((posicao[0] - A[0]) != 0 & posicao[1] - A[1] != 0) {
			g = 14;
		} else {
			g = 10;
		}
		return ((Math.abs((posicao[0] - B[0])) + Math.abs((posicao[1] - B[1]))) * 10 + g);
	}
	public 	String PathToCordenadas(HashMap aberta,HashMap fechada,int[] B,int[] A){
		String cordenada = (String)aberta.get(fechada.get(fechada.size()));
		
		String resultado=B[0]+";"+B[1]+" | "+cordenada;;
		while(true){
			resultado+=" | "+aberta.get(cordenada);
			cordenada=(String) aberta.get(cordenada);
			
			if(aberta.get(cordenada).equals(IntToString(A))){
				break;
			}
		}
		resultado+=" | "+A[0]+";"+A[1];
		return resultado;
		
	}
	public static void main(String args[]) {
		int[][] mapa = {{ 0, 0, 0, 0, 0, 0 }, 
				{ 1, 1, 0, 1, 1, 0 },
				{ 0, 1, 0, 1, 0, 1 },
				{ 0, 1, 0, 1, 1, 0 },
				{ 0, 1, 1, 1, 0, 1 }, 
				{ 0, 0, 0, 0, 1, 0 } };
		int[] A = { 0, 0 };
		int[] B = { 5, 5 };
		PathFinder path = new PathFinder();
		System.out.println(path.ProcurarPathFinder(A, B, mapa));
	}
}
