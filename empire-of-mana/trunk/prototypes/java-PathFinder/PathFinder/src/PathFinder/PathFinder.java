/*
*
* Path Finder - Algoritimo A*
* Vanderson M. do Rosario <vandersonmr2@gmail.com>
*
* Próximo passo é organizar e comentar o algoritimo.
*
*/

package PathFinder;
import java.util.HashMap;
public class PathFinder {
	HashMap<Integer, String> ListaFechada = new HashMap<Integer, String>();
	HashMap<String, Integer> ListaAbertaCusto = new HashMap<String, Integer>();
	HashMap<String, String> ListaAbertaPai = new HashMap<String, String>();
	int VezesReiniciado;
	int PMenor[];
	int ValorMenor;
	HashMap<String, String> ListaAbertaPai_UsoTemporario = null;
	public String getPath(int[] $origem, int $destino[], int[][] mapa) {
		int[] uA = $destino;
		ListaFechada.put(0, IntToString(uA));
		int custo;
		while (true) {
			PMenor = $destino;
			ValorMenor = 1000000000;
			for (int i = -1; i < 2; i++) {
				for (int j = -1; j < 2; j++) {
					int[] posicao = { uA[0] + i, uA[1] + j };
					if (estaNoMapa(posicao,mapa.length)) {
						if (terrenoPassavel(posicao, mapa) &&  !ListaFechada.containsValue(IntToString(posicao))) {
							if (!(posicao[0] == uA[0] && posicao[1] == uA[1])) {
								custo = CalcCusto(posicao, $origem, $destino);
								if (ListaAbertaCusto.containsKey(IntToString(posicao))) {
									if (Integer.valueOf((ListaAbertaCusto.get(IntToString(posicao)))) > custo) {
										AdicionarListaAberta(posicao, custo, uA);
									}
								} else {
									AdicionarListaAberta(posicao, custo, uA);
								}

								if ((custo < ValorMenor)) {
									ValorMenor = custo;
									PMenor = posicao;
								}
							}
						}
					}
				}
			}
			if (!ListaFechada.containsValue(IntToString(PMenor))) {
				ListaFechada.put(ListaFechada.size() + 1, IntToString(PMenor));
			}
			uA = PMenor;

			if (PMenor[0] == $origem[0] && PMenor[1] == $origem[1]) break;

			if (IntToString(uA).equals(IntToString($destino))) {
				VezesReiniciado += 1;
				if (VezesReiniciado > 9) {

					if (ChecarPosicoesRestantes() == null) {
						break;
					} else {
						try {
							uA = StringToInt((String) ListaAbertaPai_UsoTemporario .values().toArray()[0]);
						} catch (Exception e) {
							break;
						}
					}
					VezesReiniciado = 0;

				}
			}
		}
		return PathToCordenadas(ListaAbertaPai,ListaFechada,$origem,$destino);
	}
	public boolean terrenoPassavel(int[] posicao,int[][] mapa){
		return mapa[posicao[0]][posicao[1]]==0;
	}
	public boolean estaNoMapa(int[] posicao,int length){
		return (posicao[0] >= 0 & posicao[1] >= 0
			& posicao[0] < length
			& posicao[1] < length);
	}
	public String ChecarPosicoesRestantes() {
		@SuppressWarnings("unchecked")
		HashMap<String, String> clone = (HashMap<String, String>) ListaAbertaPai.clone();
		ListaAbertaPai_UsoTemporario = clone;
		for (int l = 0; l < ListaFechada.size() + 1; l++) {
			ListaAbertaPai_UsoTemporario.remove(ListaFechada.get(l));
			if (ListaAbertaPai_UsoTemporario.isEmpty()) return null;
		}

		return (String) ListaAbertaPai_UsoTemporario.values().toArray()[0];

	}
	public void AdicionarListaAberta(int[] posicao, int custo, int[] uA) {
		ListaAbertaCusto.put(IntToString(posicao), custo);
		ListaAbertaPai.put(IntToString(posicao), IntToString(uA));
	}
	public String IntToString(int x[]) {
		return x[0] + "," + x[1];
	}
	public int[] StringToInt(String s) {
		int[] x = new int[2];
		x[0] = Integer.parseInt(s.split(",")[0]);
		x[1] = Integer.parseInt(s.split(",")[1]);
		return x;
	}
	public int CalcCusto(int[] posicao, int[] B, int A[]) {
		int g = 0;
		if ((posicao[0] - A[0]) != 0 & posicao[1] - A[1] != 0) {
			g = 14;
		} else {
			g = 10;
		}
		return ((Math.abs((posicao[0] - B[0])) + Math.abs((posicao[1] - B[1]))) * 10 + g);
	}
	public String PathToCordenadas(HashMap aberta,HashMap fechada,int[] B,int[] A){
		String cordenada = (String)aberta.get(fechada.get(fechada.size()));
		String resultado=B[0]+","+B[1]+" → "+cordenada;;
		while(true){
			resultado+=" → "+aberta.get(cordenada);
			cordenada=(String) aberta.get(cordenada);
			if(aberta.get(cordenada).equals(IntToString(A))){
				break;
			}
		}
		resultado+=" → "+A[0]+","+A[1];
		return resultado;
	}
	public static void drawMap(int $mapa[][],String $caminho){
		String $gMapa[][] = new String[$mapa.length][$mapa[0].length];
		String $passos[] = $caminho.split(" → ");
		//String $coordenada[];
		for(int $p=0;$p<$passos.length;$p++){
			String $coordenada[] = $passos[$p].split(",");
			if($coordenada.length==2){
				if($p==0){
					$mapa[
						Integer.parseInt($coordenada[0].toString())][
						Integer.parseInt($coordenada[1].toString())
					]=7;/**/
				}else if($p==$passos.length-1){
					$mapa[
						Integer.parseInt($coordenada[0].toString())][
						Integer.parseInt($coordenada[1].toString())
					]=9;/**/
				}else{
					$mapa[
						Integer.parseInt($coordenada[0].toString())][
						Integer.parseInt($coordenada[1].toString())
					]=8;/**/
				}
			}
		}

		System.out.println("MAPA: "+$mapa[0].length+"x"+$mapa.length+"ts = "+($mapa[0].length*32)+"x"+($mapa.length*32)+"px ("+$passos.length+" passos)");
		for(int $l=0;$l<$mapa.length;$l++){
			for(int $c=0;$c<$mapa[$l].length;$c++){
				if($mapa[$l][$c]==7){$gMapa[$l][$c]="+";}
				else if($mapa[$l][$c]==8){$gMapa[$l][$c] = "*";}
				else if($mapa[$l][$c]==9){$gMapa[$l][$c]="X";}
				else if($mapa[$l][$c]==1){$gMapa[$l][$c] = String.valueOf((char)27);}
				else if($mapa[$l][$c]==0){$gMapa[$l][$c] = " ";}
				else{$gMapa[$l][$c]=String.valueOf($mapa[$l][$c]);}
				if($c==$mapa[$l].length-1){
					System.out.println($gMapa[$l][$c]);
				}else{
					System.out.print($gMapa[$l][$c]+".");
					//System.out.print($gMapa[$l][$c]+String.valueOf((char)27));
					//System.out.print($gMapa[$l][$c]);
				}
			}
		}
	}
	public static void main(String args[]) {
		/*int $mapa[][] = {
			{0,0,0,0,0,0},
			{1,1,1,1,1,0},
			{0,0,1,0,1,0},
			{0,1,0,1,1,0},
			{0,1,1,1,0,0},
			{0,0,0,0,0,0}
		};
		int $origem[] = {0,0};
		int $destino[] = {2,3}; /**/
		
		int $mapa[][] = { //Mapa Padrão: 15 linhas + 20 colunas = 640x480px
			{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
			{1,0,0,0,0,0,0,0,1,0,1,0,1,0,0,0,0,0,0,1},
			{1,1,1,1,1,1,1,0,1,0,0,0,1,0,1,0,0,0,0,1},
			{1,0,0,0,0,0,1,0,1,0,1,0,1,0,1,0,0,0,0,1},
			{1,0,1,0,0,0,0,0,1,0,1,0,1,0,1,0,0,0,0,1},
			{1,0,1,1,1,1,1,1,1,0,1,0,1,0,1,0,0,0,0,1},
			{1,0,0,0,0,0,0,0,1,0,1,0,1,0,1,0,0,0,0,1},
			{1,1,1,1,1,1,1,0,1,0,1,0,1,0,1,0,0,0,0,1},
			{1,0,0,0,0,0,0,0,1,0,1,0,1,0,1,0,0,0,0,1},
			{1,0,1,1,1,1,1,1,1,0,1,0,1,0,1,0,0,0,0,1},
			{1,0,0,1,0,0,0,0,1,0,1,0,1,0,1,0,0,0,0,1},
			{1,1,0,1,0,1,1,0,0,0,1,0,1,0,1,0,0,0,0,1},
			{1,1,0,1,1,0,1,1,1,1,1,0,1,0,1,0,0,0,0,1},
			{1,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,0,1},
			{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}
		};
		int $origem[] ={1,1}; //Dará errado se inverter a origem e o destino. =S
		int $destino[]={1,15}; // Dará errado se a colula do destino for maior do que o valor máxim,o de linhas (Exemplo: $destino[]={1,18}).

		/**/
		
		PathFinder path = new PathFinder();
		String $caminho;
		try{
			$caminho = path.getPath($origem, $destino, $mapa);
		}catch(NullPointerException $npe){ $caminho="Destinho impossível!"; }
		drawMap($mapa,$caminho);
		System.out.println("CAMINHO: "+$caminho+"\n");
		
		
	}
}