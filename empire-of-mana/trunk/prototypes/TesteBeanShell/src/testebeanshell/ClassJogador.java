/**
 * @author lunovox
 */
package testebeanshell;

public class ClassJogador {

    public ClassJogador(String Nome, Integer X, Integer Y) {
        this.Nome = Nome;
        this.X = X;
        this.Y = Y;
    }
    private String Nome;
    private Integer X;
    private Integer Y;

    public String getNome() {
        return Nome;
    }

    public void setNome(String Nome) {
        this.Nome = Nome;
    }

    public Integer getX() {
        return X;
    }

    public void setX(Integer X) {
        this.X = X;
    }

    public Integer getY() {
        return Y;
    }

    public void setY(Integer Y) {
        this.Y = Y;
    }
}
