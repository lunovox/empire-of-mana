/**
 * @author lunovox
 */
package testebeanshell;

import bsh.EvalError;
import bsh.Interpreter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;
import java.util.Scanner;
import javax.swing.JOptionPane;

public class Main {
	public static void Teste1() throws EvalError {
		Interpreter bsh = new Interpreter();
		bsh.set("foo", 5);                    // Set variables
		bsh.set("date", new Date());
		Date date = (Date) bsh.get("date");    // retrieve a variable
		// Eval a statement and get the result
		bsh.eval("bar = foo*10;");
		System.out.println("Variável 'bar' = " + bsh.get("bar"));
	}
	public static void Teste2() throws FileNotFoundException, IOException, EvalError {
		Interpreter bsh = new Interpreter();
		bsh.source("src/Scripts/HelloWorld.bsh");
	}
	public static void Teste3() throws FileNotFoundException, IOException, EvalError {
		Interpreter bsh = new Interpreter();
		bsh.source("src/Scripts/DragText.bsh");
		//bsh.eval("dragText();");
	}
	public static void Teste4() throws FileNotFoundException, IOException, EvalError {
		Interpreter bsh = new Interpreter();
		bsh.source("src/Scripts/CapturaDeTela.bsh");
		bsh.eval("captura(2000, 'aaaa');");
	}
	public static void Teste5() throws FileNotFoundException, IOException, EvalError {
		Interpreter bsh = new Interpreter();
		bsh.set("myClass", new MyClass());
		bsh.source("src/Scripts/Instanciador.bsh");
		//bsh.eval("myClass.myPrint('Aaaaaaaaaaaaaa!!!!'');");
	}
	public static void Teste6() throws FileNotFoundException, IOException, EvalError {
		Interpreter bsh = new Interpreter();
		bsh.set("EOM", new ClassEOM()); //Habilita as Janelas de Conversa como o Jogador dentro do BeanShell.
		bsh.set("Jogador", new ClassJogador("Lunovox", 10, 7)); //Instacia ClassJogador dentro do BeanShell.
		bsh.source("src/Scripts/NPC_Banqueiro.bsh"); //Abre um arquivo de Script do BeanShell
	}

	public static void main(String[] args) throws FileNotFoundException, IOException, EvalError {
		//Teste1(); //Ok!!!
		//Teste2(); //Ok!!!
		//Teste3(); //Ok!!!
		//Teste4(); //ERRO!!!
		//Teste5(); //Ok!!!
		Teste6();
	}
}
