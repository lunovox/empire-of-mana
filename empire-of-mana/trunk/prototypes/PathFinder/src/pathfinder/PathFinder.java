package pathfinder;

import java.util.HashMap;

/**
 *
 * @author vanderson
 */
public class PathFinder {
    
    static int x = 0;
    static int y = 1;
    
    private HashMap<Integer, String> closedList = new HashMap<Integer, String>();
    private HashMap<String, Integer> openList = new HashMap<String, Integer>();
    private HashMap<String, String> openListFathers = new HashMap<String, String>();
    
    private int timesRestarted;
    private int lowest;
    private int cost;
    
    public String getPath(int[] origin, int destination[], int[][] map) {
        
        int[] currentPosition = destination;
        closedList.put(0, dotIntToDotString(currentPosition));
        
        int[] bestDot;
        
        while (true) {
            // Escolhe a melhor das possibilidade 
            bestDot = bestNextDot(origin, destination, map, currentPosition);
            // Coloca essa melhor possibilidade em uma lista
            putOnClosedList(bestDot);
            // Proxima posição se torna essa.
            currentPosition = bestDot;
            //Se a posicao atual é a origem então para
            if (isEquals(currentPosition, origin)) {
                break;
            }
            // Gambiarra deve ser substituido
           
            if (isEquals(currentPosition,destination)) {
                timesRestarted += 1;
                if (timesRestarted > 9) {
                    currentPosition = origin;
                    timesRestarted = 0;
                }
            }
        
        }
        return PathToCordenadas(openListFathers, closedList, origin, destination);
    }
    
    private int[] bestNextDot(int[] origin, int destination[], int[][] map,int[] currentDot){
            int[] bestDot = destination;
            lowest=Integer.MAX_VALUE;
            
            for (int i = -1; i < 2; i++) {
                for (int j = -1; j < 2; j++) {
                    
                    int[] position = {(currentDot[x] + i),(currentDot[y] + j)};
           
                    
                    if (isOnMap(position, map[x].length, map.length)
                            && isDifferent(position, currentDot)
                            && isPassable(position, map)
                            && !wasAlreadyUsed(position)) {
     
                        cost = calcCost(position, origin, destination);
                
                        addOnOpenList(position, cost, currentDot);
                 
                        
                        if ((cost < lowest)) {
                            lowest = cost;
                            bestDot = position;
                        }
    
                    }
 
                }

            }

            return bestDot;
    }
    
    private boolean isDifferent(int[] dotA, int[] dotB) {
        return dotA[x] != dotB[x] || dotA[y] != dotB[y];
    }
    
    private boolean isEquals(int[] dotA,int[] dotB){
        return !(isDifferent(dotA, dotB));
    }
    
    private boolean wasAlreadyUsed(int[] position) {
        return closedList.containsValue(dotIntToDotString(position));
    }

    private boolean isPassable(int[] posicao, int[][] mapa) {
        return mapa[posicao[x]][posicao[y]] == 0;
    }

    private boolean isOnMap(int[] position, int mapHeight, int mapWidth) {
        return (position[x] >= 0 & position[y] >= 0
                & position[x] < mapWidth
                & position[y] < mapHeight);
    }
    
    private void addOnOpenList(int[] position, int cost, int[] currentDot) {
        if (openList.containsKey(dotIntToDotString(position))) {
            if (Integer.valueOf((openList.get(dotIntToDotString(position)))) > cost) {
                putOnOpenList(position, cost, currentDot);
            }
        } else {
            putOnOpenList(position, cost, currentDot);
        }
    }

    private void putOnOpenList(int[] currentDot, int cost, int[] FatherDot) {
        openList.put(dotIntToDotString(currentDot), cost);
        openListFathers.put(dotIntToDotString(currentDot), dotIntToDotString(FatherDot));
    }
    
    private void putOnClosedList(int[] dot){
         if (!closedList.containsValue(dotIntToDotString(dot))) {
                closedList.put(closedList.size() + 1, dotIntToDotString(dot));
         }
    }
    
    private String dotIntToDotString(int dotInt[]) {
        return dotInt[x] + "," + dotInt[y];
    }

    private int[] dotStringToDotInt(String dotString) {
        int[] dotInt = new int[2];
        dotInt[x] = Integer.parseInt(dotString.split(",")[x]);
        dotInt[y] = Integer.parseInt(dotString.split(",")[y]);
        return dotInt;
    }

    private int calcCost(int[] position, int[] dotB, int dotA[]) {
        int movimentCost = 0;
        if ((position[x] - dotA[x]) != 0 & position[y] - dotA[y] != 0) {
            movimentCost  = 14;
        } else {
            movimentCost  = 10;
        }
        return ((Math.abs((position[x] - dotB[x])) + Math.abs((position[y] - dotB[y]))) * 10 + movimentCost );
    }
    
    
    /* BAD CODE */
    private String PathToCordenadas(HashMap openList, HashMap closedList, int[] dotB, int[] dotA) {
        String cordenada = (String) openList.get(closedList.get(closedList.size()));
        String resultado = dotB[x] + "," + dotB[x] + " â†’ " + cordenada;;
        while (true) {
            resultado += " â†’ " + openList.get(cordenada);
            cordenada = (String) openList.get(cordenada);
            if (openList.get(cordenada).equals(dotIntToDotString(dotA))) {
                break;
            }
        }
        resultado += " â†’ " + dotA[x] + "," + dotA[y];
        return resultado;
    }
}
