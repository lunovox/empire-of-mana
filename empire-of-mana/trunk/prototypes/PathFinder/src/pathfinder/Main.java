package pathfinder;

public class Main {


    public static void drawMap(int $mapa[][], String $caminho) {
        String $gMapa[][] = new String[$mapa.length][$mapa[0].length];
        String $passos[] = $caminho.split(" â†’ ");
        //String $coordenada[];
        for (int $p = 0; $p < $passos.length; $p++) {
            String $coordenada[] = $passos[$p].split(",");
            if ($coordenada.length == 2) {
                if ($p == 0) {
                    $mapa[
						Integer.parseInt($coordenada[0].toString())][
						Integer.parseInt($coordenada[1].toString())] = 7;/**/
                } else if ($p == $passos.length - 1) {
                    $mapa[
						Integer.parseInt($coordenada[0].toString())][
						Integer.parseInt($coordenada[1].toString())] = 9;/**/
                } else {
                    $mapa[
						Integer.parseInt($coordenada[0].toString())][
						Integer.parseInt($coordenada[1].toString())] = 8;/**/
                }
            }
        }

        System.out.println("MAPA: " + $mapa[0].length + "x" + $mapa.length + "ts = " + ($mapa[0].length * 32) + "x" + ($mapa.length * 32) + "px (" + $passos.length + " passos)");
        for (int $l = 0; $l < $mapa.length; $l++) {
            for (int $c = 0; $c < $mapa[$l].length; $c++) {
                if ($mapa[$l][$c] == 7) {
                    $gMapa[$l][$c] = "+";
                } else if ($mapa[$l][$c] == 8) {
                    $gMapa[$l][$c] = "*";
                } else if ($mapa[$l][$c] == 9) {
                    $gMapa[$l][$c] = "X";
                } else if ($mapa[$l][$c] == 1) {
                    $gMapa[$l][$c] = String.valueOf((char) 27);
                } else if ($mapa[$l][$c] == 0) {
                    $gMapa[$l][$c] = " ";
                } else {
                    $gMapa[$l][$c] = String.valueOf($mapa[$l][$c]);
                }
                if ($c == $mapa[$l].length - 1) {
                    System.out.println($gMapa[$l][$c]);
                } else {
                    System.out.print($gMapa[$l][$c] + ".");
                    //System.out.print($gMapa[$l][$c]+String.valueOf((char)27));
                    //System.out.print($gMapa[$l][$c]);
                }
            }
        }
    }

    public static void main(String args[]) {

       int $mapa[][] = { //Mapa PadrÃ£o: 15 linhas + 20 colunas = 640x480px
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
            {1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1},
            {1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1},
            {1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1},
            {1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1},
            {1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1},
            {1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1},
            {1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1},
            {1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1},
            {1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1},
            {1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1},
            {1, 1, 0, 1, 0, 1, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1},
            {1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1},
            {1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1},
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}
        };

        int origem[] = {1, 1}; //DarÃ¡ errado se inverter a origem e o destino. =S
        int destino[] = {3, 18}; // DarÃ¡ errado se a colula do destino for maior do que o valor mÃ¡xim,o de linhas (Exemplo: $destino[]={1,18}).

        /**/

        PathFinder path = new PathFinder();
        String $caminho;
        try {
            $caminho = path.getPath(origem, destino, $mapa);
        } catch (NullPointerException $npe) {
            $caminho = "Destinho impossÃ­vel!";
        }
        drawMap($mapa, $caminho);
        System.out.println("CAMINHO: " + $caminho + "\n");


    }
}