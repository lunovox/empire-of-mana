/**
 * @author lunovox
 */
package testeluajava1;
import org.keplerproject.luajava.LuaState;
import org.keplerproject.luajava.LuaStateFactory;
public class Main {
	public static void main(String[] args) {
		LuaState lua = LuaStateFactory.newLuaState();
		lua.openLibs();
		lua.LdoString("print('Olá Mundo de Lua!')");
		//lua.LdoFile("hello.lua");
		//print("Olá Mundo de Lua!")
		System.out.println("Olá Mundo de Java!");
		lua.close();
	}
}
