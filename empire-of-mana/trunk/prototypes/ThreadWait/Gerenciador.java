package exemplothreads;
import javax.swing.JOptionPane;
/**
 * @author vanderson
 */
public class Gerenciador {
    public static void main(String[] args) {
        Processo[] p= new Processo[1000]; // Cada personagens vai ter um grupo de processos.
        for(int i=0;i<1000;i++){
            p[i]=new Processo();
            p[i].setEndereco(i);
            p[i].setPriority(1);
            p[i].start();        
        }
        while(true){
            p[Integer.parseInt(JOptionPane.showInputDialog(null,"Digite um processo para terminar de 0 a 999"))].setMsg("Msg qualquer");
        }
    }
}
