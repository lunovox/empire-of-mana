package exemplothreads;
/**
 * @author vanderson
 */
public class Processo extends Thread{
    private String msg="";
    private int endereco;

    public void setMsg(String msg){
  			this.msg=msg;
  			synchronized (this){this.notify();}
    }
    public String getMsg(){return msg;}
    public void setEndereco(int endereco){this.endereco=endereco;}
    public int getEndereco(){return endereco;}
    public void EsperarMsg() throws InterruptedException{
        msg=null;
        while(msg==null){
            synchronized (this){this.wait();}
        }
    }
    @Override
    public void run() {
        try {
            System.out.println(endereco+": comandos antes de esperar");
            EsperarMsg();
            System.out.println(endereco+": Comandos depois de esperar");
        }catch(InterruptedException ex){
            System.out.println("Problemas: "+ex);
        }
    }
}