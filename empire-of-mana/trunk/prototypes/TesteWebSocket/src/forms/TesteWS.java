/**
 * @author lunovox
 */

package forms;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.webbitserver.WebServer;
import org.webbitserver.WebServers;

public class TesteWS {
	public static void main(String[] args) {
		try {
			WebServer webServer = WebServers.createWebServer(8888);
			webServer.add("/", new WebSocketServer());
			webServer.start();
			//webServer.
			WebSocketServer.logRegister("Servidor iniciado!");
		} catch (IOException ex) {
			Logger.getLogger(TesteWS.class.getName()).log(Level.SEVERE, "Falha ao iniciar o servidor!", ex);
		}
	}
}
