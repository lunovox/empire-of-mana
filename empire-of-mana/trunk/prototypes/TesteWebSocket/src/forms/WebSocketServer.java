/**
 * @author lunovox
 */
package forms;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.webbitserver.WebSocketConnection;
import org.webbitserver.WebSocketHandler;

public class WebSocketServer implements WebSocketHandler {

	private List<WebSocketConnection> connections = new ArrayList<WebSocketConnection>();
	private WebSocketConnection websocket;
	private Integer players = 0;

	public void onOpen(WebSocketConnection wsc) throws Exception {
		//throw new UnsupportedOperationException("Not supported yet.");
		websocket = wsc;
		//wsc.send("ifexist?online&"+players);
		players++;
		wsc.data("username", wsc.hashCode());
		connections.add(wsc);
		broadCast("<font color='#0000FF'>"+wsc.data("username") + " → entrou no servidor!</font>");
		logRegister(wsc.data("username") + " → entrou no servidor!");
	}

	public void onClose(WebSocketConnection wsc) throws Exception {
		//throw new UnsupportedOperationException("Not supported yet.");
		players--;
		logRegister(wsc.data("username") + " → saiu no servidor!");
		broadCast("<font color='#0000FF'>"+wsc.data("username") + " → saiu no servidor!</font>");
		//broadCast(wsc.data("id") + " → saiu no servidor!");
		connections.remove(wsc);
		//PONG(wsc.hashCode() + " → saiu no servidor!");

	}

	public void onMessage(WebSocketConnection wsc, String testo) throws Throwable {
		//throw new UnsupportedOperationException("Not supported yet.");
		//logRegister("PONG(" + wsc.hashCode() + "): " + testo);
		if (!onCommand(wsc, testo)) {
			//wsc.send("PONG(" + wsc.hashCode() + "): " + testo);
			String mensagem = wsc.data("username") + " → " + testo;
			for (WebSocketConnection connection : connections) {
				//logRegister(connection.hashCode() + " → " + testo);
				logRegister(mensagem);
				connection.send(mensagem);
			}
		}
	}

	public void onMessage(WebSocketConnection wsc, byte[] bytes) throws Throwable {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	public void onPong(WebSocketConnection wsc, String string) throws Throwable {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	public static void logRegister(String Testo) {
		Logger.getLogger(TesteWS.class.getName()).log(Level.INFO, Testo);
	}

	private void broadCast(String message) {
		for (WebSocketConnection connection : connections) {
			logRegister(connection.data("username") + " → " + message);
			connection.send(message);
		}
	}

	private boolean onCommand(WebSocketConnection wsc, String Testo) {
		if (Testo.trim().toUpperCase().indexOf("/HELP") == 0) {
			logRegister(wsc.data("username") + " → /HELP");
			wsc.send(
				"<font color='#0000FF'>"+
				"→ /HELP<br/>"+
				"→ /PING <mensagem><br/>"+
				"→ /NAME <Apelido Visivel><br/>"+
				"→ /STOP"+
				"</font>"
			);
			return true;
		}else if (Testo.toUpperCase().indexOf("/PING ") == 0) {
			String mensagem = "PONG("+wsc.data("username")+"): " + Testo.substring(9);
			wsc.send("<font color='#0000FF'>"+mensagem+"</font>");
			logRegister(mensagem);
			return true;
		}else if (Testo.toUpperCase().indexOf("/NAME ") == 0) {
			String novoNome = Testo.substring(6);
			String mensagem = "SERVER: Usuário '"+wsc.data("username")+"' mudou o nome para '"+novoNome+"'.";
			wsc.send("<font color='#0000FF'>"+mensagem+"</font>");
			logRegister(mensagem);
			wsc.data("username", novoNome);
			return true;
		}else if (Testo.trim().toUpperCase().indexOf("/STOP") == 0) {
			logRegister(wsc.data("username") + " → Fechando Servidor!");
			for (WebSocketConnection connection : connections) {
				connection.send("<font color='#0000FF'>"+wsc.data("username") + " → Fechando Servidor!</font>");
				connection.close();
			}
			System.exit(0);
			return true;
		}
		return false;
	}
}
